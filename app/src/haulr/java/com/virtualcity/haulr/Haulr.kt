package com.virtualcity.haulr

import androidx.multidex.MultiDexApplication
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import com.mobile.distributr.common.EventBus
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import io.realm.RealmConfiguration
import net.openid.appauth.AuthState

class Haulr : MultiDexApplication() {
        var token: String? = null

        private var bus: EventBus? = null

     override fun onCreate() {
             super.onCreate()

             instance = this
             bus = EventBus()
             Realm.init(this)

             Realm.setDefaultConfiguration(realmConfiguration)


             val authState: AuthState? = PreferenceUtils.getAuthState()

             if(authState != null)
                     token = authState.idToken

         AppCenter.start(
             this, "31f6d568-a7a8-44bb-ac17-8f66f709b560",
             Analytics::class.java, Crashes::class.java
         )

     }

        fun bus(): EventBus? {
                return bus
        }

             companion object {

                     lateinit var instance: Haulr
                             private set

                     val realmConfiguration: RealmConfiguration
                             get() = RealmConfiguration.Builder()
                                     .name("Haulr")
                                     .modules(Realm.getDefaultModule()!!)
                                     .schemaVersion(23)
                                     .deleteRealmIfMigrationNeeded()
                                     .build()
             }
     }
