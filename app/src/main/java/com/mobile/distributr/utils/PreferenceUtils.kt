package com.mobile.distributr.utils

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.mobile.distributr.app.Distributr
import com.mobile.distributr.model.User
import net.openid.appauth.AuthState
import org.json.JSONException


object PreferenceUtils {

    const val CURRENT_OUTLET_NAME = "current_outlet_name"
    const val LAST_SYNC_TIME = "last_sync_time"
    const val CURRENT_OUTLET_ID = "current_outlet_id"
    const val CURRENT_OUTLET_TYPE_ID = "current_outlet_type_id"
    const val SALE_TYPE = "type"
    private const val USER = "user"
    const val DATA_SAVE_MODE = "data_save_mode"
    const val FIELD_REP_ID = "field_rep_id"

    fun getBoolean(key: String, defValue: Boolean): Boolean {
        val settings = PreferenceManager.getDefaultSharedPreferences(Distributr.instance)
        return settings.getBoolean(key, defValue)
    }

    fun putBoolean(key: String, value: Boolean) {
        val editor = PreferenceManager.getDefaultSharedPreferences(Distributr.instance).edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getInt(key: String, defValue: Int): Int {
        val settings = PreferenceManager.getDefaultSharedPreferences(Distributr.instance)
        return settings.getInt(key, defValue)
    }

    fun putInt(key: String, value: Int) {
        val editor = PreferenceManager.getDefaultSharedPreferences(Distributr.instance).edit()
        editor.putInt(key, value)
        editor.apply()
    }

    fun getLong(key: String, defValue: Long): Long {
        val settings = PreferenceManager.getDefaultSharedPreferences(Distributr.instance)
        return settings.getLong(key, defValue)
    }

    fun putLong(key: String, value: Long) {
        val editor = PreferenceManager.getDefaultSharedPreferences(Distributr.instance).edit()
        editor.putLong(key, value)
        editor.apply()
    }

    fun getString(key: String, defValue: String?): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(Distributr.instance)
        return settings.getString(key, defValue)
    }

    fun putString(key: String, value: String) {
        val editor = PreferenceManager.getDefaultSharedPreferences(Distributr.instance).edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun remove(key: String) {
        val editor = PreferenceManager.getDefaultSharedPreferences(Distributr.instance).edit()
        editor.remove(key)
        editor.apply()
    }

    fun getAuthToken(): String? {
        return getString("authToken", null)
    }

    fun saveAuthToken(token: String) {
        putString("authToken", token)
    }

    fun getUsersId(): String? {
        return getString(FIELD_REP_ID, null)
    }

    fun getAuthState(): AuthState? {
        val authStateString =
            PreferenceManager.getDefaultSharedPreferences(Distributr.instance).getString(
                "AuthState",
                null
            )
        return if (authStateString != null) {
            try {
                AuthState.jsonDeserialize(authStateString)
            } catch (e: JSONException) {
                e.printStackTrace()
                null
            }
        } else null
    }

    fun saveAuthState(authState: AuthState) {
        PreferenceManager.getDefaultSharedPreferences(Distributr.instance).edit()
            .putString("AuthState", authState.jsonSerializeString()).apply()
    }

    fun deleteSharedPreferences(){
        val settings: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(Distributr.instance)
            settings.edit().clear().apply()
    }

    fun saveUser(name:String, id: String, roles: Array<String>) {
        val user = User()

        user.name = name
        user.id = id
        user.roles = roles

        val json = Gson().toJson(user)
        putString(USER, json)
    }

//    {
//        "nbf": 1610111879,
//        "exp": 1610115479,
//        "iss": "https://auth.virtualcity.co.ke",
//        "aud": "Virtualcity_api",
//        "client_id": "Retailr-Mobile",
//        "sub": "ac27dd76-ee3a-4d8e-bef7-845308ebef38",
//        "auth_time": 1610111878,
//        "idp": "local",
//        "tenant": "5",
//        "tenant-name": "Text Book Center",
//        "name": "Patrick",
//        "role": "SalesRep",
//        "email": "sales@gmail.com",
//        "scope": [
//        "roles",
//        "openid",
//        "profile",
//        "email",
//        "Virtualcity_api"
//        ],
//        "amr": [
//        "pwd"
//        ]
//    }

}
