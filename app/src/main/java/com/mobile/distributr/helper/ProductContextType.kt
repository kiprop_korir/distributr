package com.mobile.distributr.helper

enum class ProductContextType {
    STOCK,
    SALE,
    ORDER,
    OTHER
}