package com.mobile.distributr.helper

enum class TransactionType(val rawValue:String) {
    ORDER ("Order"),
    VAN_SALE ("Sale"),
    POS ("POS"),
    RETURN ("Return"),
    LOSS ("Loss"),
    OTHER ("Other")
}