package com.mobile.distributr.constants

object Constants {
    var clientId = "Retailr-Mobile"
    var discoveryUri = "https://auth.vitrualcity.co.ke/.well-known/openid-configuration"
    var scope = "openid profile email roles Virtualcity_api"
    var authorizationEndpointUri = "https://auth.virtualcity.co.ke/connect/authorize"
    //var redirectLoginUri = "com.centauri.gauntlet:/callback"
    var redirectLoginUri = "retailr-mobile://callback"
    //var redirectLoginUri = "com.mobile.distributr:/callback"
    var tokenEndpointUri = "https://auth.virtualcity.co.ke/connect/token"
    var responseType = "code"
    var isGenerateCodeVerifier = true
    var registrationEndpointUri = ""
    var redirectLogoutUri = ""
    var isNonceAdded = true
    var clientSecret = "Retailr-Mobile"
    var baseUrl = "https://retailr.api.virtualcity.co.ke/api/"
}