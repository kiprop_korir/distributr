package com.mobile.distributr.common.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey
import java.io.Serializable

open class Category : RealmObject() , Serializable {

    @SerializedName("name")
    var name: String? = null
    @PrimaryKey
    @SerializedName("categoryCode")
    var code: String? = null
}
