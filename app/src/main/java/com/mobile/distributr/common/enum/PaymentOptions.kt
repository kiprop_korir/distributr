package com.mobile.distributr.common.enum

enum class PaymentOptions (val rawValue:String) {
    BANK ("BANK"),
    CASH ("CASH"),
    CHEQUE ("CHEQUE"),
    MPESA ("MPESA"),
    VOUCHER("VOUCHR")
}
