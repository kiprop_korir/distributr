package com.mobile.distributr.common.model
import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class Cart : RealmObject() {

    @SerializedName("items")
    var items: RealmList<CartItem?> = RealmList()
    @SerializedName("total")
    var total: Double? = null
    @SerializedName("gross_total")
    var grossTotal: Double? = null
    @SerializedName("tax")
    var tax: Double? = null
    @SerializedName("date")
    var date: String? = null
    @PrimaryKey
    @SerializedName("outlet_id")
    var outletId: String? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("valueDiscounts")
    var valueDiscounts: Double? = null
    @SerializedName("productDiscounts")
    var productDiscounts: Double? = null
}
