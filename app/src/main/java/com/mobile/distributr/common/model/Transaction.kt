package com.mobile.distributr.common.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName

open class Transaction : RealmObject() {

    @SerializedName("date")
    var date: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("amount")
    var amonut: Double? = null
    @SerializedName("type")
    var type: String? = null

}
