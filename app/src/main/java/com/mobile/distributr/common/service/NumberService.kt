package com.mobile.distributr.common.service

import java.math.RoundingMode
import java.text.DecimalFormat

class NumberService {

    companion object {

        fun roundOff(number: Double): Double {
            val df = DecimalFormat("#")
            df.roundingMode = RoundingMode.CEILING
            return df.format(number).toDouble()
        }

        fun formatAmount(number: Double) : String {
            val formatter = DecimalFormat("#,###.00")
            val roundedFigure = roundOff(number)
            val numberString = formatter.format(roundedFigure).toString()
            return "$numberString KES"
        }
    }
}