package com.mobile.distributr.common

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import kotlinx.android.synthetic.main.app_bar.*

open class BaseActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun showSuccessSnackBar(description:String){
        val snackBarView = Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
        //val view = snackBarView.view
        //val params = view.layoutParams as FrameLayout.LayoutParams
        //params.gravity = Gravity.BOTTOM
        //params.topMargin = 170
        //view.layoutParams = params
        snackBarView.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
        snackBarView.setTextColor(resources.getColor(R.color.white))
        snackBarView.setBackgroundTint(resources.getColor(R.color.green_400))
        snackBarView.show()
    }

}