package com.mobile.distributr.common.service

import com.mobile.distributr.app.Distributr
import com.mobile.distributr.model.DiscountGroupProduct
import com.mobile.distributr.model.InvoiceValueDiscount
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.common.service.TransactionService.Companion.getActualPrice
import com.mobile.distributr.model.DiscountGroup
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import io.realm.RealmList

class DiscountService {

    companion object {

        var realm: Realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(Distributr.instance)
            Realm.getDefaultInstance()
        }

        fun getProductItemDiscount(cartItem: CartItem): Double {

            //TODO: Clean below code
            var discount = 0.0

            val currentOutletTypeId =
                PreferenceUtils.getInt(PreferenceUtils.CURRENT_OUTLET_TYPE_ID, 0).toString()

            //check if product has a discount
            realm.beginTransaction()

            val discountGroupProducts =
                realm.where(DiscountGroupProduct::class.java).findAll()
            realm.commitTransaction()

            val discountGroupProductsIds = arrayListOf<Int>()

            discountGroupProducts.map {
                it.productId?.let { it1 -> discountGroupProductsIds.add(it1) }
            }

            if (discountGroupProductsIds.contains(cartItem.product?.id) && !cartItem.product?.isFreemium!!){

                realm.beginTransaction()
                val discountGroupObject = realm.where(DiscountGroup::class.java).equalTo("outletTypeId", currentOutletTypeId)
                    .findFirst()
                realm.commitTransaction()

                if(discountGroupObject != null){
                    realm.beginTransaction()
                    val discountProductObject = realm.where(DiscountGroupProduct::class.java)
                        .equalTo("productId", cartItem.product?.id).equalTo("discountGroupId", discountGroupObject.id).findFirst()
                    realm.commitTransaction()

                    if(discountProductObject != null){
                        val productPrice = getActualPrice(cartItem.product!!) ?: 0.0
                        val totalPrice = (productPrice * cartItem.quantity!!)
                        val productDiscount = (discountProductObject.rate?.div(100))?.times(totalPrice)
                        if (productDiscount != null) {
                            discount += productDiscount
                        }
                    }
                }
            }

            return discount
        }

        fun getTotalProductDiscount(items: RealmList<CartItem?>?): Double {
            var totalDiscount = 0.0
            if (items != null) {
                for (item in items) {
                    totalDiscount += item?.let {
                        getProductItemDiscount(
                            it
                        )
                    }!!
                }
            }
            return NumberService.roundOff(totalDiscount)
        }

        fun getValueDiscount(total: Double): Double {
            val valueDiscounts =
                realm.where(InvoiceValueDiscount::class.java).sort("minInvoiceValue").findAll()

            var discountTotal = 0.0

            for (discount in valueDiscounts) {
                //apply discount
                if (total.toInt() >= discount.minInvoiceValue?.toInt()!! && !discount.hasExpired!! && discount.isActive!!) {
                    val rate = (discount.percentageDiscount ?: 0.0).div(100)
                    discountTotal += rate.times(total)
                    break
                }
            }

            return NumberService.roundOff(discountTotal)
        }
    }
}