package com.mobile.distributr.common.enum

enum class ManifestType (val rawValue:String) {
    SALES("Sales") ,
    DELIVERY("Delivery")
}
