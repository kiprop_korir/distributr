package com.mobile.distributr.common.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class ProductItem : RealmObject(), Serializable {

    @SerializedName("productName")
    var name: String? = null
    @SerializedName("productCode")
    var code: String? = null
    @SerializedName("productPrice")
    var price: Double? = null
    @SerializedName("productQuantity")
    var quantity: Double? = null
}
