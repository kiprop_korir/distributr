package com.mobile.distributr.common.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.model.Category
import kotlinx.android.synthetic.main.item_category.view.*

class CategoryAdapter(
    private val context: Context,
    private var dataSource: ArrayList<Category>,
    private val itemClickListener: ItemClickListener

) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataSource.count()

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sorted = dataSource.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.code!! })
        val newDataSource: ArrayList<Category> = arrayListOf()
        newDataSource.addAll(sorted)
        dataSource = newDataSource

        holder.bindItems(dataSource[position],itemClickListener)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return ViewHolder(v)
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(category: Category, itemClickListener: ItemClickListener) {
            itemView.name.text = category.name

            itemView.setOnClickListener {

                itemClickListener.itemClick(category  )

            }
        }
    }

    interface ItemClickListener {
        fun itemClick(category: Category)
    }
}
