package com.mobile.distributr.common.enum

enum class AppFlavours (val rawValue:String) {
    DISTRIBUTR ("distributr"),
    MERCHANDIZR ("merchandizr"),
    HAULR ("haulr"),
    RETAILR ("retailr")
}
