package com.mobile.distributr.common.enum

enum class PricingDiscountCriteria (val rawValue:Int) {
    BY_PERCENTAGE (0),
    BY_VALUE (1)
}
