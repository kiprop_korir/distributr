package com.mobile.distributr.common.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobile.distributr.R
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.common.service.TransactionService
import com.mobile.distributr.helper.ProductContextType
import com.mobile.distributr.model.OutletType
import com.mobile.distributr.model.PricingTier
import com.mobile.distributr.model.Product
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Case
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_product.view.*
import java.text.DecimalFormat
import kotlin.properties.Delegates

class ProductAdapter(
    context: Context,
    private var dataSource: MutableList<Product>,
    private val contextType: ProductContextType? = ProductContextType.SALE,
    private val itemClickListener: ItemClickListener

) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    var realm : Realm = try {
        Realm.getDefaultInstance()
    } catch (e: Exception) {
        Realm.init(context)
        Realm.getDefaultInstance()
    }

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sorted = dataSource.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.name!! })
        val newDataSource: ArrayList<Product> = arrayListOf()
        newDataSource.addAll(sorted)
        dataSource = newDataSource

        if (contextType != null) {
            holder.bindItems( dataSource[position],itemClickListener,contextType)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ViewHolder(v)
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(product: Product, itemClickListener: ItemClickListener, contextType: ProductContextType
                      ) {

            itemView.name.text = product.name
            itemView.code.text = product.code
            itemView.quantity.text = "Available Qty: ${product.productQty.toString()}"

            val amount = TransactionService.getActualPrice(product)

            val formatter = DecimalFormat("#,###.00")
            val amt = formatter.format(amount)
            itemView.amount.text = "KES $amt"

            when (contextType) {
                ProductContextType.SALE -> {
                    itemView.quantity.visibility = View.GONE
                    itemView.amount.visibility = View.VISIBLE
                }
                ProductContextType.ORDER-> {
                    itemView.quantity.visibility = View.GONE
                    itemView.amount.visibility = View.VISIBLE
                }
                ProductContextType.STOCK -> {
                    itemView.amount.visibility = View.GONE
                    itemView.quantity.visibility = View.VISIBLE
                    itemView.amount.text = product.code
                }
                ProductContextType.OTHER -> {
                    itemView.amount.visibility = View.GONE
                    itemView.quantity.visibility = View.GONE
                    itemView.amount.text = product.code
                }
            }

            val dataSaveMode = PreferenceUtils.getBoolean(PreferenceUtils.DATA_SAVE_MODE, false)

            if(dataSaveMode){
                itemView.ivProductImage.visibility = View.GONE
            }
            else {
                Glide.with(itemView.context)
                    .load(product.imageUrl)
                    .placeholder(R.mipmap.image_placeholder).
                    into(itemView.ivProductImage)
                itemView.ivProductImage.visibility = View.VISIBLE
            }

            itemView.setOnClickListener {
                itemClickListener.itemClick(product  )
            }
        }
    }


    fun applyFilter(searchString: String?) {

        dataSource.clear()

        if (searchString.isNullOrEmpty()) {

            val realmProducts: RealmResults<Product>? = if (contextType == ProductContextType.SALE) {
                realm.where(Product::class.java).equalTo("isIssued", true)
                    .findAll()
            } else {
                realm.where(Product::class.java)
                    .findAll()
            }

            if (realmProducts != null) {
                for (product in realmProducts) {
                    dataSource.add(product)
                }
            }
        }
        else {

            val realmProducts: RealmResults<Product>? = if (contextType == ProductContextType.SALE) {
                realm.where(Product::class.java).equalTo("isIssued", true)
                .contains("name", searchString.toString().replace(" ",""), Case.INSENSITIVE).or()
                    .contains("code", searchString.toString().replace(" ",""))
                    .findAll()
            } else {
                realm.where(Product::class.java)
                .contains("name", searchString.toString().replace(" ",""), Case.INSENSITIVE).or()
                    .contains("code", searchString.toString().replace(" ",""))
                    .findAll()
            }

            if (realmProducts != null) {
                for (product in realmProducts) {
                    dataSource.add(product)
                }
            }
        }

        notifyDataSetChanged()
    }

    interface ItemClickListener {
        fun itemClick(product: Product)
    }

}
