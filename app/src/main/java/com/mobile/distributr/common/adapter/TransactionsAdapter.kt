package com.mobile.distributr.common.adapter;

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.model.Transaction
import kotlinx.android.synthetic.main.item_transaction.view.*
import java.text.DecimalFormat

class TransactionsAdapter(
    private val context: Context,
    private val dataSource: ArrayList<Transaction>

) : RecyclerView.Adapter<TransactionsAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return dataSource.count()

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position])
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_transaction, parent, false)
        return ViewHolder(v)
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(transaction: Transaction) {
            itemView.tv_description.text = transaction.description
            var amount = transaction.amonut
            val formatter = DecimalFormat("#,###.00")
            val amt = formatter.format(amount)
            itemView.tv_amount.text = amt
            itemView.tv_date.text = transaction.date

            if(transaction.type == "CREDIT"){
                itemView.tv_amount.setTextColor(itemView.context.resources.getColor(R.color.red_300))
            }
            else if(transaction.type == "DEBIT"){

                itemView.tv_amount.setTextColor(itemView.context.resources.getColor(R.color.green_500))
            }
        }
    }
}
