package com.mobile.distributr.common.service

import com.mobile.distributr.app.Distributr
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.model.Product
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import io.realm.RealmList

class TransactionService {

    companion object {

        var realm: Realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(Distributr.instance)
            Realm.getDefaultInstance()
        }

        fun getGrossTotal(items: RealmList<CartItem?>?): Double{
            var grossTotal = 0.0

            if (items != null) {
                for (item in items) {

                    //check if product is freemium
                    if(!item?.product?.isFreemium!!) {
                        val productPrice = getActualPrice(item.product!!)
                        val total = (productPrice?.times(item.quantity!!))
                        if (total != null) {
                            grossTotal += total
                        }
                    }
                }
            }

           return NumberService.roundOff(grossTotal)
        }

        fun getTaxTotal(items: RealmList<CartItem?>?): Double{
            var tax = 0.0

            if (items != null) {
                for (item in items) {

                    //check if product is freemium

                    if(!item?.product?.isFreemium!!) {
                        val productPrice = getActualPrice(item.product!!) ?: 0.0

                        val untaxedTotal = (productPrice * item.quantity!!)
                        val taxableTotal = untaxedTotal - DiscountService.getProductItemDiscount(
                            item
                        )
                        val rate = (item.product!!.vatRate ?: 16.0).div(100)
                        val productTax = rate.times(taxableTotal)
                        tax += productTax
                    }
                }
            }

            return NumberService.roundOff(tax)
        }

        fun getActualPrice(product: Product) : Double? {

            val currentOutletTypeId =
                PreferenceUtils.getInt(PreferenceUtils.CURRENT_OUTLET_TYPE_ID, 0)
            var amount = product.price

            if (product.tierPrices != null) {
                for (pricingTier in product.tierPrices!!) {
                    if (pricingTier.outletTypeId == currentOutletTypeId) {
                       amount = pricingTier.price ?: 0.0
                    }
                }
            }
            return amount
        }
    }
}