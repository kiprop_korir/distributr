package com.mobile.distributr.common.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.Product
import io.realm.annotations.PrimaryKey

open class CartItem : RealmObject() {

    @SerializedName("product")
    var product: Product? = null
    @SerializedName("quantity")
    var quantity: Int? = null
    @SerializedName("amount")
    var amount: Double? = null
    @SerializedName("amount_incl_vat")
    var amountTotal: Double? = null
    @SerializedName("VAT")
    var VAT: Double? = null
    @PrimaryKey
    @SerializedName("productId")
    var productId: Int? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("discount")
    var discount: String? = null
    @SerializedName("outletId")
    var outletId: String? = null
    @SerializedName("state")
    var state: String? = null
}
