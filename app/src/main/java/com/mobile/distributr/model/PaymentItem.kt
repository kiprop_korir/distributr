package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class PaymentItem : RealmObject() {
    @PrimaryKey
    @SerializedName("refNo")
    var refNo: String? = null
    @SerializedName("amount")
    var amount: Double? = null
    @SerializedName("timestamp")
    var timestamp: String? = null
    @SerializedName("paymentOption")
    var paymentOption: String? = null

//    {
//        "refNo": "string",
//        "amount": 0,
//        "invoiceRef": "string",
//        "invoiceTotal": 0,
//        "balance": 0,
//        "paymentOption": "CASH = 0",
//        "timestamp": "2021-02-06T08:59:29.886Z",
//        "receipt": {
//        "refNo": "string",
//        "amount": 0,
//        "customer": "string",
//        "timestamp": "2021-02-06T08:59:29.886Z"
//    }
//    }
}
