package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class DiscountGroupProduct : RealmObject() {

    @SerializedName("productId")
    var productId: Int? = null
    @SerializedName("rate")
    var rate: Double? = null
    @SerializedName("discountGroupId")
    var discountGroupId: String? = null
    @SerializedName("expiryDate")
    var expiryDate: String?= null
    @SerializedName("isActive")
    var isActive: Boolean?= null
    @PrimaryKey
    @SerializedName("id")
    var id: String?= null

}
