package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.common.model.CartItem
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class PricingTier : RealmObject() {

    @SerializedName("description")
    var description: String? = null
    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("code")
    var code: String? = null
    @SerializedName("timestamp")
    var timestamp: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("isActive")
    var isActive: Boolean? = false
    @SerializedName("outletTypeId")
    var outletTypeId: String? = null
}