package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class SaleItemTest : RealmObject() {

    @SerializedName("product")
    var product: Product? = null
    @SerializedName("quantity")
    var quantity: Int? = null
    @SerializedName("amount")
    var amount: Double? = null
    @SerializedName("amount_incl_vat")
    var amountTotal: Double? = null
    @SerializedName("VAT")
    var VAT: Double? = null
    @PrimaryKey
    @SerializedName("productID")
    var productID: String? = null

}
