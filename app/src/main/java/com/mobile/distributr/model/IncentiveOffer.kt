package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class IncentiveOffer : RealmObject() {

    @SerializedName("minLimit")
    var minLimit: Int? = null
    @SerializedName("productId")
    var productId: Int? = null
    @SerializedName("offerProductId")
    var offerProductId: Int?= null
    @SerializedName("expiryDate")
    var expiryDate: String?= null
    @SerializedName("hasExpired")
    var hasExpired: Boolean?= null
    @SerializedName("isActive")
    var isActive: Boolean?= null
    @PrimaryKey
    @SerializedName("id")
    var id: String?= null

}
