package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName

open class SaleTest : RealmObject() {

    @SerializedName("transactionReference")
    var productName: String? = null
    @SerializedName("transactionDate")
    var productCode: String? = null
    @SerializedName("orderTypeId")
    var orderTypeId: Double? = null
    @SerializedName("documentStatusId")
    var documentStatusId: Double? = null
    @SerializedName("orderParentId")
    var orderParentId: Double? = null
    @SerializedName("orderLineItemType")
    var orderLineItemType: Double? = null
    @SerializedName("quantity")
    var quantity: Int? = null
    @SerializedName("value")
    var value: Double? = null
    @SerializedName("VAT")
    var VAT: Double? = null
    @SerializedName("productDiscount")
    var productDiscount: Int? = null
    @SerializedName("distributorId")
    var distributorId: Double? = null
    @SerializedName("distributorName")
    var distributorName: Double? = null
    @SerializedName("salesmanId")
    var salesmanId: Double? = null
    @SerializedName("productId")
    var productId: Double? = null
    @SerializedName("returnable")
    var returnable: Double? = null
    @SerializedName("returnableType")
    var returnableType: Double? = null
    @SerializedName("product")
    var product: Product? = null

}
