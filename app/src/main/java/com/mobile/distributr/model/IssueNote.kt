package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey
import java.io.Serializable

open class IssueNote : RealmObject() , Serializable {

    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("code")
    var totalItems: Int? = null

    @SerializedName("fieldRepId")
    var fieldRepId: Int? = null

    @SerializedName("fieldRepName")
    var fieldRepName: String? = null

    @SerializedName("timestamp")
    var timestamp: String? = null

    @SerializedName("refNo")
    var refNo: String? = null
}
