package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class ProductLineItem : RealmObject() {
    @PrimaryKey
    @SerializedName("productId")
    var productId: Int? = null
    @SerializedName("quantity")
    var quantity: Int? = null
    @SerializedName("timestamp")
    var timestamp: String? = null
    @SerializedName("invoice")
    var invoice: InvoiceItem? = null
}
