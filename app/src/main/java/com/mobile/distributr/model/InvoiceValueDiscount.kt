package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class InvoiceValueDiscount : RealmObject() {

    @SerializedName("minInvoiceValue")
    var minInvoiceValue: Double? = null
    @SerializedName("percentageDiscount")
    var percentageDiscount: Double? = null
    @SerializedName("offerProductId")
    var offerProductId: Int? = null
    @SerializedName("amount")
    var startDate: String?= null
    @SerializedName("expiryDate")
    var expiryDate: String?= null
    @SerializedName("hasExpired")
    var hasExpired: Boolean?= null
    @SerializedName("isActive")
    var isActive: Boolean?= null
    @PrimaryKey
    @SerializedName("id")
    var id: String?= null

}
