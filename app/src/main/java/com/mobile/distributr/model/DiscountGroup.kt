package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class DiscountGroup : RealmObject() {

    @SerializedName("outletTypeId")
    var outletTypeId: String? = null
    @SerializedName("isActive")
    var isActive: Boolean?= null
    @PrimaryKey
    @SerializedName("id")
    var id: String?= null


//    "outletTypeId": 8,
//    "outletTypeName": "Outlet Type 1",
//    "totalProducts": 0,
//    "code": "GroupOutlettype1",
//    "isActive": true,
//    "timestamp": "2021-01-17T13:55:20.1741584",
//    "name": "GroupOutlettype1",
//    "id": 5
}
