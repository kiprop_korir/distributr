package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey
import java.io.Serializable

open class Product : RealmObject() {

    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("code")
    var code: String? = null
    @SerializedName("imageUrl")
    var imageUrl: String? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("brand")
    var brand: String? = null
    @Index
    @SerializedName("category")
    var category: String? = null
    @SerializedName("price")
    var price: Double? = 0.00
    @SerializedName("isFreemium")
    var isFreemium: Boolean? = false
    @SerializedName("timestamp")
    var timestamp: String? = null
    @SerializedName("isActive")
    var isActive: Boolean? = null
    @SerializedName("quantity")
    var productQty: Int? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("tierPrices")
    var tierPrices: RealmList<TierPrice>? = null
    @SerializedName("vatRate")
    var vatRate: Double? = null
    var isIssued: Boolean = false
}
