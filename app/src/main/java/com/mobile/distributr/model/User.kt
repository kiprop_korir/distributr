package com.mobile.distributr.model

import java.io.Serializable

class User : Serializable {
    var name: String? = null
    var roles: Array<String>? = null
    var id: String? = null
}