package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.common.model.CartItem
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class InvoiceItem : RealmObject() {
    @PrimaryKey
    @SerializedName("refNo")
    var refNo: String? = null
    @SerializedName("payments")
    var payments: RealmList<PaymentItem?> = RealmList()
}
