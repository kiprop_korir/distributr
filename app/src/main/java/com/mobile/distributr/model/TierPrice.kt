package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey
import java.io.Serializable

open class TierPrice : RealmObject(), Serializable {
    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("productId")
    var productId: Int? = null
    @SerializedName("outletTypeId")
    var outletTypeId: Int? = null
    @SerializedName("value")
    var value: Double? = null
    @SerializedName("price")
    var price: Double? = null
}