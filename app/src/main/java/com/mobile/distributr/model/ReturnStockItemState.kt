package com.mobile.distributr.model
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ReturnStockItemState : RealmObject(){
    @SerializedName("name")
    var description: String? = null
    @PrimaryKey
    @SerializedName("id")
    var id: String? = null
}