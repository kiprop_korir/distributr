package com.mobile.distributr.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.common.model.CartItem
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class Order : RealmObject() {

    @SerializedName("items")
    var items: RealmList<CartItem?> = RealmList()
    @SerializedName("id")
    var id: String? = null
    @PrimaryKey
    @SerializedName("date")
    var date: String? = null
    @SerializedName("status")
    var status: String? = null
}
