package com.mobile.distributr.config

object UserConfig {
    const val AUTHORITY_URL = "https://vcidentity.b2clogin.com/tfp/vcidentity.onmicrosoft.com/B2C_1A_VCTY_signin/"
    val SCOPES = arrayListOf("https://vcidentity.onmicrosoft.com/RetailrApi/Retailr.Read.All")
}