package com.mobile.distributr.app

import android.content.ContentValues.TAG
import android.content.Context
import android.content.pm.PackageManager
import android.util.Base64
import android.util.Log
import androidx.multidex.MultiDexApplication
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import com.mobile.distributr.BuildConfig
import com.mobile.distributr.common.EventBus
import com.mobile.distributr.common.enum.AppFlavours
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import io.realm.RealmConfiguration
import net.openid.appauth.AuthState
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class Distributr : MultiDexApplication() {
        var token: String? = null

        private var bus: EventBus? = null
        val isMockEnvironment = false

     override fun onCreate() {
             super.onCreate()

             instance = this

            printHashKey(this)

             bus = EventBus()
             Realm.init(this)

             Realm.setDefaultConfiguration(realmConfiguration)

             val authState: AuthState? = PreferenceUtils.getAuthState()

             if(authState != null)
                     token = authState.idToken

            var appCenterToken =  String()

         when (BuildConfig.FLAVOR) {
             AppFlavours.RETAILR.rawValue -> {
                 appCenterToken = "8b98c8d0-485b-432b-a478-53505495b3d4"
             }
             AppFlavours.MERCHANDIZR.rawValue -> {
                 appCenterToken = "3f52576c-13ef-4689-9fc0-534c8487b505"
             }
             AppFlavours.HAULR.rawValue -> {
                 appCenterToken = "31f6d568-a7a8-44bb-ac17-8f66f709b560"
             }
             AppFlavours.DISTRIBUTR.rawValue -> {
                 appCenterToken = "36362475-3e43-4af8-a764-4426968b90fb"
             }
         }

         AppCenter.start(
             this, appCenterToken,
             Analytics::class.java, Crashes::class.java
         )

     }

    fun printHashKey(pContext: Context?) {
        try {
            val info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey: String = String(Base64.encode(md.digest(), 0))
                Log.d("HashKey", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e(TAG, "printHashKey()", e)
        } catch (e: Exception) {
            Log.e(TAG, "printHashKey()", e)
        }
    }
        fun bus(): EventBus? {
                return bus
        }

             companion object {

                     lateinit var instance: Distributr
                             private set

                     val realmConfiguration: RealmConfiguration
                             get() = RealmConfiguration.Builder()
                                     .name("Distributr")
                                     .modules(Realm.getDefaultModule()!!)
                                     .schemaVersion(23)
                                     .deleteRealmIfMigrationNeeded()
                                     .build()
             }
     }
