package com.mobile.distributr.modules.deliveries.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class Delivery : RealmObject() {
    @PrimaryKey
    @SerializedName("id")
    var id: String? = null
    @SerializedName("timestamp")
    var timestamp: String? = null
    @SerializedName("isDelivered")
    var isDelivered: Boolean? = null
    @SerializedName("deliveryBy")
    var deliveryBy: String? = null
    @SerializedName("customerName")
    var customerName: String? = null
    @SerializedName("refNo")
    var refNo: String? = null
    @SerializedName("invoiceId")
    var invoiceId: String? = null
}
