package com.mobile.distributr.modules.notifications

import android.os.Bundle
import android.view.Gravity
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.notifications.adapter.NotificationsAdapter
import com.mobile.distributr.modules.notifications.model.DistributrNotification
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_notifications.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlin.collections.ArrayList

class NotificationsActivity : AppCompatActivity() {

    var notifications: ArrayList<DistributrNotification> = arrayListOf()
    lateinit var realm: Realm
    var outletId = ""
    lateinit var type: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        tv_title.text = "My Notifications"

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        realm.beginTransaction()
        val realmProducts = realm.where(DistributrNotification::class.java)
            .findAll()
        for (product in realmProducts) {
            notifications.add(product)
        }
        realm.commitTransaction()

        val adapter =
            NotificationsAdapter(
                this,
                notifications,
                object :
                    NotificationsAdapter.ItemClickListener {


                    override fun itemClick(notification: DistributrNotification) {
                        //
                    }
                })

        rv_notifications.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_notifications.adapter = adapter

        btn_back.setOnClickListener{
            finish()
        }

    }
}