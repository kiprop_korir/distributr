package com.mobile.distributr.modules.loadstock

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.model.Product
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.modules.loadstock.adapter.LoadStockItemAdapter
import com.mobile.distributr.modules.loadstock.model.LoadStockItem
import com.mobile.distributr.modules.loadstock.model.LoadStockProductItem
import com.mobile.distributr.modules.home.Home
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_summary_stock_take.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.dialog_add_to_cart.*
import kotlinx.android.synthetic.main.dialog_stock_take.view.*
import java.util.*

class LoadStockSummary : AppCompatActivity() {

    var productItems: ArrayList<LoadStockProductItem> = arrayListOf()
    lateinit var realm: Realm
    private var cart: Cart? = null
    var outletId = ""
    lateinit var type: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary_load_stock)
        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        tv_title.text = "LOAD STOCK SUMMARY"
        date.text = Date().toString()


        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        realm.beginTransaction()
        val realmProducts = realm.where(LoadStockItem::class.java)
            .findAll()

        if (realmProducts.size > 0 ) {
            for (item in realmProducts[0]?.productItems!!){
                productItems.add(item)
            }
        }
        realm.commitTransaction()

        val adapter = LoadStockItemAdapter(this@LoadStockSummary,productItems,object : LoadStockItemAdapter.ItemClickListener {


            override fun changeQuantity(item: LoadStockProductItem) {
                val mDialogView = LayoutInflater.from(this@LoadStockSummary)
                    .inflate(R.layout.dialog_load_stock, null)
                //AlertDialogBuilder
                val mBuilder = AlertDialog.Builder(this@LoadStockSummary)
                    .setView(mDialogView)
                    .setTitle(item.product?.name)
                //show dialog
                val mAlertDialog = mBuilder.show()

                val quantity = edQuantity.text.toString()

                mDialogView.edQuantity.setText(quantity)
                //place cursor end of edit text
                mDialogView.edQuantity.setSelection(mDialogView.edQuantity.text.length)
                //mDialogView.quantity.requestFocus()
                mDialogView.btn_add.text = "DONE"
                //show keyboard
                //showSoftKeyboard(mDialogView.ed_quantity)//login button click of custom layout
                mDialogView.btn_add.setOnClickListener {
                    //dismiss dialog
                    mAlertDialog.dismiss()

                    realm.beginTransaction()
                    val qty = mDialogView.edQuantity.text.toString().toInt()
                    item.quantity = qty
                    realm.copyToRealmOrUpdate(item)
                    realm.commitTransaction()

                    showSuccessSnackBar("Quantity changed successfully" )
                    rv_products.adapter?.notifyDataSetChanged()
                }
            }
        })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        complete.setOnClickListener{
            //update local product list

            for (item in productItems){
                realm.beginTransaction()
                val product = realm.where(Product::class.java).equalTo("productCode", item.product?.code!!).findFirst()
                if (product != null) {
                    product.productQty = item.quantity?.let { it1 ->
                        product.productQty?.plus(
                            it1
                        )
                    }
                }
                realm.commitTransaction()
            }
            showSuccessSnackBar("Stock loaded successfully")
            Handler().postDelayed({
                val intent = Intent(this, Home::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            }, 1500)

        }

        btn_back.setOnClickListener{
            finish()
        }
    }

    private fun showSuccessSnackBar(description: String){
        Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
    }

}