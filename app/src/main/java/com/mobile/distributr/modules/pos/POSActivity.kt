package com.mobile.distributr.modules.pos

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.helper.ProductContextType
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.model.Product
import com.mobile.distributr.common.service.TransactionService
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.common.service.DiscountService
import com.mobile.distributr.common.service.NumberService
import com.mobile.distributr.modules.pos.adapter.POSCartItemAdapter
import com.mobile.distributr.modules.pos.adapter.POSCategoryAdapter
import com.mobile.distributr.modules.pos.adapter.POSProductAdapter
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_pos_tablet.*
import kotlinx.android.synthetic.main.activity_pos_tablet.rv_products
import kotlinx.android.synthetic.main.app_bar_pos.*
import kotlinx.android.synthetic.main.dialog_add_to_cart.view.*
import kotlinx.android.synthetic.main.pos_cart.*
import kotlin.collections.ArrayList
import kotlin.math.sqrt

class POSActivity :AppCompatActivity() {

    private lateinit var state: State
    
    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pos_tablet)

        state = State()

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        loadViews()
        calculateTotals()
    }
    
    private fun loadViews(){

        val metrics = DisplayMetrics()
        this.windowManager.defaultDisplay.getMetrics(metrics)

        val yInches = metrics.heightPixels / metrics.ydpi
        val xInches = metrics.widthPixels / metrics.xdpi
        val diagonalInches = sqrt(xInches * xInches + yInches * yInches.toDouble())

        if (diagonalInches >= 6.5) { // 6.5inch device or bigger
                state.screenType = POSScreenType.TABLET
        } else {
                state.screenType = POSScreenType.MOBILE
        }

        //requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        tv_title.text = "POS"

        back.setOnClickListener{
            onBackPressed()
        }

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        state.realm.executeTransaction{

            val realmProducts: RealmResults<Product>? = state.realm.where(Product::class.java)
                        .equalTo("isIssued", true)
                        .findAll()

            if (realmProducts != null) {
                for (product in realmProducts) {
                    state.products.add(product)
                }
            }
        }

        btnCheckOut.setOnClickListener {
            val intent = Intent(this, POSSummaryActivity::class.java)
            startActivity(intent)
        }

        state.productAdapter = POSProductAdapter(this, state.products,state.cartItemsList, ProductContextType.SALE, object : POSProductAdapter.ItemClickListener {

            @SuppressLint("SetTextI18n")
            override fun itemClick(product: Product) {

                val cartItem = getExistingCartItem(product.id!!)

                val mDialogView = LayoutInflater.from(this@POSActivity).inflate(R.layout.dialog_add_to_cart, null)
                //AlertDialogBuilder
                val mBuilder = AlertDialog.Builder(this@POSActivity)
                    .setView(mDialogView)
                    .setTitle(product.name)
                //show dialog
                val  mAlertDialog = mBuilder.show()

                if(state.type == TransactionType.VAN_SALE.rawValue)
                    mDialogView.available_quantity.visibility = View.VISIBLE

                mDialogView.available_quantity.text = "Available Qty: ${product.productQty}"

                val quantity = cartItem?.quantity ?: 1

                mDialogView.edQuantity.setText(quantity.toString())

                //place cursor end of edit text
                mDialogView.edQuantity.setSelection(mDialogView.edQuantity.text.length)

                mDialogView.btn_add.setOnClickListener {
                    //dismiss dialog
                    mAlertDialog.dismiss()
                    addToCart(cartItem, product, mDialogView.edQuantity.text.toString().toInt())
                }

                mDialogView.edQuantity.doAfterTextChanged {
                    mDialogView.btn_add.isEnabled = false
                    val qty = it.toString()

                    if(qty == "" || qty == "0"){
                        mDialogView.tvErrorText.text = "Invalid input"
                        mDialogView.tvErrorText.visibility = View.VISIBLE
                        return@doAfterTextChanged
                    }

                    if (it.toString().toInt() <= product.productQty!!){
                        mDialogView.tvErrorText.visibility = View.INVISIBLE
                        mDialogView.btn_add.isEnabled = true
                    }
                    else {
                        mDialogView.tvErrorText.visibility = View.VISIBLE
                        mDialogView.tvErrorText.text = "Quantity entered is more than the available quantity!"
                    }
                }

            }
        })

        if( state.screenType == POSScreenType.TABLET){
            rv_products.layoutManager = GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false)
        }
        else {
            rv_products.layoutManager = GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false)
        }

        rv_products.adapter = state.productAdapter


        val categoryList = mutableListOf<String>()

        //get unique categories

        state.realm.executeTransaction {
            val categories: RealmResults<Product>  = state.realm.where(Product::class.java).distinct("category").findAll()
            for(i in 0 until categories.count()){
                categories[i]?.category?.let { it1 -> categoryList.add(it1) }
            }
        }

        val categoryAdapter = POSCategoryAdapter( categoryList, object : POSCategoryAdapter.ItemClickListener {

            @SuppressLint("SetTextI18n")
            override fun itemClick(title: String) {

                state.products.clear()

                if (title == "ALL CATEGORIES") {

                    val realmProducts: RealmResults<Product>? = state.realm.where(Product::class.java)
                        .equalTo("isIssued", true)
                        .findAll()

                    if (realmProducts != null) {
                        for (product in realmProducts) {
                            state.products.add(product)
                        }
                    }

                } else {
                    state.realm.executeTransaction {
                        val realmProducts =
                            state.realm.where(Product::class.java).equalTo("isIssued", true)
                                .equalTo("category", title)
                                .findAll()

                        if (realmProducts != null) {
                            for (product in realmProducts) {
                                state.products.add(product)
                            }
                        }
                    }
                }
                state.productAdapter?.updateData(state.products)
            }
        })

        rvCategories.layoutManager = LinearLayoutManager(this,  LinearLayoutManager.HORIZONTAL, false)

        rvCategories.adapter = categoryAdapter

        state.cartAdapter = POSCartItemAdapter(
                this,
                state.cartItemsList,
                object :
                    POSCartItemAdapter.ClickListener {
                    override fun changeQuantity(item: CartItem, quantity: Int) {
                        //get object from state.realm
                        state.realm.beginTransaction()
                        item.quantity = quantity
                        state.realm.copyToRealmOrUpdate(item)
                        state.realm.commitTransaction()
                        calculateTotals()
                        updateCart()
                    }

                    override fun deleteFromCart(item: CartItem, position: Int) {
                        //
                        state.realm.beginTransaction()
                        item.deleteFromRealm()
                        state.realm.commitTransaction()
                        //state.realm.close()
                        getCartItems()
                        rv_products.adapter?.notifyDataSetChanged()
                        calculateTotals()
                        updateCart()
                    } })

            rvCart.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            rvCart.adapter = state.cartAdapter

    }


    fun getCartItems(){

        state.cartItemsList.clear()

        state.realm.beginTransaction()
        state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type).equalTo("outletId", state.type).findFirst()
        state.realm.commitTransaction()

        if (state.cart?.items != null) {
            for (item in state.cart?.items!!)
                state.cartItemsList.add(item!!)
        }
        else {
            state.cart = Cart()
            state.cart!!.outletId = state.type
            state.cart!!.type = state.type

            //if(state.screenType == POSScreenType.TABLET) {
                empty_cart.visibility = View.VISIBLE
                bottom.visibility = View.GONE
            //}
        }
    }
    @SuppressLint("SetTextI18n")
    fun calculateTotals(){

        state.grossTotal = 0.0
        state.tax = 0.0
        state.totalIncTax = 0.0
        state.totalLessDiscount = 0.0
        state.cart = null

        getCartItems()

        if (state.cart?.items?.count()!! > 0) {

            state.grossTotal = TransactionService.getGrossTotal(state.cart?.items)
            state.productDiscounts = DiscountService.getTotalProductDiscount(state.cart?.items)
            state.valueDiscount = DiscountService.getValueDiscount(state.grossTotal)
            state.subTotal = state.grossTotal - state.valueDiscount - state.productDiscounts
            state.tax = TransactionService.getTaxTotal(state.cart?.items)
            state.netAmount = state.tax + state.subTotal

            tvGrossTotal.text = NumberService.formatAmount(state.grossTotal)
            tvProductDiscounts.text = NumberService.formatAmount(state.productDiscounts)
            tvValueDiscounts.text = NumberService.formatAmount(state.valueDiscount)
            tvSubTotal.text = NumberService.formatAmount(state.subTotal)
            tvTaxTotal.text = NumberService.formatAmount(state.tax)
            tvNetTotal.text = NumberService.formatAmount(state.netAmount)

        }
        else {
            tvGrossTotal.text = "KES O.OO"
            tvTaxTotal.text = "KES O.OO"
            tvNetTotal.text = "KES O.OO"
            tvSubTotal.text = "KES O.OO"
            tvProductDiscounts.text = "KES O.OO"
            tvValueDiscounts.text = "KES O.OO"
        }
    }

    fun addToCart(existingCartItem: CartItem?, product: Product, quantity:Int){

        if(existingCartItem != null) {
            state.realm.executeTransaction{
                existingCartItem.quantity = quantity
                state. realm.copyToRealmOrUpdate(existingCartItem)
            }
            showSuccessSnackBar("Cart item updated")
        }
        else {
            val cartItem = CartItem()
            cartItem.product = product
            cartItem.quantity = quantity
            cartItem.productId = product.id
            cartItem.type = state.type
            cartItem.outletId = state.outletId

            val vat = quantity * product.price!! * product.vatRate!!
            cartItem.VAT = vat
            val amount = quantity * product.price!!
            cartItem.amount = amount
            cartItem.amountTotal = amount + vat
            cartItem.type = state.type
            cartItem.outletId = state.outletId

            state.realm.executeTransaction() {
                state.cart!!.items.add(cartItem)
                state.realm.copyToRealmOrUpdate(state.cart!!)
            }
            showSuccessSnackBar(product.name + " added to cart")
        }
        getCartItems()
        updateCart()
    }

    fun getExistingCartItem(productId: Int) : CartItem? {
        var item: CartItem? = null
        state.realm.executeTransaction() {
            item = state.realm.where(CartItem::class.java).equalTo("productId", productId).equalTo("type", state.type).
            equalTo("outletId",state.outletId).findFirst()
        }
        return item
    }

    private fun showSuccessSnackBar(description: String){
        val snackBar = Snackbar.make(toolbar,description, Snackbar.LENGTH_SHORT)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(resources.getColor(R.color.colorSuccess, null))
        snackBar.show()
    }

    private fun updateCart(){

        state.realm.beginTransaction()
        state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type).equalTo("outletId", state.outletId).findFirst()

        if (state.cart == null) {
            state.cart = Cart()
            state.cart!!.type = state.type
            state.cart!!.outletId = state.outletId
        }
        state.realm.commitTransaction()

        val count = state.cart?.items?.count() ?: 0

        if (count > 0) {
            tvCartCount.text = count.toString()
            tvCartCount.visibility = View.VISIBLE
        } else
            tvCartCount.visibility = View.GONE

        state.cartAdapter?.updateData(state.cartItemsList)
        state.productAdapter?.updateData(state.products)
        calculateTotals()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }
    override fun onBackPressed() {
        super.onBackPressed()

        val builder = androidx.appcompat.app.AlertDialog.Builder(this)

        builder.setTitle("Exit POS mode?")
        builder.setMessage("Are you sure you want to exit POS Mode?")

        builder.setPositiveButton("Yes") { _, _ ->
            finish()
        }

        builder.setNegativeButton("No", null)

        val dialog = builder.create()
        dialog.show()
    }

    override fun onResume() {
        super.onResume()
        updateCart()
    }

    private class State {
        var products: ArrayList<Product> = arrayListOf()
        lateinit var realm: Realm
        var cart: Cart? = null
        var outletId = TransactionType.POS.rawValue
        var cartItemsList = ArrayList<CartItem>()
        var grossTotal: Double = 0.0
        var totalLessDiscount: Double = 0.0
        var discount: Double = 0.0
        var tax: Double = 0.0
        var totalIncTax: Double = 0.0
        val type = TransactionType.POS.rawValue
        var screenType: POSScreenType = POSScreenType.MOBILE
        var productDiscounts: Double = 0.0
        var valueDiscount: Double = 0.0
        var cartAdapter: POSCartItemAdapter? = null
        var productAdapter: POSProductAdapter? = null
        var outletName = ""
        var subTotal: Double = 0.0
        var netAmount: Double = 0.0
    }
}