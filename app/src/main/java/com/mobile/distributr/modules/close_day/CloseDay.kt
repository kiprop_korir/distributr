package com.mobile.distributr.modules.close_day

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import kotlinx.android.synthetic.main.activity_close_day.*
import kotlinx.android.synthetic.main.app_bar.*

class CloseDay : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_close_day)
        tv_title.text = "CLOSE DAY"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        financials.setOnClickListener{
            //toast("Under works")
        }

        return_inventory.setOnClickListener {
                    val intent = Intent(this, ReturnActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
        }
        btn_back.setOnClickListener{
            finish()
        }
    }

    private fun showSuccessSnackBar(description: String){
        Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
    }
}
