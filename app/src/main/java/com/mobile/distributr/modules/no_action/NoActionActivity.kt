package com.mobile.distributr.modules.no_action

import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.utils.PreferenceUtils
import kotlinx.android.synthetic.main.activity_no_action.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.app_bar.toolbar
import java.util.*


class NoActionActivity : AppCompatActivity()  {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_no_action)
   tv_title.text = "NO ACTION"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val outletName = PreferenceUtils.getString(PreferenceUtils.CURRENT_OUTLET_NAME, "")
        val outletId = PreferenceUtils.getInt(PreferenceUtils.CURRENT_OUTLET_ID, 0).toString()

        outlet.text = outletName
        date.text = Date().toString()

        val adapter = ArrayAdapter.createFromResource(
            applicationContext,
            R.array.no_action_reasons,
            android.R.layout.simple_spinner_dropdown_item
        )

        complete.setOnClickListener{
            showSuccessSnackBar("No Action completed successfully")
            Handler().postDelayed({
                finish()
            }, 1500)
        }

        spin_reason.adapter = adapter

        btn_back.setOnClickListener{
            finish()
        }

    }

    private fun showSuccessSnackBar(description: String){
        Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
    }

}
