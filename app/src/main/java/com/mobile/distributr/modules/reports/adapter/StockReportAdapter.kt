package com.mobile.distributr.modules.reports.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.reports.model.StockReportProductItem
import kotlinx.android.synthetic.main.item_stock_report.view.*


class StockReportAdapter(
    private val context: Context,
    private val dataSource: ArrayList<StockReportProductItem>

) :  RecyclerView.Adapter<StockReportAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position])
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_stock_report, parent, false)
        return ViewHolder(v)
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(item: StockReportProductItem) {
            itemView.name.text = item.product?.name
            itemView.quantity.text = item.quantity.toString()
            val changeQuantity = item.quantityChange.toString()
            itemView.amount.text = "$changeQuantity units"

            when (item.activityDescription) {
                "sale" -> itemView.activity_description.text = "sold"
                "order" -> itemView.activity_description.text = "ordered"
                else -> itemView.activity_description.text = item.activityDescription
            }
        }
    }

    interface ItemClickListener {
        fun changeQuantity(item: StockReportProductItem)
    }
}
