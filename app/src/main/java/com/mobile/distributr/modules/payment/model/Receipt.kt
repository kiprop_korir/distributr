package com.mobile.distributr.modules.payment.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class Receipt : RealmObject() {
    @PrimaryKey
    @SerializedName("refNo")
    var refNo: String? = null
    @SerializedName("amount")
    var amount: Double? = null
    @SerializedName("customer")
    var customer: String? = null
    @SerializedName("timestamp")
    var timestamp: String? = null
}
