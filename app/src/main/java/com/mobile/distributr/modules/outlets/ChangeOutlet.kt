package com.mobile.distributr.modules.outlets

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.MenuItemCompat
import com.mobile.distributr.R
import com.mobile.distributr.modules.wallet.adapter.ViewPagerAdapter
import com.mobile.distributr.modules.wallet.fragments.TodayHistoryFragment
import com.mobile.distributr.modules.wallet.fragments.WeeklyHistoryFragment
import com.mobile.distributr.utils.PreferenceUtils
import kotlinx.android.synthetic.main.activity_change_outlet.*
import kotlinx.android.synthetic.main.app_bar.*

class ChangeOutlet : AppCompatActivity() {

    private var searchView:androidx.appcompat.widget.SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_outlet)

        tv_title.text = "CHANGE OUTLET"

        setOutlet()

        btn_back.setOnClickListener{
            finish()
        }

        setupViewPager()
    }

    private fun setupViewPager() {

        val adapter = ViewPagerAdapter(supportFragmentManager)

        val first = ViewByRouteFragment()
        val second = ViewByProximityFragment()

        adapter.addFragment(first, "VIEW BY ROUTE")
        adapter.addFragment(second, "VIEW BY PROXIMITY")

        viewPager.adapter = adapter

        tabs.setupWithViewPager(viewPager)
    }

    private fun setOutlet() {
        val outlet = PreferenceUtils.getString(PreferenceUtils.CURRENT_OUTLET_NAME, null)
            ?: "No outlet selected"
        current_outlet.text = outlet
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.home, menu)
        val searchItem: MenuItem = menu.findItem(R.id.action_search)
        searchView = MenuItemCompat.getActionView(searchItem) as androidx.appcompat.widget.SearchView
        searchView!!.setOnCloseListener { true }

        val searchPlate =  searchView!!.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
        searchPlate.hint = "Search for outlet"
        val searchPlateView: View =
            searchView!!.findViewById(androidx.appcompat.R.id.search_plate)
        searchPlateView.setBackgroundColor(
            ContextCompat.getColor(
                this,
                android.R.color.transparent
            )
        )

        searchView!!.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
            // Toast.makeText(applicationContext, query, Toast.LENGTH_SHORT).show()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
               // mAdapter.filter.filter(newText);
                return false
            }
        })

        val searchManager =
            getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView!!.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        return super.onCreateOptionsMenu(menu)
    }
}
