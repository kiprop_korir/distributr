package com.mobile.distributr.modules.wallet

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import kotlinx.android.synthetic.main.activity_send_money.*
import kotlinx.android.synthetic.main.app_bar.*

class SendMoney : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_money)
        tv_title.text = "SEND MONEY"


        bank.setOnClickListener{
            val intent = Intent(this, SendToBank::class.java)
            startActivity(intent)
        }

        wallet.setOnClickListener{
            val intent = Intent(this, SendToWallet::class.java)
            startActivity(intent)
        }

        mobile_money.setOnClickListener{
            val intent = Intent(this, SendToMobileMoney::class.java)
            startActivity(intent)
        }

        btn_back.setOnClickListener{
            finish()
        }
    }

}
