package com.mobile.distributr.modules.outlets.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey
import java.io.Serializable

open class Route : RealmObject(), Serializable {

    @SerializedName("description")
    var description: String? = null
    @SerializedName("code")
    var code: String? = null
    @SerializedName("name")
    var name: String? = null
    @PrimaryKey
    @SerializedName("id")
        var id: Int? = null
}
