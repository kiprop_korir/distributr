package com.mobile.distributr.modules.manifest.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.service.TransactionService
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.common.service.NumberService
import kotlinx.android.synthetic.main.item_stock_summary.view.*
import java.math.RoundingMode
import java.text.DecimalFormat

class StockSummaryItemAdapter(
    private val context: Context,
    private val dataSource: ArrayList<CartItem>,
    private val clickListener: ClickListener

) :  RecyclerView.Adapter<StockSummaryItemAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position], clickListener)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_stock_summary, parent, false)
        return ViewHolder(
            v
        )
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(cartItem: CartItem, clickListener: ClickListener) {
            itemView.name.text = cartItem.product?.name
            itemView.code.text = cartItem.product?.code

            itemView.quantity.text = cartItem.quantity.toString()

            itemView.quantity.paintFlags = itemView.quantity.paintFlags or Paint.UNDERLINE_TEXT_FLAG

            itemView.quantity.setOnClickListener{
                clickListener.changeQuantity(cartItem,cartItem.quantity!!)
            }

            itemView.remove.setOnClickListener {
                clickListener.deleteFromCart(cartItem, adapterPosition)
            }

        }

        private fun roundOffDecimal(number: Double): Double? {
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            return df.format(number).toDouble()
        }
    }

    interface ClickListener {
        fun changeQuantity(item: CartItem, quantity: Int)
        fun deleteFromCart(item: CartItem, position: Int)
    }
}
