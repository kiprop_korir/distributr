package com.mobile.distributr.modules.splashscreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.multidex.BuildConfig
import com.mobile.distributr.R
import com.mobile.distributr.common.enum.AppFlavours
import com.mobile.distributr.modules.home.Home
import com.mobile.distributr.modules.onboarding.LoginActivity
import com.mobile.distributr.modules.pos.POSActivity
import com.mobile.distributr.utils.PreferenceUtils
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        tv_app_name.text = resources.getString(R.string.app_name)

        val delayedHandler = Handler()
        delayedHandler.postDelayed({
            val authToken = PreferenceUtils.getAuthToken()

            val intent: Intent = if (authToken == null) {
                Intent(applicationContext, LoginActivity::class.java)
            } else {
                if(BuildConfig.FLAVOR == AppFlavours.RETAILR.rawValue) {
                    Intent(applicationContext, POSActivity::class.java)
                } else {
                    Intent(applicationContext, Home::class.java)
                }
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()

        }, 2000)

    }
}