package com.mobile.distributr.modules.manifest.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.ProductLineItem
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class StockReturnState : RealmObject() {
    @SerializedName("items")
    var items: RealmList<ProductLineItem?> = RealmList()
    @PrimaryKey
    @SerializedName("id")
    var id: String? = null
    @SerializedName("date")
    var date: String? = null
    @SerializedName("state")
    var state: String? = null
    @SerializedName("timestamp")
    var timestamp: String? = null
}
