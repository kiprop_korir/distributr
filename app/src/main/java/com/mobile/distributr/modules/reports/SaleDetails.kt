package com.mobile.distributr.modules.reports

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.modules.outlets.model.Outlet
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_change_outlet.*
import kotlinx.android.synthetic.main.app_bar.*

class SaleDetails : AppCompatActivity() {

    lateinit var realm: Realm
    var outlets: ArrayList<Outlet> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_outlet)

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        realm.beginTransaction()
        val realmOutlets = realm.where(Outlet::class.java)
            .findAll()
        for (outlet in realmOutlets) {
            outlets.add(outlet)
        }
        realm.commitTransaction()
        
        setOutlet()


        btn_back.setOnClickListener{
            finish()
        }

    }


    fun setOutlet() {
        val outlet = PreferenceUtils.getString(PreferenceUtils.CURRENT_OUTLET_NAME, null)
            ?: "No outlet selected"
        current_outlet.text = outlet
    }


}
