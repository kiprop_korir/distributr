package com.mobile.distributr.modules.payment.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class Payment : RealmObject() {
    @PrimaryKey
    @SerializedName("refNo")
    var refNo: String? = null
    @SerializedName("amount")
    var amount: Double? = null
    @SerializedName("invoiceRef")
    var invoiceRef: String? = null
    @SerializedName("invoiceRef")
    var balance: Double? = null
    @SerializedName("invoiceTotal")
    var invoiceTotal: Double? = null
    @SerializedName("paymentOption")
    var paymentOption: String? = null
    @SerializedName("timestamp")
    var timestamp: String? = null
    @SerializedName("receipt")
    var receipt: Receipt? = null
}
