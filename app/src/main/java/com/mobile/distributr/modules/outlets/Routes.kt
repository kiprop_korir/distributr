package com.mobile.distributr.modules.outlets

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.modules.outlets.model.Outlet
import io.realm.Realm

import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.routes.*

class Routes : AppCompatActivity() {

    lateinit var realm: Realm
    var outlets: ArrayList<Outlet> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.routes)
        tv_title.text = "MY ROUTES"


        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        btn_back.setOnClickListener{
            finish()
        }
    }

}
