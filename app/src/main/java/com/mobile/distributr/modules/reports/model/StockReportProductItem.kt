package com.mobile.distributr.modules.reports.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.Product
import io.realm.annotations.PrimaryKey

open class StockReportProductItem : RealmObject() {

    @SerializedName("product")
    var product: Product? = null
    @PrimaryKey
    @SerializedName("productId")
    var productId: Int? = null
    @SerializedName("quantity")
    var quantity: Int? = null
    @SerializedName("activity_description")
    var activityDescription: String? = null
    @SerializedName("quantity_change")
    var quantityChange: Int? = null
}
