package com.mobile.distributr.modules.pos.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobile.distributr.R
import com.mobile.distributr.helper.ProductContextType
import com.mobile.distributr.model.Product
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Case
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_product_pos.view.*
import java.text.DecimalFormat

class POSProductAdapter(
    private val context: Context,
    private var dataSource: MutableList<Product>,
    var cartItemsList: ArrayList<CartItem>,
    private val contextType: ProductContextType? = ProductContextType.SALE,
    private val itemClickListener: ItemClickListener

) : RecyclerView.Adapter<POSProductAdapter.ViewHolder>() {

    var realm : Realm = try {
        Realm.getDefaultInstance()
    } catch (e: Exception) {
        Realm.init(context)
        Realm.getDefaultInstance()
    }

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    fun updateData(updatedData: MutableList<Product>) {
        dataSource = updatedData
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sorted = dataSource.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.name!! })
        val newDataSource: ArrayList<Product> = arrayListOf()
        newDataSource.addAll(sorted)
        dataSource = newDataSource

        if (contextType != null) {
            holder.bindItems(context,dataSource[position],cartItemsList,itemClickListener,contextType)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_product_pos, parent, false)
        return ViewHolder(v)
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(context: Context, product: Product, cartItems: ArrayList<CartItem> , itemClickListener: ItemClickListener, contextType: ProductContextType) {
            itemView.tvName.text = product.name

            val amount = product.price

            val formatter = DecimalFormat("#,###.00")
            val amt = formatter.format(amount)

            itemView.tvQuantity.text = product.productQty.toString()
            itemView.tvAvailableQuantity.text = "Available Qty: ${product.productQty.toString()}"
            itemView.tvQuantity.visibility = View.GONE
            itemView.tvPrice.visibility = View.VISIBLE
            itemView.tvPrice.text = "KES $amt"

            val dataSaveMode = PreferenceUtils.getBoolean(PreferenceUtils.DATA_SAVE_MODE, false)

            if(dataSaveMode){
                itemView.ivProductImage.visibility = View.GONE
            }
            else {
                Glide.with(itemView.context)
                    .load(product.imageUrl)
                    .placeholder(R.mipmap.image_placeholder).
                    into(itemView.ivProductImage)
                itemView.ivProductImage.visibility = View.VISIBLE
            }

            itemView.setOnClickListener {
                itemClickListener.itemClick(product  )
            }

            val cartItemIds = arrayListOf<Int>()

                cartItems.map {
                    it.productId?.let { it1 -> cartItemIds.add(it1) }
                }

            if (cartItemIds.contains( product.id )) {
                itemView.rlSelectedOverlay.visibility  = View.VISIBLE
            }
            else {
                itemView.rlSelectedOverlay.visibility  = View.GONE
            }
        }
    }


    fun applyFilter(searchString: String?) {

        dataSource.clear()

        if (searchString.isNullOrEmpty()) {

            val realmProducts: RealmResults<Product>? = if (contextType == ProductContextType.SALE) {
                realm.where(Product::class.java).equalTo("isIssued", true)
                    .findAll()
            } else {
                realm.where(Product::class.java)
                    .findAll()
            }

            if (realmProducts != null) {
                for (product in realmProducts) {
                    dataSource.add(product)
                }
            }
        }
        else {

            val realmProducts: RealmResults<Product>? = if (contextType == ProductContextType.SALE) {
                realm.where(Product::class.java).equalTo("isIssued", true)
                    .contains("name", searchString.toString().replace(" ",""), Case.INSENSITIVE).or()
                    .contains("code", searchString.toString().replace(" ",""))
                    .findAll()
            } else {
                realm.where(Product::class.java)
                    .contains("name", searchString.toString().replace(" ",""), Case.INSENSITIVE).or()
                    .contains("code", searchString.toString().replace(" ",""))
                    .findAll()
            }

            if (realmProducts != null) {
                for (product in realmProducts) {
                    dataSource.add(product)
                }
            }
        }

        notifyDataSetChanged()
    }

    interface ItemClickListener {
        fun itemClick(product: Product)
    }

}
