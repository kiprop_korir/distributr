package com.mobile.distributr.modules.sale.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.InvoiceItem
import com.mobile.distributr.model.ProductLineItem
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class Sale : RealmObject() {

    @SerializedName("items")
    var items: RealmList<ProductLineItem?> = RealmList()
    @PrimaryKey
    @SerializedName("id")
    var id: String? = null
    @SerializedName("date")
    var date: String? = null
    @SerializedName("outletName")
    var outletName: String? = null
    @SerializedName("outletId")
    var outletId: String? = null
    @SerializedName("amount")
    var amount: Double? = null
    @SerializedName("paymentMade")
    var paymentMade: Double? = null
    @SerializedName("valueDiscounts")
    var valueDiscounts: Double? = null
    @SerializedName("productDiscounts")
    var productDiscounts: Double? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("consumerId")
    var consumerId: String? = null
    @SerializedName("state")
    var state: String? = null
    @SerializedName("timestamp")
    var timestamp: String? = null
    @SerializedName("invoice")
    var invoice: InvoiceItem? = null
    var isSynced: Boolean? = false
}
