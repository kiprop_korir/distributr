package com.mobile.distributr.modules.notifications

enum class NotificationStatus(val color: String)  {
    READ("read"),
    UNREAD("undread")
}