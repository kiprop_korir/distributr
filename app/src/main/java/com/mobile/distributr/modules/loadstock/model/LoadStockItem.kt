package com.mobile.distributr.modules.loadstock.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class LoadStockItem : RealmObject() {

    @SerializedName("product")
    var productItems: RealmList<LoadStockProductItem> =   RealmList()
    @PrimaryKey
    @SerializedName("date")
    var date: String? = null
}
