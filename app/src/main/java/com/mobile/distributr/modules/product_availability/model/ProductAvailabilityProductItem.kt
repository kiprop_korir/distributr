package com.mobile.distributr.modules.product_availability.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.Product
import io.realm.annotations.PrimaryKey

open class ProductAvailabilityProductItem : RealmObject() {

    @SerializedName("product")
    var product: Product? = null
    @PrimaryKey
    @SerializedName("productCode")
    var productCode: String? = null
    @SerializedName("available")
    var available: Boolean = true
    @SerializedName("storage_location")
    var imageLocation: String? = null
}
