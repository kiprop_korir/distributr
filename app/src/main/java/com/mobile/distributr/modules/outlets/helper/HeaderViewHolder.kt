package com.mobile.distributr.modules.outlets.helper

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R

internal class HeaderViewHolder(view: View) :
    RecyclerView.ViewHolder(view) {
    val tvTitle: TextView = view.findViewById(R.id.tvTitle)
}