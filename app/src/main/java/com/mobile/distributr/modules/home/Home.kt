package com.mobile.distributr.modules.home

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.mobile.distributr.BuildConfig
import com.mobile.distributr.R
import com.mobile.distributr.app.Distributr
import com.mobile.distributr.common.enum.AppFlavours
import com.mobile.distributr.event.LoggedOutEvent
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.model.*
import com.mobile.distributr.modules.close_day.CloseDay
import com.mobile.distributr.modules.deliveries.DeliveriesActivity
import com.mobile.distributr.modules.deliveries.model.Delivery
import com.mobile.distributr.modules.home.adapter.NavMenuAdapter
import com.mobile.distributr.modules.loadstock.LoadStock
import com.mobile.distributr.modules.manifest.ManifestActivity
import com.mobile.distributr.modules.manifest.model.StockReturn
import com.mobile.distributr.modules.no_action.NoActionActivity
import com.mobile.distributr.modules.notifications.NotificationsActivity
import com.mobile.distributr.modules.onboarding.LoginActivity
import com.mobile.distributr.modules.outlets.ChangeOutlet
import com.mobile.distributr.modules.outlets.OutletFinder
import com.mobile.distributr.modules.outlets.model.Outlet
import com.mobile.distributr.modules.outlets.model.Route
import com.mobile.distributr.modules.product_availability.ProductAvailability
import com.mobile.distributr.modules.reports.Reports
import com.mobile.distributr.modules.sale.MakeSale
import com.mobile.distributr.modules.sale.model.Sale
import com.mobile.distributr.modules.settings.Settings
import com.mobile.distributr.modules.wallet.Wallet
import com.mobile.distributr.network.RestClient
import com.mobile.distributr.network.RestInterface
import com.mobile.distributr.network.network_body.InvoiceBody
import com.mobile.distributr.network.network_body.PostSaleBody
import com.mobile.distributr.network.network_body.ReturnNoteBody
import com.mobile.distributr.utils.PreferenceUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.drawer_layout.*
import kotlinx.android.synthetic.main.nav_header_home.*
import java.util.*

class Home : AppCompatActivity() {

    var outlet: String? = null
    lateinit var realm: Realm
    private val disposable = CompositeDisposable()
    private val restInterface by lazy {
        RestClient.client.create(RestInterface::class.java)
    }

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.drawer_layout)

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        loadViews()

        if(!Distributr.instance.isMockEnvironment) {
            getUserId()
        }

        Distributr.instance
            .bus()
            ?.toObservable()
            ?.subscribe { event ->
                if (event is LoggedOutEvent) {
                    PreferenceUtils.deleteSharedPreferences()
                    val intent = Intent(this, LoginActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)

                    disposable.delete(disposable)

                    this.runOnUiThread {
                        kotlin.run {
                                 Toast.makeText(
                                     applicationContext,
                                     "You have been logged out by the system, kindly log in again",
                                     Toast.LENGTH_LONG
                                 ).show()
                              }
                         }
                    }
                }
    }

    private fun loadViews(){

        val json = PreferenceUtils.getString("user", "")
        val user  = Gson().fromJson(json, User::class.java)

        tv_username.text =  user.name
        var roles = ""
        user.roles?.map {
            roles += "$it\n"
        }
        tv_role.text = roles

        if(Distributr.instance.isMockEnvironment){
            tv_mock_data.visibility = View.VISIBLE
        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)

        close_day.setOnClickListener{  drawerLayout.closeDrawer(GravityCompat.START)
            val intent = Intent(this, CloseDay::class.java)
            startActivity(intent)
        }

        val menuList = findViewById<ListView>(R.id.menuList)

        val listItems = mutableListOf<String>()

        when (BuildConfig.FLAVOR) {
            AppFlavours.DISTRIBUTR.rawValue, AppFlavours.HAULR.rawValue -> {
                listItems.add("Notifications")
                listItems.add("My Routes & Outlets")
                listItems.add("Reports")
                listItems.add("Manifest")
                listItems.add("Settings")
            }
            AppFlavours.MERCHANDIZR.rawValue -> {
                listItems.add("Notifications")
                listItems.add("My Routes & Outlets")
                listItems.add("Reports")
                listItems.add("Settings")
            }

            AppFlavours.RETAILR.rawValue -> {
                listItems.add("Notifications")
                listItems.add("Reports")
                listItems.add("Manifest")
                listItems.add("Settings")
            }
        }

        val adapter = NavMenuAdapter(this, listItems as ArrayList<String>)
        menuList.adapter = adapter

        menuList.setOnItemClickListener { _, _, position, _ ->

            drawerLayout.closeDrawer(GravityCompat.START)

            when (listItems[position]) {
                "Notifications" -> {
                    startActivity(Intent(this, NotificationsActivity::class.java))
                }
                "My Routes & Outlets" -> {
                    startActivity(Intent(this, ChangeOutlet::class.java))
                }
                "Reports" -> {
                    startActivity(Intent(this, Reports::class.java))
                }
                "Manifest" -> {
                    startActivity(Intent(this, ManifestActivity::class.java))
                }
                "Settings" -> {
                    startActivity(Intent(this, Settings::class.java))
                }
            }
        }

        when (BuildConfig.FLAVOR) {
            AppFlavours.DISTRIBUTR.rawValue -> {

                product_availability.setOnClickListener {
                    if (outlet.isNullOrBlank())
                        showOutletErrorMessage()
                    else
                        startActivity(Intent(this, ProductAvailability::class.java))
                }
                load_stock.setOnClickListener {
                    startActivity(Intent(this, LoadStock::class.java))
                }
                no_action.setOnClickListener {
                    if (!outlet.isNullOrBlank())
                        startActivity(Intent(this, NoActionActivity::class.java))
                    else
                        showOutletErrorMessage()
                }
                pick_outlet.setOnClickListener {
                    startActivity(Intent(this, OutletFinder::class.java))
                }

                tv_change_outlet.setOnClickListener {
                    startActivity(Intent(this, ChangeOutlet::class.java))
                }
                make_delivery.setOnClickListener {
                    startActivity(Intent(this, DeliveriesActivity::class.java))
                }
                menuIcon.setOnClickListener {
                    drawerLayout.openDrawer(GravityCompat.START)
                }

                wallet.setOnClickListener {
                    startActivity(Intent(this, Wallet::class.java))
                }

                make_sale.setOnClickListener {
                    if (outlet.isNullOrBlank()) {
                        showOutletErrorMessage()
                    } else {
                        val i = Intent(this, MakeSale::class.java)
                        i.putExtra("type", TransactionType.VAN_SALE.rawValue)
                        startActivity(i)
                    }
                }

                make_order.setOnClickListener {
                    if (outlet.isNullOrBlank()) {
                        showOutletErrorMessage()
                    } else {
                        val i = Intent(this, MakeSale::class.java)
                        i.putExtra("type", TransactionType.ORDER.rawValue)
                        startActivity(i)
                    }
                }

            }
            AppFlavours.MERCHANDIZR.rawValue -> {

                product_availability.setOnClickListener {
                    if (!outlet.isNullOrBlank())
                        startActivity(Intent(this, ProductAvailability::class.java))
                    else
                        showOutletErrorMessage()
                }

                pick_outlet.setOnClickListener {
                    startActivity(Intent(this, OutletFinder::class.java))
                }

                tv_change_outlet.setOnClickListener {
                    startActivity(Intent(this, ChangeOutlet::class.java))
                }
                menuIcon.setOnClickListener {
                    drawerLayout.openDrawer(GravityCompat.START)
                }

                wallet.setOnClickListener {
                    startActivity(Intent(this, Wallet::class.java))
                }

            }
            AppFlavours.HAULR.rawValue -> {

                load_stock.setOnClickListener {
                    startActivity(Intent(this, LoadStock::class.java))
                }

                pick_outlet.setOnClickListener {
                    startActivity(Intent(this, OutletFinder::class.java))
                }

                make_delivery.setOnClickListener {
                    startActivity(Intent(this, DeliveriesActivity::class.java))
                }
                menuIcon.setOnClickListener {
                    drawerLayout.openDrawer(GravityCompat.START)
                }

                wallet.setOnClickListener {
                    startActivity(Intent(this, Wallet::class.java))
                }
            }

            AppFlavours.RETAILR.rawValue -> {

            }
        }

        switchDataMode.isChecked =  PreferenceUtils.getBoolean(
            PreferenceUtils.DATA_SAVE_MODE,
            false
        )

        switchDataMode?.setOnCheckedChangeListener { _, isChecked ->
            PreferenceUtils.putBoolean(PreferenceUtils.DATA_SAVE_MODE, isChecked)
        }

        tv_sync_now.paintFlags = tv_last_synced.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        tv_sync_now.setOnClickListener {
            loadAppData()

        }
    }

    private fun loadAppData(){

        when (BuildConfig.FLAVOR) {

            AppFlavours.DISTRIBUTR.rawValue -> {
                loadAllProducts()
                loadIssues()
                loadRoutes()
                loadDeliveries()
                loadDiscountGroups()
                loadIncentiveOffers()
                loadInvoiceValueDiscounts()
                loadPromotionalOffers()
                loadOutletTypes()
                loadPricingTiers()
                loadEnums()
            }

            AppFlavours.RETAILR.rawValue -> {
                loadIssues()
                loadDiscountGroups()
                loadIncentiveOffers()
                loadInvoiceValueDiscounts()
                loadPricingTiers()
                loadEnums()
            }

            AppFlavours.HAULR.rawValue -> {
                loadAllProducts()
                loadIssues()
                loadDeliveries()
                loadOutletTypes()
                loadEnums()
            }

            AppFlavours.MERCHANDIZR.rawValue -> {
                loadAllProducts()
                loadEnums()
            }
        }
    }

    private fun loadEnums(){
        loadReturnStockStates()
    }

    //this is done hourly
//    private fun loadPersistentAppData(){
//        loadAllProducts()
//        loadIssues()
//        loadRoutes()
//        loadDeliveries()
//        loadDiscountGroupProducts()
//        loadDiscountGroups()
//        loadIncentiveOffers()
//        loadInvoiceValueDiscounts()
//        loadDiscountGroupProducts()
//        loadPromotionalOffers()
//        loadPricingTiers()
//    }

    private fun loadAllProducts(){

        disposable.add(
            restInterface.getAllProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    updateSyncTime()

                    for (i in it?.items!!) {

                        val existingProductItem = getExistingProductItem(i.id!!)

                        if (existingProductItem == null) {
                            val product = Product()
                            product.brand = i.brand
                            product.category = i.category
                            product.code = i.code
                            product.id = i.id
                            product.imageUrl = i.imageUrl
                            product.isActive = i.isActive
                            product.isFreemium = i.isFreemium
                            product.price = i.price
                            product.type = i.type
                            product.timestamp = i.timestamp
                            product.name = i.name
                            product.vatRate = i.vatRate
                            product.tierPrices = i.tierPrices

                            realm.beginTransaction()
                            realm.insertOrUpdate(product)
                            realm.commitTransaction()
                        } else {
                            existingProductItem.price = i.price
                            existingProductItem.category = i.category
                            existingProductItem.isActive = i.isActive
                            existingProductItem.category = i.category
                            existingProductItem.code = i.code
                            existingProductItem.id = i.id
                            existingProductItem.imageUrl = i.imageUrl
                            existingProductItem.isFreemium = i.isFreemium
                            existingProductItem.type = i.type
                            existingProductItem.timestamp = i.timestamp
                            existingProductItem.name = i.name
                            existingProductItem.vatRate = i.vatRate
                            existingProductItem.tierPrices = i.tierPrices

                            realm.beginTransaction()
                            realm.insertOrUpdate(existingProductItem)
                            realm.commitTransaction()
                        }
                    }
                }) {
                    println(it)
                })
    }

    private fun loadIssues(){

        disposable.add(
            restInterface.getIssues(id = PreferenceUtils.getUsersId() ?: "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    //check if we already have first issue note

                    if (it != null) {
                        for (i in it) {
                            realm.beginTransaction()
                            val issueNote = realm.where(IssueNote::class.java)
                                .equalTo("id", i.id)
                                .findFirst()
                            realm.commitTransaction()

                            if (issueNote == null) {
                                i.id?.let { it1 -> loadIssuedProducts(it1) }
                            }
                        }
                    }

                    if (it != null) {
                        for (i in it) {
                            realm.beginTransaction()
                            val issue = IssueNote()
                            issue.id = i.id
                            realm.copyToRealmOrUpdate(issue)
                            realm.commitTransaction()
                        }
                    }

                }) {
                    println(it)
                })
    }

    private fun loadIssuedProducts(id: Int){

        println("loading issue note id $id ")

        disposable.add(
            restInterface.getIssuedProducts(id = id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    for (i in it!!) {

                        println("productxxx")
                        println(i.name)
                        println(i.productQty)

                        val product = Product()
                        product.brand = i.brand
                        product.category = i.category
                        product.code = i.code
                        product.id = i.id
                        product.imageUrl = i.imageUrl
                        product.isActive = i.isActive
                        product.isFreemium = i.isFreemium
                        product.price = i.price
                        product.type = i.type
                        product.timestamp = i.timestamp
                        product.name = i.name
                        product.vatRate = i.vatRate
                        product.isIssued = true
                        product.tierPrices = i.tierPrices

                        val existingProductItem = getExistingProductItem(product.id!!)

                        if (existingProductItem != null) {
                            println("iwejfkoe")
                            println(existingProductItem.productQty)
                            println(existingProductItem.productQty?.plus(i.productQty!!))
                            val existingQuantity = existingProductItem.productQty ?: 0
                            product.productQty = existingQuantity
                                .plus(i.productQty!!)
                        }

                        println("iwejfkoew")
                        realm.beginTransaction()
                        realm.insertOrUpdate(product)
                        realm.commitTransaction()
                    }
                }) {
                    println(it)
                })
    }

    private fun getExistingProductItem(productId: Int) : Product? {
        realm.beginTransaction()
        val item: Product? =
            realm.where(Product::class.java).equalTo("id", productId).findFirst()
        realm.commitTransaction()
        return item
    }

    private fun loadRoutes(){

        disposable.add(
            restInterface.getRoutes(id = PreferenceUtils.getUsersId() ?: "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    if (it != null) {
                        for (i in it) {
                            val route =
                                Route()
                            route.code = i.code
                            route.description = i.description
                            route.id = i.id
                            route.name = i.name

                            realm.beginTransaction()
                            realm.copyToRealmOrUpdate(route)
                            realm.commitTransaction()

                            i.id?.let { it1 -> loadOutlets(it1) }
                        }
                    }
                }) {
                    println(it)
                })
    }


    private fun loadOutlets(routeId: Int){

        disposable.add(
            restInterface.getOutlets(id = routeId.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    for (i in it?.items!!) {
                        val outlet =
                            Outlet()
                        outlet.town = i.town
                        outlet.code = i.code
                        outlet.id = i.id
                        outlet.isActive = i.isActive
                        outlet.timestamp = i.timestamp
                        outlet.name = i.name
                        outlet.routeId = routeId
                        outlet.typeId = i.typeId

                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(outlet)
                        realm.commitTransaction()
                    }
                }) {
                    println(it)
                })
    }

    private fun loadDeliveries(){

        disposable.add(
            restInterface.getDeliveries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ it ->

                    for (i in it?.items!!) {
                        val delivery = Delivery()
                        delivery.customerName = i.customerName
                        delivery.deliveryBy = i.deliveryBy
                        delivery.id = i.id
                        delivery.invoiceId = i.invoiceId
                        delivery.isDelivered = i.isDelivered
                        delivery.refNo = i.refNo
                        delivery.timestamp = i.timestamp

                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(delivery)
                        realm.commitTransaction()
                    }
                }) {
                    println(it)
                })
    }


    private fun loadDiscountGroups(){

        disposable.add(
            restInterface.getDiscountGroups()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    for (i in it?.items!!) {
                        val discountGroup = DiscountGroup()
                        discountGroup.id = i.id
                        discountGroup.isActive = i.isActive
                        discountGroup.outletTypeId = i.outletTypeId

                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(discountGroup)
                        realm.commitTransaction()

                        i.id?.let { it1 -> loadDiscountGroupProducts(it1) }

                    }
                }) {
                    println(it)
                })
    }

    private fun loadDiscountGroupProducts(id: String){

        println("downloading discount grups")

        disposable.add(
            restInterface.getDiscountGroupProducts(id = id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    println(it?.items?.size)


                    for (i in it?.items!!) {
                        val discountGroupProduct = DiscountGroupProduct()
                        discountGroupProduct.id = i.id
                        discountGroupProduct.isActive = i.isActive
                        discountGroupProduct.expiryDate = i.expiryDate
                        discountGroupProduct.productId = i.productId
                        discountGroupProduct.rate = i.rate
                        discountGroupProduct.discountGroupId = i.discountGroupId

                        println("it?.items?.size")
                        println(discountGroupProduct.discountGroupId)
                        println("it?.items?.size")

                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(discountGroupProduct)
                        realm.commitTransaction()
                    }
                }) {
                    println(it)
                })
    }

    private fun loadIncentiveOffers(){

        disposable.add(
            restInterface.getIncentiveOffers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    for (i in it?.items!!) {
                        val incentiveOffer = IncentiveOffer()
                        incentiveOffer.id = i.id
                        incentiveOffer.isActive = i.isActive
                        incentiveOffer.expiryDate = i.expiryDate
                        incentiveOffer.offerProductId = i.offerProductId
                        incentiveOffer.minLimit = i.minLimit
                        incentiveOffer.productId = i.productId

                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(incentiveOffer)
                        realm.commitTransaction()

                    }
                }) {
                    println(it)
                })
    }

    private fun loadInvoiceValueDiscounts(){

        disposable.add(
            restInterface.getInvoiceValueDiscounts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    for (i in it?.items!!) {
                        val invoiceValueDiscount = InvoiceValueDiscount()
                        invoiceValueDiscount.id = i.id
                        invoiceValueDiscount.isActive = i.isActive
                        invoiceValueDiscount.expiryDate = i.expiryDate
                        invoiceValueDiscount.minInvoiceValue = i.minInvoiceValue
                        invoiceValueDiscount.hasExpired = i.hasExpired
                        invoiceValueDiscount.startDate = i.startDate
                        invoiceValueDiscount.percentageDiscount = i.percentageDiscount

                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(invoiceValueDiscount)
                        realm.commitTransaction()
                    }
                }) {
                    println(it)
                })
    }

    private fun loadPromotionalOffers(){

        disposable.add(
            restInterface.getPromotionOffers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    for (i in it?.items!!) {
                        val promotionalOffer = PromotionalOffer()
                        promotionalOffer.id = i.id
                        promotionalOffer.isActive = i.isActive
                        promotionalOffer.expiryDate = i.expiryDate
                        promotionalOffer.productID = i.productID
                        promotionalOffer.hasExpired = i.hasExpired
                        promotionalOffer.startDate = i.startDate

                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(promotionalOffer)
                        realm.commitTransaction()
                    }
                }) {
                    println(it)
                })
    }

    private fun loadPricingTiers(){

        disposable.add(
            restInterface.getPricingTiers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    for (i in it?.items!!) {
                        val pricingTier = PricingTier()
                        pricingTier.id = i.id
                        pricingTier.code = i.code
                        pricingTier.description = i.description
                        pricingTier.timestamp = i.timestamp
                        pricingTier.isActive = i.isActive
                        pricingTier.outletTypeId = i.outletTypeId

                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(pricingTier)
                        realm.commitTransaction()
                    }
                }) {
                    println(it)
                })
    }

    private fun loadOutletTypes(){

        disposable.add(
            restInterface.getOutletTypes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    for (i in it?.items!!) {
                        val outletType = OutletType()
                        outletType.id = i.id
                        outletType.code = i.code
                        outletType.timestamp = i.timestamp
                        outletType.isActive = i.isActive

                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(outletType)
                        realm.commitTransaction()
                    }
                }) {
                    println(it)
                })
    }

    private fun loadReturnStockStates(){

        disposable.add(
            restInterface.getReturnStates()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    //check if we already have that issue note
                    for (i in it) {
                        val item = ReturnStockItemState()
                        item.id = i.id
                        item.description = i.description
                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(item)
                        realm.commitTransaction()
                    }

                }) {
                    println(it)
                })
    }

    private fun getUserId(){

        disposable.add(
            restInterface.getUserDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    PreferenceUtils.putString(PreferenceUtils.FIELD_REP_ID, it?.id.toString())
                    loadAppData()
                }) {
                    println(it)
                    println("error fetching user details")
                })
    }

    private fun syncSales() {

            realm.beginTransaction()

            val sales = realm.where(Sale::class.java).equalTo("isSynced", false)
                .findAll()

            realm.commitTransaction()

            for (sale in sales) {

                val saleId = sale.id
                val saleBody = PostSaleBody()
                saleBody.outletId = sale.outletId?.toInt()
                saleBody.consumerId = sale.consumerId?.toInt()
                saleBody.refNo = sale.id
                saleBody.timeStamp = sale.date

                val invoiceBody = InvoiceBody()
                invoiceBody.payments = sale.invoice?.payments!!.map { it }
                invoiceBody.refNo = sale.invoice?.refNo

                saleBody.invoice = invoiceBody

                val saleItems = mutableListOf<ProductLineItem>()

                for (lineItem in sale.items) {
                    val saleProductItem = ProductLineItem()
                    saleProductItem.productId = lineItem?.productId
                    saleProductItem.quantity = lineItem?.quantity
                    saleProductItem.timestamp = sale.date
                    saleItems.add(saleProductItem)
                }

                saleBody.lineItems = saleItems

                if(sale.isSynced == true) { return }

                val post: Observable<JsonObject> = if(sale.type == TransactionType.ORDER.rawValue) {
                    restInterface.postOrder(saleBody)
                } else {
                    restInterface.postSale(saleBody)
                }

                disposable.add(
                    post
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ obj ->

                            updateSyncTime()

                            if (obj.get("isSuccess").asBoolean) {
                                //modify record as synced
                                realm.beginTransaction()
                                val saleItem =
                                    realm.where(Sale::class.java).equalTo("id", saleId)
                                        .findFirst()
                                saleItem?.isSynced = true
                                realm.copyToRealmOrUpdate(saleItem)
                                realm.commitTransaction()
                            }
                        }) { err ->
                            println("post sale errrr here")
                            println(err)
                        })
            }
    }

    @SuppressLint("SetTextI18n")
    private fun updateSyncTime(){
        tv_last_synced.text = "Last synced on ${
            Date()}"
    }

    private fun postReturnNotes() {

        realm.beginTransaction()

        val returnNotes = realm.where(StockReturn::class.java).equalTo("isSynced", false)
            .findAll()

        realm.commitTransaction()

        for (returnNote in returnNotes) {

            val returnNoteId = returnNote.id
            val returnNoteBody = ReturnNoteBody()
            returnNoteBody.timeStamp = returnNote.date
            returnNoteBody.fieldRepId = PreferenceUtils.getUsersId()

            val returnNoteItems = mutableListOf<ProductLineItem>()

            for (lineItem in returnNote.items) {
                val returnNoteProductItem = ProductLineItem()
                returnNoteProductItem.productId = lineItem?.productId
                returnNoteProductItem.quantity = lineItem?.quantity
                returnNoteProductItem.timestamp = returnNote.date
                returnNoteItems.add(returnNoteProductItem)
            }

            returnNoteBody.lineItems = returnNoteItems

            if(returnNote.isSynced == true) { return }

            val post: Observable<JsonObject> = restInterface.postReturns(returnNoteBody)

            disposable.add(
                post
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ obj ->

                        if (obj.get("isSuccess").asBoolean) {
                            //modify record as synced
                            realm.beginTransaction()
                            val returnNoteItem =
                                realm.where(StockReturn::class.java).equalTo("id", returnNoteId)
                                    .findFirst()
                            returnNoteItem?.isSynced = true
                            if (returnNoteItem != null) {
                                realm.copyToRealmOrUpdate(returnNoteItem)
                            }
                            realm.commitTransaction()
                        }
                    }) { err ->
                        println("post returnNote errrr here")
                        println(err)
                    })
        }
    }

    private fun showOutletErrorMessage(){
        Snackbar.make(
            drawer_layout,
            "You need to select an outlet to continue",
            Snackbar.LENGTH_SHORT
        ).show()
    }

    override fun onResume() {
        super.onResume()
        outlet = PreferenceUtils.getString(PreferenceUtils.CURRENT_OUTLET_NAME, null)

        if (outlet == null) {
            tv_change_outlet.text = "SELECT OUTLET"
            current_outlet.text = "No outlet selected"
        }
        else {
            current_outlet.text = outlet
        }

        if(!Distributr.instance.isMockEnvironment) {
            syncSales()
            postReturnNotes()
        }

        println("on resume called")
    }
}