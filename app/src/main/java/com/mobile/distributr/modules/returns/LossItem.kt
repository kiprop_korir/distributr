package com.mobile.distributr.modules.returns

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class LossItem : RealmObject() {

    @SerializedName("product")
    var productItems: RealmList<LossProductItem> =   RealmList()
    @PrimaryKey
    @SerializedName("date")
    var date: String? = null
    @SerializedName("outletId")
    var outletId: String? = null
}
