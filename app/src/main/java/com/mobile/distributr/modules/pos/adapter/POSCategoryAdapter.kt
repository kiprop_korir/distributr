package com.mobile.distributr.modules.pos.adapter;

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import kotlinx.android.synthetic.main.item_pos_category.view.*
import kotlinx.android.synthetic.main.pos_cart.view.*
import kotlin.coroutines.coroutineContext


class POSCategoryAdapter(
    private var dataSource: MutableList<String>,
    private val itemClickListener: ItemClickListener

) : RecyclerView.Adapter<POSCategoryAdapter.ViewHolder>() {

    companion object {
        var last_position = 0
    }

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    init {
        val sorted = dataSource.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it})
        val newDataSource: ArrayList<String> = arrayListOf()
        newDataSource.addAll(sorted)
        dataSource =  newDataSource
        dataSource.add(0,"ALL CATEGORIES")
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.tvTitle.text = dataSource[position]
        holder.itemView.setOnClickListener {
            last_position = position
            itemClickListener.itemClick(dataSource[position])
            notifyDataSetChanged()
        }
        if (last_position == position) {
            holder.itemView.tvTitle.setTextColor(holder.itemView.context.resources.getColor(R.color.colorPrimary))
            holder.itemView.background = holder.itemView.context.resources.getDrawable(R.drawable.bg_pos_category_selected)
        } else {
            holder.itemView.tvTitle.setTextColor(holder.itemView.context.resources.getColor(R.color.colorTextSecondary))
            holder.itemView.background = holder.itemView.context.resources.getDrawable(R.drawable.bg_pos_category)
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_pos_category, parent, false)
        return ViewHolder(v)
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n", "UseCompatLoadingForDrawables")
        fun bindItems( title: String, itemClickListener: ItemClickListener) {

        }
    }

    interface ItemClickListener {
        fun itemClick(title: String)
    }
}
