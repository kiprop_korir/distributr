package com.mobile.distributr.modules.pos

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.Result
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_scanner.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import com.mobile.distributr.R
import com.mobile.distributr.model.Product
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.common.model.CartItem
import io.realm.Realm
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.dialog_product_found.view.*


class ScannerActivity :AppCompatActivity(), ZXingScannerView.ResultHandler{

    private var mScannerView : ZXingScannerView? = null
    var products: ArrayList<Product> = arrayListOf()
    lateinit var realm: Realm
    private var cart: Cart? = null
    val type = "pos"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)
   supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tv_title.text = "Scan Code"

        mScannerView = ZXingScannerView(this)
        rl_scan.addView(mScannerView)
        mScannerView?.setResultHandler(this)
        mScannerView?.startCamera()
        mScannerView?.isSoundEffectsEnabled = true
        mScannerView?.setAutoFocus(true)

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }
        realm.beginTransaction()
        val realmProducts = realm.where(Product::class.java)
            .findAll()
        for (product in realmProducts) {
            products.add(product)
        }
        realm.commitTransaction()

        updateCart()
        cancel.setOnClickListener{
        finish()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentResult != null) {
            if (intentResult.contents == null) Toast.makeText(
                this,
                "Scan was cancelled",
                Toast.LENGTH_SHORT
            ).show() else Toast.makeText(
                this,
                intentResult.contents,
                Toast.LENGTH_SHORT
            ).show()
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

     override fun handleResult(rawResult: Result?) {
        // Toast.makeText(this, rawResult.toString(), Toast.LENGTH_LONG).show()

         val product = products[(0..products.size).random()]
         val cartItem = getExistingCartItem(product.code!!)
         val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_product_found, null)
         //AlertDialogBuilder
         val mBuilder = AlertDialog.Builder(this)
             .setView(mDialogView)
             .setTitle(product.name)
         //show dialog
         val  mAlertDialog = mBuilder.show()

         val quantity = cartItem?.quantity ?: 1

         mDialogView.quantity.setText(quantity.toString())

         //place cursor end of edit text
         mDialogView.quantity.setSelection(mDialogView.quantity.text.length)

         //mDialogView.quantity.requestFocus()
         //show keyboard
         //showSoftKeyboard(mDialogView.ed_quantity)//login button click of custom layout
         mDialogView.btn_add.setOnClickListener {
             //dismiss dialog
             mAlertDialog.dismiss()
             addToCart(cartItem, product, mDialogView.quantity.text.toString().toInt())
         }
         mDialogView.btn_cancel.setOnClickListener{
             mAlertDialog.dismiss()
             mScannerView?.startCamera()
             mScannerView?.setResultHandler(this)
         }
     }

fun getExistingCartItem(productCode:String) : CartItem? {
    var item: CartItem? = null
    realm.executeTransaction() {
        item = realm.where(CartItem::class.java).equalTo("productCode", productCode).equalTo("type",type).findFirst()
    }
    return item
}

    fun addToCart(existingCartItem: CartItem?, product: Product, quantity:Int){

        if(existingCartItem == null) {

            val cartItem = CartItem()
            cartItem.product = product
            cartItem.quantity = quantity
            cartItem.productId = product.id

            val vat = quantity * product.price!! * 0.16
            cartItem.VAT = vat
            val amount = quantity * product.price!!
            cartItem.amount = amount
            cartItem.amountTotal = amount + vat

            realm.executeTransaction() {
                cart!!.items.add(cartItem)
                realm.copyToRealmOrUpdate(cart)
            }
            showSuccessSnackBar("added to cart")
            Handler().postDelayed({
                finish()
            }, 1000)

        }
        else {

            realm.beginTransaction()

            val item: CartItem? =  realm.where(
                CartItem::class.java).equalTo("productCode", product.code).findFirst()
            item?.quantity = quantity
            realm.copyToRealmOrUpdate(item)
            realm.commitTransaction()
            showSuccessSnackBar("updated")
        }
        updateCart()
    }

    private fun updateCart(){

        realm.beginTransaction()
        cart = realm.where(Cart::class.java).equalTo("type", type).equalTo("outletId", type).findFirst()

        if (cart == null) {
            cart = Cart()
            cart!!.type = type
            cart!!.outletId = type
        }
        realm.commitTransaction()
    }

    private fun showSuccessSnackBar(description: String){
        Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
    }
 }