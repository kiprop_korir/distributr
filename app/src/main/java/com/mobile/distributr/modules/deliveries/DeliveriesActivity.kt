package com.mobile.distributr.modules.deliveries

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.deliveries.adapter.DeliveryAdapter
import com.mobile.distributr.modules.deliveries.model.Delivery
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_make_delivery.*
import kotlinx.android.synthetic.main.app_bar.*


class DeliveriesActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    lateinit var realm: Realm
    var deliveries: ArrayList<Delivery> = arrayListOf()
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_make_delivery)
         supportActionBar?.setDisplayHomeAsUpEnabled(true)
        tv_title.text = "MAKE DELIVERY"
        loadData()

        btn_back.setOnClickListener{
            finish()
        }
    }
    
    private fun loadData(){
        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        realm.beginTransaction()
        val objects = realm.where(Delivery::class.java)
            .findAll()
        for (delivery in objects) {
            deliveries.add(delivery)
        }
        realm.commitTransaction()

        if(objects.count() == 0 )
            empty_text.isVisible = true

        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.adapter = DeliveryAdapter(deliveries)
    }

}