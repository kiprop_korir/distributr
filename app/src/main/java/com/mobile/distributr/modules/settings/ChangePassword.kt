package com.mobile.distributr.modules.settings

import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import kotlinx.android.synthetic.main.activity_change_password.*


import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.app_bar.toolbar

class ChangePassword : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        tv_title.text = "CHANGE PASSWORD"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        confirm.setOnClickListener{
            showSuccessSnackBar("Password changed successfully")
            Handler().postDelayed({
                finish()
            }, 1000)
        }

        btn_back.setOnClickListener{
            finish()
        }
    }

    private fun showSuccessSnackBar(description: String){
        Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
    }
}
