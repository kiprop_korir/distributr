package com.mobile.distributr.modules.loadstock.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.loadstock.model.LoadStockProductItem
import kotlinx.android.synthetic.main.item_summary.view.*
import java.math.RoundingMode
import java.text.DecimalFormat


class LoadStockItemAdapter(
    private val context: Context,
    private val dataSource: ArrayList<LoadStockProductItem>,
    private val clickListener: LoadStockItemAdapter.ItemClickListener

) :  RecyclerView.Adapter<LoadStockItemAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return dataSource.count()

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position], clickListener)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_summary, parent, false)
        return ViewHolder(v)
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: LoadStockProductItem, clickListener: ItemClickListener) {
            itemView.name.text = item.product?.name
            itemView.amount.visibility  = View.INVISIBLE
            itemView.quantity.text = item.quantity.toString()
            itemView.quantity.paintFlags = itemView.quantity.paintFlags or Paint.UNDERLINE_TEXT_FLAG

            itemView.quantity.setOnClickListener{
                clickListener.changeQuantity(item)
            }
        }

        private fun roundOffDecimal(number: Double): Double? {
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            return df.format(number).toDouble()
        }
    }

    interface ItemClickListener {
        fun changeQuantity(item: LoadStockProductItem)
    }
}
