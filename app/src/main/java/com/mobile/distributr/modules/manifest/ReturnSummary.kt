package com.mobile.distributr.modules.manifest

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.modules.common.TransactionResultActivity
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.model.InvoiceItem
import com.mobile.distributr.model.PaymentItem
import com.mobile.distributr.model.Product
import com.mobile.distributr.model.ProductLineItem
import com.mobile.distributr.modules.manifest.model.StockReturnItem
import com.mobile.distributr.modules.manifest.adapter.StockSummaryItemAdapter
import com.mobile.distributr.modules.manifest.model.StockReturn
import com.mobile.distributr.modules.reports.model.StockReportProductItem
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_cart.rv_products
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_sale_summary.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.dialog_add_to_cart.view.*
import kotlinx.android.synthetic.main.item_product.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ReturnSummary : AppCompatActivity() {

    private lateinit var state: State

    @ExperimentalStdlibApi
    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_returns_summary)

        state = State()

        state.type = intent.getStringExtra("type")!!
        state.outletName = PreferenceUtils.getString(PreferenceUtils.CURRENT_OUTLET_NAME, null).toString()
        state.outletId = PreferenceUtils.getInt(PreferenceUtils.CURRENT_OUTLET_ID, 0).toString()

        loadViews()

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        getCartItems()
        calculateTotalItems()

    }

    @ExperimentalStdlibApi
    @SuppressLint("SetTextI18n")
    private fun loadViews(){

        tv_title.text = state.type.capitalize(Locale.ROOT) + " Summary"

        date.text = Date().toString()

        complete.setOnClickListener {
            saveReturnStockItem()
            updateProductList()

            clearCartData()

            val intent = Intent(this, TransactionResultActivity::class.java)
            intent.putExtra("type", state.type)
            intent.putExtra("beneficiary", state.outletName)
            intent.putExtra("invoice_value", state.totalItems.toDouble())
            intent.putExtra("total_items", state.totalItems)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }

        val adapter =
            StockSummaryItemAdapter(
                this,
                state.cartItemsList,
                object :
                    StockSummaryItemAdapter.ClickListener {

                    @SuppressLint("SetTextI18n")
                    override fun changeQuantity(item: CartItem, quantity: Int) {
                        val mDialogView = LayoutInflater.from(this@ReturnSummary)
                            .inflate(R.layout.dialog_add_to_cart, null)
                        //AlertDialogBuilder
                        val mBuilder = AlertDialog.Builder(this@ReturnSummary)
                            .setView(mDialogView)
                            .setTitle(item.product!!.name)
                        //show dialog
                        val mAlertDialog = mBuilder.show()

                        val newQuantity = item.quantity ?: 1

                        mDialogView.edQuantity.setText(newQuantity.toString())

                        //place cursor end of edit text
                        mDialogView.edQuantity.setSelection(mDialogView.edQuantity.text.length)

                        mDialogView.btn_add.text = "EDIT"

                        mDialogView.btn_add.setOnClickListener {
                            //dismiss dialog
                            mAlertDialog.dismiss()
                            editCartItem(item, mDialogView.edQuantity.text.toString().toInt())
                        }
                    }
                    override fun deleteFromCart(item: CartItem, position: Int) {
                        //
                        state.realm.beginTransaction()
                        item.deleteFromRealm()
                        state.realm.commitTransaction()
                        //state.realm.close()
                        getCartItems()
                        rv_products.adapter?.notifyDataSetChanged()
                        calculateTotalItems()
                    }
                })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        btn_back.setOnClickListener{
            finish()
        }
    }

    private fun updateProductList() {
        //TODO: Clean this code below

        //update local product list
        var currentProduct: Product?
        for (item in state.cartItems!!) {
            state.realm.beginTransaction()
            currentProduct =
                state.realm.where(Product::class.java).equalTo("id", item?.productId!!)
                    .findFirst()

            val currentQty: Int = currentProduct?.productQty ?: 0
            val newQuantity = item.quantity?.let { currentQty.minus(it) }
            if (currentProduct != null) {
                currentProduct.productQty = newQuantity
            }
            state.realm.copyToRealmOrUpdate(currentProduct!!)


            state.realm.commitTransaction()

            //update stock take list

            state.realm.beginTransaction()
            val stockProductItem = StockReportProductItem()
            stockProductItem.product = item.product
            stockProductItem.productId = item.productId
            stockProductItem.activityDescription = state.type
            stockProductItem.quantity = currentProduct.productQty
            stockProductItem.quantityChange = item.quantity
            state.realm.copyToRealmOrUpdate(stockProductItem)
            state.realm.commitTransaction()
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun saveReturnStockItem(){

        val f = SimpleDateFormat("yyyy-MMM-dd HH:mm:ss")
        val dateString = f.format(Date())

        state.stockReturn = StockReturn()
        state.stockReturn.date = dateString

        val returnStockItems = RealmList<StockReturnItem?>()

        for (lineItem in state.cartItems!!){
            val returnItemProductItem = StockReturnItem()
            returnItemProductItem.productId = lineItem?.product?.id
            returnItemProductItem.quantity = lineItem?.quantity
            returnItemProductItem.product = lineItem?.product
            returnStockItems.add(returnItemProductItem)
        }

        state.stockReturn.items = returnStockItems

        state.realm.executeTransaction {
            state.realm.copyToRealmOrUpdate(state.stockReturn)
        }
    }

    private fun clearCartData(){
        //clear cart
        state.realm.executeTransaction{
            state.cart?.deleteFromRealm()
            val cartItems = state.realm.where(CartItem::class.java).equalTo("type",state.type)
            .findAll()
            cartItems?.deleteAllFromRealm()
        }
    }

    fun editCartItem(item: CartItem?, quantity:Int){
        state.realm.beginTransaction()

        item?.quantity = quantity
        state.realm.copyToRealmOrUpdate(item)
        state.realm.commitTransaction()
        showSuccessSnackBar()

        updateCart()
        rv_products.adapter?.notifyDataSetChanged()
        calculateTotalItems()
    }

    private fun showSuccessSnackBar(){
        val snackBar = Snackbar.make(toolbar,"Product updated successfully", Snackbar.LENGTH_SHORT)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(resources.getColor(R.color.green_400, null))
        snackBar.show()
    }
    private fun updateCart(){

        state.realm.beginTransaction()
        state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type).findFirst()

        if (state.cart == null) {
            state.cart = Cart()
            state.cart!!.type = state.type
        }
        state.realm.commitTransaction()

        val count = state.cart?.items?.count() ?: 0

        if (count > 0) {
            cart_count.text = count.toString()
            cart_count.visibility = View.VISIBLE
        } else
            cart_count.visibility = View.GONE
    }

    private fun getCartItems(){

        state.cartItemsList.clear()

        state.realm.beginTransaction()
        if (state.type == TransactionType.POS.rawValue) state.outletId = "POS"

        state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type).findFirst()

        state.cartItems = state.cart?.items
        state.realm.commitTransaction()

        if (state.cartItems != null) {
            for (item in state.cartItems!!)
                state.cartItemsList.add(item!!)
        }
    }

    private fun calculateTotalItems() {

        var totalItems = 0
        if (state.cartItems?.count()!! > 0) {

            for (item in state.cartItems!!){
                totalItems += item?.quantity!!
            }
        }
        state.totalItems = totalItems
        tvTotal.text = totalItems.toString()
    }


    private  class State {
        lateinit var realm: Realm
        var outletId = ""
        var outletName = ""
        lateinit var type: String
        var cart: Cart? = null
        var cartItemsList = ArrayList<CartItem>()
        var cartItems : RealmList<CartItem?>? = null
        var totalItems = 0
        lateinit var stockReturn: StockReturn
    }
}
