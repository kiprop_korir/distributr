package com.mobile.distributr.modules.reports

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.reports.adapter.StockReportAdapter
import com.mobile.distributr.modules.reports.model.StockReportProductItem
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_stock_reports.*
import kotlinx.android.synthetic.main.app_bar.*
import java.util.*

open class StockReports :AppCompatActivity() {

    var products: ArrayList<StockReportProductItem> = arrayListOf()
    lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock_reports)

        tv_title.text = "STOCK REPORT"

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        realm.beginTransaction()
        val realmProducts = realm.where(StockReportProductItem::class.java)
            .findAll()
        for (product in realmProducts) {
            products.add(product)
        }
        realm.commitTransaction()

        val adapter = StockReportAdapter(this,products)

            rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            rv_products.adapter = adapter

        if(products.size == 0)
            empty.visibility = View.VISIBLE

        btn_back.setOnClickListener{
            finish()
        }

    }
}