package com.mobile.distributr.modules.notifications.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

open class DistributrNotification : RealmObject() {

    @SerializedName("message")
    var message: String? = null
    @PrimaryKey
    @SerializedName("date")
    var date: String? = null
    @SerializedName("status")
    var status: String? = null
    @SerializedName("title")
    var title: String? = null
}
