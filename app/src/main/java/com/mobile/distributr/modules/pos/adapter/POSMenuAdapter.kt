package com.mobile.distributr.modules.pos.adapter;

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity


class POSMenuAdapter(
    private val context: Context,
    private val dataSource: Array<Pair<String, Int>>
) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.item_pos_item, parent, false)
        val textView = rowView.findViewById<TextView>(R.id.title)
        val img = rowView.findViewById<ImageView>(R.id.icon)
        textView.text = dataSource[position].first
        Glide.with(context)
            .load(dataSource[position].second)
            .into(img)

        return rowView
    }

}
