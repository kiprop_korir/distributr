package com.mobile.distributr.modules.product_availability

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.adapter.ProductAdapter
import com.mobile.distributr.helper.ProductContextType
import com.mobile.distributr.model.*
import com.mobile.distributr.modules.product_availability.model.ProductAvailabilityItem
import com.mobile.distributr.utils.PreferenceUtils
import com.mobile.distributr.utils.PreferenceUtils.CURRENT_OUTLET_ID
import com.mobile.distributr.utils.PreferenceUtils.CURRENT_OUTLET_NAME
import io.realm.Realm
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.content_view_products.*
import kotlin.collections.ArrayList

class ProductAvailability : AppCompatActivity() {

    var products: ArrayList<Product> = arrayListOf()
    lateinit var realm: Realm
    var productAvailabilityItem: ProductAvailabilityItem? = null
    var outletId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_make_sale)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tv_title.text = "PRODUCT AVAILABILITY"

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        val outletName = PreferenceUtils.getString(CURRENT_OUTLET_NAME, "")
        outletId = PreferenceUtils.getInt(CURRENT_OUTLET_ID, 0).toString()

        productAvailabilityItem = getExistingItem()
        realm.beginTransaction()

        val realmProducts = realm.where(Product::class.java)
            .findAll()

        for (product in realmProducts) {
            products.add(product)
        }

        realm.commitTransaction()

        val adapter = ProductAdapter(this,products, ProductContextType.OTHER,object : ProductAdapter.ItemClickListener {
            override fun itemClick(product: Product) {
                val intent = Intent(applicationContext, ImageCaptureActivity::class.java)
                //intent.putExtra("product", realm.copyFromRealm(product) )
                startActivity(intent)
            }
        })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        complete.setOnClickListener {
            val intent = Intent(this, ProductAvailabilitySummary::class.java)
            startActivity(intent)
        }

        // Add Text Change Listener to EditText
        edSearch.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                // Call back the Adapter with current character to Filter
                this@ProductAvailability.runOnUiThread(Runnable {
                    adapter.applyFilter(s.toString())
                })
            }

            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {}
        })


        btn_back.setOnClickListener{
            onBackPressed()
        }
    }
    private fun getExistingItem() : ProductAvailabilityItem? {
        var item: ProductAvailabilityItem? = null

        realm.executeTransaction() {

            item = realm.where(ProductAvailabilityItem::class.java).equalTo("outletId", outletId).findFirst()
                ?: ProductAvailabilityItem()
            item!!.outletId = outletId
        }
        return item
    }

    override fun onResume() {
        super.onResume()
        productAvailabilityItem = getExistingItem()

        if(productAvailabilityItem?.productItems?.size!! > 0){
            complete.visibility = View.VISIBLE
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }
    override fun onBackPressed() {
        // super.onBackPressed()
        if (productAvailabilityItem?.productItems?.count()!! > 0) {
            val builder = androidx.appcompat.app.AlertDialog.Builder(this)

            builder.setTitle("Cancel Report")
            builder.setMessage("Are you sure you want to cancel this transaction?")

            builder.setPositiveButton("Yes") { _, _ ->

                finish()
            }

            builder.setNegativeButton("No", null)

            val dialog = builder.create()
            dialog.show()
    } else {
        finish()
        }
    }
}