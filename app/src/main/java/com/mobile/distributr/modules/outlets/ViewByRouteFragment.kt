package com.mobile.distributr.modules.outlets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobile.distributr.R
import com.mobile.distributr.modules.outlets.model.Outlet
import com.mobile.distributr.modules.outlets.helper.OutletSection
import com.mobile.distributr.modules.outlets.helper.OutletSection.ItemClickListener
import com.mobile.distributr.modules.outlets.model.Route
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_view_by_route.*


class ViewByRouteFragment: Fragment(){

    lateinit var realm: Realm
    var outlets: ArrayList<Outlet> = arrayListOf()
    var routes: ArrayList<Route> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_view_by_route, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(context)
            Realm.getDefaultInstance()
        }

        realm.executeTransaction{
            val realmOutlets = realm.where(Outlet::class.java)
                .findAll()
            for (outlet in realmOutlets) {
                outlets.add(outlet)
            }

            val realmRoutes = realm.where(Route::class.java)
                .findAll()
            for (route in realmRoutes) {
                routes.add(route)
            }
        }


        // Create an instance of SectionedRecyclerViewAdapter
        val sectionAdapter = SectionedRecyclerViewAdapter()

        // Add your Sections

        for (route in routes) {
            val section = OutletSection()
            val filteredOutlets: ArrayList<Outlet> = arrayListOf()

            for (outlet in outlets) {
                if (outlet.routeId == route.id) {
                    filteredOutlets.add(outlet)
                }
            }

            section.route = route
            section.outlets = filteredOutlets
            section.itemClickListener = object : ItemClickListener {
                override fun itemClick() {
                    activity?.finish()
                }
            }

            if(filteredOutlets.count() > 0)
            sectionAdapter.addSection(section)
        }

// Set up your RecyclerView with the SectionedRecyclerViewAdapter

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = sectionAdapter

    }

}