package com.mobile.distributr.modules.product_availability.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.product_availability.model.ProductAvailabilityProductItem
import kotlinx.android.synthetic.main.item_product_availability_summary.view.*
import kotlinx.android.synthetic.main.item_summary.view.amount
import kotlinx.android.synthetic.main.item_summary.view.name
import java.math.RoundingMode
import java.text.DecimalFormat


class ProductAvailabilityItemAdapter(
    private val context: Context,
    private val dataSource: ArrayList<ProductAvailabilityProductItem>,
    private val clickListener: ProductAvailabilityItemAdapter.ItemClickListener

) :  RecyclerView.Adapter<ProductAvailabilityItemAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return dataSource.count()

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position], clickListener)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_product_availability_summary, parent, false)
        return ViewHolder(v)
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: ProductAvailabilityProductItem, clickListener: ItemClickListener) {
            itemView.name.text = item.product?.name
            itemView.available.paintFlags = itemView.available.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            Glide.with(itemView.context).load(item.imageLocation).into(itemView.image);
            itemView.amount.visibility = View.INVISIBLE
            if(item.available)
                itemView.available.text = "YES"
            else
                itemView.available.text = "NO"
            itemView.available.setOnClickListener{
                clickListener.changeQuantity(item)
            }
        }

        private fun roundOffDecimal(number: Double): Double? {
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            return df.format(number).toDouble()
        }
    }

    interface ItemClickListener {
        fun changeQuantity(item: ProductAvailabilityProductItem)
    }
}
