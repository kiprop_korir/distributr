package com.mobile.distributr.modules.reports

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import kotlinx.android.synthetic.main.app_bar.*

class PendingOutlets : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pending_outlets)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        tv_title.text = "PENDING OUTLETS"

        btn_back.setOnClickListener{
            finish()
        }

    }

}
