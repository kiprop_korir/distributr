package com.mobile.distributr.modules.outlets

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.cart.CartActivity

import kotlinx.android.synthetic.main.activity_new_outlet.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.content_new_outlet.*

class NewOutlet : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_outlet)
        tv_title.text = "Add New Outlet"


        btnNext.setOnClickListener{
            val route = route.selectedItem.toString()
            val name = outlet_name.text.toString()
            val roadName = road.text.toString()
            val code = outlet_code.text.toString()
            val type = outlet_type.selectedItem.toString()
            val category = outlet_category.selectedItem.toString()

            val intent = Intent(this, CartActivity::class.java)
            intent.putExtra("route" , route)
            intent.putExtra("road" , roadName)
            intent.putExtra("code" , code)
            intent.putExtra("category" , category)
            intent.putExtra("type", type)
            startActivity(intent)

        }
        btn_back.setOnClickListener{
            finish()
        }

    }

}
