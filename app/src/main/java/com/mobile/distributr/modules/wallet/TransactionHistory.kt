package com.mobile.distributr.modules.wallet

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.wallet.adapter.ViewPagerAdapter
import com.mobile.distributr.modules.wallet.fragments.MonthlyHistoryFragment
import com.mobile.distributr.modules.wallet.fragments.TodayHistoryFragment
import com.mobile.distributr.modules.wallet.fragments.WeeklyHistoryFragment
import kotlinx.android.synthetic.main.app_bar.*


class   TransactionHistory : AppCompatActivity() {

    private lateinit var viewpager: ViewPager
    private lateinit var tabs: TabLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_history)
        tv_title.text = "MY TRANSACTIONS"

        initViews()

        setupViewPager()

        btn_back.setOnClickListener{
            finish()
        }
    }

    private fun initViews() {
        tabs = findViewById(R.id.tabs)
        viewpager = findViewById(R.id.viewPager)
    }


    private fun setupViewPager() {

        val adapter = ViewPagerAdapter(supportFragmentManager)

        var first = TodayHistoryFragment ()
        var second = WeeklyHistoryFragment ()
        var third = MonthlyHistoryFragment()

        adapter.addFragment(first, "TODAY")
        adapter.addFragment(second, "THIS WEEK")
        adapter.addFragment(third, "THIS MONTH")

        viewpager.adapter = adapter

        tabs.setupWithViewPager(viewpager)

    }
}