package com.mobile.distributr.modules.cart.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.Product
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class OrderCart : RealmObject() {


    @SerializedName("id")
    var saleID: String? = null
    @SerializedName("products")
    var products: RealmList<Product?> = RealmList()
    @SerializedName("total")
    var total: Double? = 0.00
    @PrimaryKey
    @SerializedName("time")
    var time: String? = null
}
