package com.mobile.distributr.modules.manifest.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.InvoiceItem
import com.mobile.distributr.model.ProductLineItem
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class StockReturn : RealmObject() {
    @SerializedName("items")
    var items: RealmList<StockReturnItem?> = RealmList()
    @SerializedName("id")
    var id: String? = null
    @PrimaryKey
    @SerializedName("timestamp")
    var date: String? = null
    var isSynced: Boolean? = false
}
