package com.mobile.distributr.modules.outlets

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.SupportMapFragment
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import kotlinx.android.synthetic.main.app_bar.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import com.mobile.distributr.modules.outlets.model.Outlet
import com.google.android.gms.maps.model.CameraPosition
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.provider.Settings
import android.widget.Toast
import android.os.Looper
import androidx.core.app.ActivityCompat.*
import com.google.android.gms.maps.model.Marker
import com.mobile.distributr.R
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_outlet_details.get_directions
import kotlinx.android.synthetic.main.activity_outlet_finder.*
import com.google.android.gms.location.*

class OutletFinder : AppCompatActivity(), OnMapReadyCallback {

    lateinit var realm: Realm
    var outlets: ArrayList<Outlet> = arrayListOf()
    private var selectedMarker : Marker? = null
    var selectedOutletID : String? = null
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    val PERMISSION_ID = 42
    lateinit var googleMapObj: GoogleMap
    
        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_outlet_finder)

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

            getLastLocation()

            tv_title.text = "OUTLET FINDER"

            realm = try {
                Realm.getDefaultInstance()
            } catch (e: Exception) {
                Realm.init(this)
                Realm.getDefaultInstance()
            }

            realm.beginTransaction()
            val realmOutlets = realm.where(Outlet::class.java)
                .findAll()
            for (outlet in realmOutlets) {
                outlets.add(outlet)
            }
            realm.commitTransaction()
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
            get_directions.setOnClickListener {

                val builder = Uri.Builder()
                builder.scheme("https")
                    .authority("www.google.com")
                    .appendPath("maps")
                    .appendPath("dir")
                    .appendPath("")
                    .appendQueryParameter("api", "1")
                    .appendQueryParameter(
                        "destination",
                        selectedMarker?.position?.latitude.toString() + "," + selectedMarker?.position?.longitude.toString()
                    )
                val url = builder.build().toString()

                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)

            }

            view_details.setOnClickListener {
                val i = Intent(this, OutletDetails::class.java)
                i.putExtra("outletId", selectedMarker?.snippet)
                startActivity(i)
            }

            btn_back.setOnClickListener{
                finish()
            }
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        if (googleMap != null) {
            googleMapObj = googleMap
        }

        var locations: ArrayList<LatLng> = arrayListOf()

        for (outlet in outlets) {
            val location = LatLng(outlet.latitude ?: 0.0, outlet.longitude ?: 0.0)
            locations.add(location)

            val m =   MarkerOptions().position(location)
                .title(outlet.name)

            m.snippet(outlet.id.toString())

            googleMapObj.addMarker(m)
        }

        zoomToCurrentLocation(googleMapObj, locations[0])

    }

    private fun zoomToCurrentLocation(googleMap: GoogleMap, location: LatLng){
        val cameraPosition = CameraPosition.Builder().target(location).zoom(15.0f).build()
        val cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
        googleMap.moveCamera(cameraUpdate)
        googleMap.setOnMarkerClickListener { marker ->
            info.visibility = View.VISIBLE
            selectedMarker = marker
            false
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                       zoomToCurrentLocation(googleMapObj, location = LatLng(location.latitude, location.longitude))
                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            zoomToCurrentLocation(googleMapObj, location = LatLng(mLastLocation.latitude, mLastLocation.longitude))
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_ID
        )
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }

}
