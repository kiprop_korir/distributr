package com.mobile.distributr.modules.invoice

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.modules.common.TransactionResultActivity
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.common.service.DiscountService
import com.mobile.distributr.common.service.NumberService
import com.mobile.distributr.common.service.TransactionService
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.model.InvoiceItem
import com.mobile.distributr.model.PaymentItem
import com.mobile.distributr.model.Product
import com.mobile.distributr.model.ProductLineItem
import com.mobile.distributr.modules.invoice.adapter.InvoiceItemAdapter
import com.mobile.distributr.modules.reports.model.StockReportProductItem
import com.mobile.distributr.modules.sale.model.Sale
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_invoice.*
import kotlinx.android.synthetic.main.activity_payment.complete
import kotlinx.android.synthetic.main.app_bar.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class InvoiceActivity : AppCompatActivity() {

    private lateinit var state: State

    @ExperimentalStdlibApi
    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invoice)

        state = State()

        state.type = intent.getStringExtra("type")!!
        state.balance = intent.getDoubleExtra("balance", 0.0)
        state.paymentMade = intent.getDoubleExtra("amount_received", 0.0)
        state.paymentType = intent.getStringExtra("payment_type")
        state.refNo = intent.getStringExtra("ref_no")

        state.outletName =
            PreferenceUtils.getString(PreferenceUtils.CURRENT_OUTLET_NAME, null).toString()
        state.outletId =
            PreferenceUtils.getInt(PreferenceUtils.CURRENT_OUTLET_ID, 0).toString()

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        getCartItems()
        setViews()
        calculateTotals()

        val adapter = InvoiceItemAdapter(
            this,
            state.cartItemsList)

        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapter

        btn_back.setOnClickListener{
            finish()
        }
    }

    @SuppressLint("SetTextI18n")
    @ExperimentalStdlibApi
    private fun setViews(){

        tv_title.text = "${state.type.capitalize(Locale.ROOT)} INVOICE"
        ivPrinter.visibility = View.VISIBLE
        tv_invoice_number.text = "INV-${Date().day}${Date().month}${Date().year}${Date().seconds}"
        tv_invoice_to.text = state.outletName
        val f = SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.US)
        val dateString = f.format(Date())
        tv_invoice_date.text = dateString

        complete.setOnClickListener {

            val builder = androidx.appcompat.app.AlertDialog.Builder(this)

            builder.setTitle("Print Invoice")
            builder.setMessage("Do you want to print an invoice for this transaction?")

            builder.setPositiveButton("Yes") { _, _ ->
                saveSale()
                Snackbar.make(layout,"Printing is coming soon", Snackbar.LENGTH_SHORT).show()
            }

            builder.setNegativeButton("No, proceed") { _, _ ->
                saveSale()
                clearCartData()

                val intent = Intent(this, TransactionResultActivity::class.java)
                intent.putExtra("type", state.type)
                intent.putExtra("beneficiary", state.outletName)
                intent.putExtra("invoice_value", state.netAmount)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)

            }

            val dialog = builder.create()
            dialog.show()

        }
    }
    
    @SuppressLint("SimpleDateFormat")
    private fun saveSale(){

        val f = SimpleDateFormat("yyyy-MMM-dd HH:mm:ss")
        val dateString = f.format(Date())

        state.sale = Sale()
        state.sale.date = dateString
        state.sale.id = dateString
        state.sale.type = state.type
        state.sale.amount = state.netAmount
        state.sale.outletName = state.outletName
        state.sale.outletId = state.outletId
        state.sale.valueDiscounts = 0.0
        state.sale.productDiscounts = state.cart?.productDiscounts
        state.sale.paymentMade = state.paymentMade
        state.sale.state = "0"
        if (state.type == TransactionType.POS.rawValue) state.sale.consumerId = "0" else
            state.sale.outletId = state.outletId

        val invoiceItem = InvoiceItem()
        invoiceItem.refNo = dateString.toString()

        val paymentItem = PaymentItem()
        paymentItem.amount = state.paymentMade
        paymentItem.paymentOption = state.paymentType
        paymentItem.refNo = dateString.toString()
        paymentItem.timestamp = dateString.toString()

        invoiceItem.payments.add(paymentItem)

        state.sale.invoice = invoiceItem

        val saleItems = RealmList<ProductLineItem?>()

        for (lineItem in state.cartItems!!){
            val saleProductItem = ProductLineItem()
            saleProductItem.productId = lineItem?.product?.id
            saleProductItem.quantity = lineItem?.quantity
            saleProductItem.timestamp = dateString
            saleItems.add(saleProductItem)
        }

        state.sale.items = saleItems

        state.realm.executeTransaction {
            state.realm.copyToRealmOrUpdate(state.sale)
        }

        //TODO: Clean this code below


        if(state.type != TransactionType.ORDER.rawValue) {
            //update local product list
            var currentProduct: Product?
            for (item in state.cartItems!!) {
                state.realm.beginTransaction()
                currentProduct =
                    state.realm.where(Product::class.java).equalTo("id", item?.productId!!)
                        .findFirst()

                if (currentProduct != null) {
                    currentProduct.productQty = item.quantity?.let { it1 ->
                        currentProduct.productQty?.minus(
                            it1
                        )
                    }
                    state.realm.copyToRealmOrUpdate(currentProduct!!)
                }

                state.realm.commitTransaction()

                //update stock take list

                state.realm.beginTransaction()
                val stockProductItem = StockReportProductItem()
                stockProductItem.product = item?.product
                stockProductItem.productId = item?.productId
                stockProductItem.activityDescription = state.type
                stockProductItem.quantity = currentProduct?.productQty
                stockProductItem.quantityChange = item?.quantity
                state.realm.copyToRealmOrUpdate(stockProductItem)
                state.realm.commitTransaction()
            }
        }

    }

    private fun clearCartData(){
        //clear cart
        state.realm.executeTransaction{
            state.cart?.deleteFromRealm()
            val cartItems = state.realm.where(CartItem::class.java).equalTo("type",state.type).
            equalTo("outletId",state.outletId).findAll()
            cartItems?.deleteAllFromRealm()
        }
    }

    private fun getCartItems() {

        state.cartItemsList.clear()

        state.realm.beginTransaction()
        if (state.type == TransactionType.POS.rawValue) state.outletId = "POS"

        state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type).equalTo("outletId",state.outletId).findFirst()

        state.cartItems = state.cart?.items
        state.realm.commitTransaction()

        if (state.cartItems != null) {
            for (item in state.cartItems!!)
                state.cartItemsList.add(item!!)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun calculateTotals(){
        state.grossTotal = 0.0
        state.subTotal = 0.0
        state.tax = 0.0
        state.netAmount = 0.0
        state.totalLessDiscount = 0.0
        state.cart = null

        getCartItems()

        if (state.cartItems?.count()!! > 0) {

            state.grossTotal = TransactionService.getGrossTotal(state.cartItems)
            state.productDiscounts = DiscountService.getTotalProductDiscount(state.cartItems)
            state.valueDiscount = DiscountService.getValueDiscount(state.grossTotal - state.productDiscounts)
            state.subTotal = state.grossTotal - state.valueDiscount - state.productDiscounts
            state.tax = TransactionService.getTaxTotal(state.cartItems)
            state.netAmount = state.tax + state.subTotal

            tvGrossTotal.text = NumberService.formatAmount(state.grossTotal)
            tvProductDiscounts.text = NumberService.formatAmount(state.productDiscounts)
            tvValueDiscounts.text = NumberService.formatAmount(state.valueDiscount)
            tvSubTotal.text = NumberService.formatAmount(state.subTotal)
            tvTaxTotal.text = NumberService.formatAmount(state.tax)
            tvTotal.text = NumberService.formatAmount(state.netAmount)
            tvBalance.text = NumberService.formatAmount(state.balance)
            tvPaymentMade.text = NumberService.formatAmount(state.paymentMade)
        }
        else {
            tvGrossTotal.text = "KES O.OO"
            tvTaxTotal.text = "KES O.OO"
            tvTotal.text = "KES O.OO"
            tvSubTotal.text = "KES O.OO"
            tvProductDiscounts.text = "KES O.OO"
            tvValueDiscounts.text = "KES O.OO"
        }
    }

    private class State {
        lateinit var realm: Realm
        lateinit var type: String
        var cart: Cart? = null
        lateinit var sale: Sale
        var cartItems: RealmList<CartItem?>? = null
        var cartItemsList = ArrayList<CartItem>()
        var grossTotal: Double = 0.0
        var totalLessDiscount: Double = 0.0
        var valueDiscount: Double = 0.0
        var tax: Double = 0.0
        var paymentMade: Double = 0.0
        var balance: Double = 0.0
        var outletId = ""
        var outletName = ""
        var subTotal: Double = 0.0
        var productDiscounts: Double = 0.0
        var netAmount: Double = 0.0
        var paymentType: String? = null
        var refNo: String? = null
    }
}
