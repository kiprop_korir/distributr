package com.mobile.distributr.modules.outlets.helper

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R

internal class OutletItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val name: TextView = itemView.findViewById(R.id.name)
    val code: TextView = itemView.findViewById(R.id.code)
    val select: TextView = itemView.findViewById(R.id.select)
}