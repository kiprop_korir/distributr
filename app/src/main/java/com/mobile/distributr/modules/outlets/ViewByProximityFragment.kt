package com.mobile.distributr.modules.outlets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mobile.distributr.R
import com.mobile.distributr.modules.outlets.model.Outlet
import io.realm.Realm

class ViewByProximityFragment: Fragment(){

    lateinit var realm: Realm
    var outlets: ArrayList<Outlet> = arrayListOf()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_view_by_proximity, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(context)
            Realm.getDefaultInstance()
        }

        realm.beginTransaction()
        val realmOutlets = realm.where(Outlet::class.java)
            .findAll()
        for (outlet in realmOutlets) {
            outlets.add(outlet)
        }

        realm.commitTransaction()
    }
}