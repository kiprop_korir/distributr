package com.mobile.distributr.modules.payment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.common.enum.PaymentOptions
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.common.service.NumberService
import com.mobile.distributr.model.PaymentOption
import com.mobile.distributr.modules.invoice.InvoiceActivity
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.app_bar.*
import java.math.RoundingMode
import java.text.DecimalFormat
import kotlin.collections.ArrayList

class PaymentActivity : AppCompatActivity() {

    private lateinit var state: State

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        state = State()

        state.type = intent.getStringExtra("type")!!
        state.total = intent.getDoubleExtra("amount_due", 0.0)
        state.outletName = PreferenceUtils.getString(PreferenceUtils.CURRENT_OUTLET_NAME, null).toString()
        state.outletId = PreferenceUtils.getInt(PreferenceUtils.CURRENT_OUTLET_ID,0).toString()

        tv_title.text = "COLLECT PAYMENT"

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        getCartItems()

        total.text = NumberService.formatAmount(state.total)

        edAmount.setText("%,d".format(roundOffDecimal(state.total)?.toInt()))

        radioGroup.setOnCheckedChangeListener { radioGroup, i ->

            when (i) {
                R.id.cash ->{
                    layoutMpesa.visibility = View.GONE
                    layoutCheque.visibility = View.GONE
                    layoutVoucher.visibility = View.GONE
                    state.paymentType = PaymentOptions.CASH.rawValue
                }
                R.id.cheque ->{
                    layoutMpesa.visibility = View.GONE
                    layoutCheque.visibility = View.VISIBLE
                    layoutVoucher.visibility = View.GONE
                    state.paymentType = PaymentOptions.CHEQUE.rawValue
                }
                R.id.mobile_money ->{
                    layoutMpesa.visibility = View.VISIBLE
                    layoutCheque.visibility = View.GONE
                    layoutVoucher.visibility = View.GONE
                    state.paymentType = PaymentOptions.MPESA.rawValue
                }
                R.id.radioVoucher ->{
                    layoutMpesa.visibility = View.GONE
                    layoutCheque.visibility = View.GONE
                    layoutVoucher.visibility = View.VISIBLE
                    state.paymentType = PaymentOptions.VOUCHER.rawValue
                }
            }
        }

//        edAmount.addTextChangedListener(object : TextWatcher {
//            override fun onTextChanged(
//                s: CharSequence,
//                start: Int,
//                before: Int,
//                count: Int
//            ) {
//
//            }
//
//            override fun beforeTextChanged(
//                s: CharSequence,
//                start: Int,
//                count: Int,
//                after: Int
//            ) {
//            }
//
//            override fun afterTextChanged(s: Editable) {
//                edAmount.removeTextChangedListener(this)
//
//                try {
//                    var originalString = s.toString()
//                    val longval: Long
//                    if (originalString.contains(",")) {
//                        originalString = originalString.replace(",".toRegex(), "")
//                    }
//                    longval = originalString.toLong()
//                    val formatter =
//                        NumberFormat.getInstance(Locale.US)
//                    formatter.apply {"#,###,###,###"}
//                    val formattedString = formatter.format(longval)
//
//                    //setting text after format to EditText
//                    edAmount.setText(formattedString)
//                    edAmount.setSelection(edAmount.getText().length)
//                } catch (nfe: NumberFormatException) {
//                    nfe.printStackTrace()
//                }
//
//                edAmount.addTextChangedListener(this)
//
//            }
//        })

        complete.setOnClickListener {

            val paymentMade = edAmount.text.let { it } ?: return@setOnClickListener

            state.paymentMade = paymentMade.toString().replace(",", "").replace(" ", "").toDouble()


            state.balance = state.total - state.paymentMade
            state.balance = NumberService.roundOff(state.balance)
            val intent = Intent(this, InvoiceActivity::class.java)
            intent.putExtra("type", state.type)
            intent.putExtra("beneficiary", state.outletName)
            intent.putExtra("amount_received", state.paymentMade)
            intent.putExtra("balance", state.balance)
            intent.putExtra("payment_type", state.paymentType)
            intent.putExtra("ref_no", state.refNo)

            startActivity(intent)
        }

        btn_back.setOnClickListener{
            finish()
        }
    }

    fun roundOffDecimal(number: Double): Double? {
        val df = DecimalFormat("#")
        df.roundingMode = RoundingMode.CEILING
        return df.format(number).toDouble()
    }

    private fun getCartItems(){
        state.cartItemsList.clear()

        state.realm.beginTransaction()
        if (state.type == TransactionType.POS.rawValue) state.outletId = "POS"
        state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type).equalTo("outletId",state.outletId).findFirst()
        state.cartItems = state.cart?.items
        state.realm.commitTransaction()

        if (state.cartItems != null) {
            for (item in state.cartItems!!)
                state.cartItemsList.add(item!!)
        }
        else {
            empty_cart.visibility = View.VISIBLE
            bottom.visibility = View.GONE
        }
    }

    private class State {
        lateinit var realm: Realm
        lateinit var type: String
        var total: Double = 0.0
        var balance: Double = 0.0
        var paymentMade: Double = 0.0
        var outletId: String? = null
        var outletName = ""
        var cartItems : RealmList<CartItem?>? = null
        var cartItemsList = ArrayList<CartItem>()
        var cart: Cart? = null
        var paymentType: String? = null
        var refNo: String? = null
    }
}
