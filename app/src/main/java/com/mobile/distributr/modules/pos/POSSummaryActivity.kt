package com.mobile.distributr.modules.pos

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.common.service.DiscountService
import com.mobile.distributr.common.service.NumberService
import com.mobile.distributr.common.service.TransactionService
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.modules.sale.adapter.SummaryItemAdapter
import com.mobile.distributr.modules.home.Home
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_sale_summary.rv_products
import kotlinx.android.synthetic.main.activity_sale_summary.tvGrossTotal
import kotlinx.android.synthetic.main.activity_sale_summary.tvProductDiscounts
import kotlinx.android.synthetic.main.activity_sale_summary.tvTaxTotal
import kotlinx.android.synthetic.main.activity_sale_summary.tvValueDiscounts
import kotlinx.android.synthetic.main.activity_sale_summary_pos.*
import kotlinx.android.synthetic.main.activity_sale_summary_pos.complete
import kotlinx.android.synthetic.main.activity_sale_summary_pos.date
import kotlinx.android.synthetic.main.activity_sale_summary_pos.outlet
import kotlinx.android.synthetic.main.activity_sale_summary_pos.tvTotal
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.dialog_add_to_cart.view.*
import java.util.*
import kotlin.collections.ArrayList

class POSSummaryActivity : AppCompatActivity(){

    private lateinit var state: State

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sale_summary_pos)

        state = State()

        loadViews()

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        getCartItems()
        calculateTotals()

    }

    @SuppressLint("SetTextI18n")
    private fun loadViews(){

        if(state.type == TransactionType.POS.rawValue)
            outlet.visibility = View.GONE
        else
            outlet.text = state.type + " to " + state.outletName

        tv_title.text = state.type.capitalize() + " Summary"

        date.text = Date().toString()

        val adapter =
            SummaryItemAdapter(
                this,
                state.cartItemsList,
                object :
                    SummaryItemAdapter.ClickListener {

                    @SuppressLint("SetTextI18n")
                    override fun changeQuantity(item: CartItem, quantity: Int) {
                        val mDialogView = LayoutInflater.from(this@POSSummaryActivity)
                            .inflate(R.layout.dialog_add_to_cart, null)
                        //AlertDialogBuilder
                        val mBuilder = AlertDialog.Builder(this@POSSummaryActivity)
                            .setView(mDialogView)
                            .setTitle(item.product!!.name)
                        //show dialog
                        val mAlertDialog = mBuilder.show()

                        val newQuantity = item.quantity ?: 1

                        mDialogView.edQuantity.setText(newQuantity.toString())

                        //place cursor end of edit text
                        mDialogView.edQuantity.setSelection(mDialogView.edQuantity.text.length)

                        mDialogView.btn_add.text = "EDIT"

                        mDialogView.btn_add.setOnClickListener {
                            //dismiss dialog
                            mAlertDialog.dismiss()
                            editCartItem(item, mDialogView.edQuantity.text.toString().toInt())
                            calculateTotals()
                        }
                    }
                    override fun deleteFromCart(item: CartItem, position: Int) {
                        //
                        state.realm.beginTransaction()
                        item.deleteFromRealm()
                        state.realm.commitTransaction()
                        //state.realm.close()
                        getCartItems()
                        rv_products.adapter?.notifyDataSetChanged()
                        calculateTotals()
                    }
                })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        btn_back.setOnClickListener{
            finish()
        }

        radioGroup.setOnCheckedChangeListener { radioGroup, i ->

            when (i) {
                R.id.cash ->{
                    layoutMpesa.visibility = View.GONE
                    layoutCheque.visibility = View.GONE
                    layoutVoucher.visibility = View.GONE
                }
                R.id.cheque ->{
                    layoutMpesa.visibility = View.GONE
                    layoutCheque.visibility = View.VISIBLE
                    layoutVoucher.visibility = View.GONE
                }
                R.id.mobile_money ->{
                    layoutMpesa.visibility = View.VISIBLE
                    layoutCheque.visibility = View.GONE
                    layoutVoucher.visibility = View.GONE
                }
                R.id.radioVoucher ->{
                    layoutMpesa.visibility = View.GONE
                    layoutCheque.visibility = View.GONE
                    layoutVoucher.visibility = View.VISIBLE
                }
            }
        }

//        edAmount.addTextChangedListener(object : TextWatcher {
//            override fun onTextChanged(
//                s: CharSequence,
//                start: Int,
//                before: Int,
//                count: Int
//            ) {
//                edAmount.setText( "%,d".format(s.toString().replace(",", "").toInt()))
//            }
//
//            override fun beforeTextChanged(
//                s: CharSequence,
//                start: Int,
//                count: Int,
//                after: Int
//            ) {
//            }
//
//            override fun afterTextChanged(s: Editable) {}
//        })


        complete.setOnClickListener{

            val builder = androidx.appcompat.app.AlertDialog.Builder(this)
            val inflater = this.layoutInflater
            val v: View = inflater.inflate(R.layout.dialog_pos_sale_complete, null) // this line
            builder.setView(v)
            val dialog = builder.create()
            val btnComplete = v.findViewById(R.id.btnComplete) as Button
            val tvDescription = v.findViewById(R.id.tvDescription) as TextView

            tvDescription.text = "You have successfully made a POS sale for a total value of ${NumberService.formatAmount(state.netAmount)}"

            btnComplete.setOnClickListener {
                dialog.dismiss()
                clearCartData()
                val intent = Intent(this, Home::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            }

            dialog.show()
        }
    }

    private fun clearCartData(){
        //clear cart

        state.realm.executeTransaction{
            state.cart?.deleteFromRealm()
            val cartItems = state.realm.where(CartItem::class.java).equalTo("type",state.type).
            equalTo("outletId",state.outletId).findAll()
            cartItems?.deleteAllFromRealm()
        }
    }

    fun editCartItem(item: CartItem?, quantity:Int){
        state.realm.beginTransaction()

        item?.quantity = quantity
        state.realm.copyToRealmOrUpdate(item)
        state.realm.commitTransaction()
        showSuccessSnackBar()

        updateCart()
        rv_products.adapter?.notifyDataSetChanged()
        calculateTotals()
    }

    private fun showSuccessSnackBar(){
        val snackBar = Snackbar.make(toolbar,"Product updated successfully", Snackbar.LENGTH_SHORT)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(resources.getColor(R.color.green_400, null))
        snackBar.show()
    }
    private fun updateCart(){

        state.realm.beginTransaction()
        state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type).findFirst()

        if (state.cart == null) {
            state.cart = Cart()
            state.cart!!.type = state.type
        }
        state.realm.commitTransaction()

        val count = state.cart?.items?.count() ?: 0

        if (count > 0) {
            cart_count.text = count.toString()
            cart_count.visibility = View.VISIBLE
        } else
            cart_count.visibility = View.GONE
    }

    private fun getCartItems(){

        state.cartItemsList.clear()

        state.realm.beginTransaction()
        if (state.type == TransactionType.POS.rawValue) state.outletId = "POS"

        state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type).equalTo("outletId",state.outletId).findFirst()

        state.cartItems = state.cart?.items
        state.realm.commitTransaction()

        if (state.cartItems != null) {
            for (item in state.cartItems!!)
                state.cartItemsList.add(item!!)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun calculateTotals(){
        state.grossTotal = 0.0
        state.subTotal = 0.0
        state.tax = 0.0
        state.netAmount = 0.0
        state.totalLessDiscount = 0.0
        state.cart = null

        getCartItems()

        if (state.cartItems?.count()!! > 0) {

            state.grossTotal = TransactionService.getGrossTotal(state.cartItems)
            state.productDiscounts = DiscountService.getTotalProductDiscount(state.cartItems)
            state.valueDiscount = DiscountService.getValueDiscount(state.grossTotal - state.productDiscounts)
            state.subTotal = state.grossTotal - state.valueDiscount - state.productDiscounts
            state.tax = TransactionService.getTaxTotal(state.cartItems)
            state.netAmount = state.tax + state.subTotal

            tvGrossTotal.text = NumberService.formatAmount(state.grossTotal)
            tvProductDiscounts.text = NumberService.formatAmount(state.productDiscounts)
            tvValueDiscounts.text = NumberService.formatAmount(state.valueDiscount)
            tvSubTotal.text = NumberService.formatAmount(state.subTotal)
            tvTaxTotal.text = NumberService.formatAmount(state.tax)
            tvTotal.text = NumberService.formatAmount(state.netAmount)

        }
        else {
            tvGrossTotal.text = "KES O.OO"
            tvTaxTotal.text = "KES O.OO"
            tvTotal.text = "KES O.OO"
            tvSubTotal.text = "KES O.OO"
            tvProductDiscounts.text = "KES O.OO"
            tvValueDiscounts.text = "KES O.OO"
        }

        tvTotalDue.text = NumberService.formatAmount(state.netAmount)
    }

    private  class State {
        lateinit var realm: Realm
        var outletId = TransactionType.POS.rawValue
        var outletName = "Walk In Customer"
        var type: String = TransactionType.POS.rawValue
        var cart: Cart? = null
        var cartItemsList = ArrayList<CartItem>()
        var cartItems : RealmList<CartItem?>? = null
        var grossTotal: Double = 0.0
        var subTotal: Double = 0.0
        var totalLessDiscount: Double = 0.0
        var productDiscounts: Double = 0.0
        var valueDiscount: Double = 0.0
        var tax: Double = 0.0
        var netAmount: Double = 0.0
    }
}
