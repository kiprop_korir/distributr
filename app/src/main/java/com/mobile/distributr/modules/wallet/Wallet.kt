package com.mobile.distributr.modules.wallet

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.adapter.TransactionsAdapter
import com.mobile.distributr.common.model.Transaction
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_wallet.*


class Wallet : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)
   Realm.init(this)

        back.setOnClickListener{
            finish()
        }

        tvViewMore.paintFlags = tvViewMore.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        tvViewMore.setOnClickListener{
            val intent = Intent(this, Statements::class.java)
            startActivity(intent)
        }

        send_money.setOnClickListener{
            val intent = Intent(this, SendMoney::class.java)
            startActivity(intent)
        }
        my_account.setOnClickListener{
            val intent = Intent(this, MyAccount::class.java)
            startActivity(intent)
        }

        val transactions = ArrayList<Transaction>()

        val trans1 = Transaction()
        trans1.amonut = 20000.00
        trans1.description = "Sent to M-PESA"
        trans1.date = "4th Feb 2020"
        trans1.type = "CREDIT"

        val trans2 = Transaction()
        trans2.amonut = 4000.00
        trans2.description = "Sent to M-PESA"
        trans2.date = "4th Feb 2020"
        trans2.type = "CREDIT"


        val trans3 = Transaction()
        trans3.amonut = 9000.00
        trans3.description = "Receive money from ALEX NJENGA"
        trans3.date = "4th Feb 2020"
        trans3.type = "DEBIT"


        val trans5 = Transaction()
        trans5.amonut = 7700.00
        trans5.description = "Sent to M-PESA"
        trans5.date = "4th Feb 2020"
        trans5.type = "CREDIT"

        val trans4 = Transaction()
        trans4.amonut = 8000.00
        trans4.description = "Receive payment from Braeside School"
        trans4.date = "4th Feb 2020"
        trans4.type = "DEBIT"


        val trans6 = Transaction()
        trans6.amonut = 300.00
        trans6.description = "Sent to M-PESA"
        trans6.date = "4th Feb 2020"
        trans6.type = "CREDIT"


       transactions.add(trans1)
        transactions.add(trans2)
        transactions.add(trans3)
        transactions.add(trans4)
        transactions.add(trans5)
        transactions.add(trans6)

        val obj_adapter = TransactionsAdapter(this,transactions)

        rvTransactions.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvTransactions.adapter = obj_adapter

        back.setOnClickListener{
            finish()
        }
    }
}
