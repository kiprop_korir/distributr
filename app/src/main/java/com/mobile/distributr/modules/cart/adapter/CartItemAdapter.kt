package com.mobile.distributr.modules.cart.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobile.distributr.R
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.common.service.NumberService
import com.mobile.distributr.common.service.TransactionService
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.utils.PreferenceUtils
import kotlinx.android.synthetic.main.item_cart.view.*
import kotlinx.android.synthetic.main.item_cart.view.amount
import kotlinx.android.synthetic.main.item_cart.view.code
import kotlinx.android.synthetic.main.item_cart.view.ivProductImage
import kotlinx.android.synthetic.main.item_cart.view.name
import kotlinx.android.synthetic.main.item_cart.view.remove
import kotlinx.android.synthetic.main.item_cart.view.tvPrice
import kotlinx.android.synthetic.main.item_summary.view.*
import java.math.RoundingMode
import java.text.DecimalFormat


class CartItemAdapter(
    private val context: Context,
    private val dataSource: ArrayList<CartItem>,
    private val type: String,
    private val clickListener: ClickListener

) :  RecyclerView.Adapter<CartItemAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return dataSource.count()

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position], type, clickListener)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_cart, parent, false)
        return ViewHolder(
            v
        )
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(cartItem: CartItem, type: String, clickListener: ClickListener) {

            itemView.name.text = cartItem.product?.name
            itemView.code.text = cartItem.product?.code

            val productPrice = TransactionService.getActualPrice(cartItem.product!!)

            val amount = productPrice!! * cartItem.quantity!!
            val roundedOffAmt = roundOffDecimal(amount)
            val formatter = DecimalFormat("#,###.00")
            val amt = formatter.format(amount)

            if(cartItem.product?.isFreemium == true) {
                itemView.amount.text =  "FREE"
                }
            else {
                itemView.amount.text =   "KES $amt"
            }

            itemView.ed_quantity.setText(cartItem.quantity.toString())

            //disable minus if qty is 1

                if(cartItem.quantity == 1) {
//                    itemView.minus.setBackgroundColor(
//                        ContextCompat.getColor(itemView.context, R.color.grey_300))
                    itemView.minus.isEnabled = false
                }

            itemView.add.setOnClickListener{
                addOrSubtract(true, clickListener, cartItem)
            }
            itemView.minus.setOnClickListener{
                addOrSubtract(false, clickListener, cartItem)
            }

            itemView.remove.setOnClickListener {
                clickListener.deleteFromCart(cartItem, adapterPosition)
            }

            val dataMode = PreferenceUtils.getBoolean(PreferenceUtils.DATA_SAVE_MODE, false)

            if(!dataMode) {
                Glide.with(itemView.context).load(cartItem.product?.imageUrl).placeholder(R.mipmap.image_placeholder).into(itemView.ivProductImage)
            }
            else {
                itemView.ivProductImage.visibility = View.GONE
            }

            itemView.tvPrice.text = "@ ${productPrice?.let { NumberService.formatAmount(it) }}"

            when (type){
                TransactionType.RETURN.rawValue -> {
                    itemView.amount.visibility = View.GONE
                }
                 TransactionType.LOSS.rawValue -> {
                    itemView.amount.text = "State: ${cartItem.state}"
                }
                else -> {

                }
            }

        }

        @SuppressLint("SetTextI18n")
        private fun addOrSubtract(add:Boolean, clickListener: ClickListener, cartItem: CartItem){
            var quantity =  itemView.ed_quantity.text.toString().toInt()

            //update value on realm

            if (add)
                quantity += 1
            else
                quantity -= 1

            val productPrice = TransactionService.getActualPrice(cartItem.product!!)

            itemView.ed_quantity.setText(quantity.toString())
            val amount = productPrice!! * quantity
            val roundedOffAmt = roundOffDecimal(amount)

            val formatter = DecimalFormat("#,###.00")
            val amt = formatter.format(roundedOffAmt)

            itemView.amount.text = "KES $amt"

            clickListener.changeQuantity(cartItem, quantity)

            itemView.minus.isEnabled = quantity != 1

        }

        fun roundOffDecimal(number: Double): Double? {
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            return df.format(number).toDouble()
        }


    }

    interface ClickListener {
        fun changeQuantity(item: CartItem, quantity: Int)
        fun deleteFromCart(item: CartItem, position: Int)
    }
}
