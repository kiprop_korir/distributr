package com.mobile.distributr.modules.reports.adapter;

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.modules.outlets.model.Outlet
import kotlinx.android.synthetic.main.item_outlet.view.name
import kotlinx.android.synthetic.main.item_ranking.view.*


class RankingAdapter(
    private val context: Context,
    private val dataSource: ArrayList<Outlet>


) : RecyclerView.Adapter<RankingAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return dataSource.count()

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tag = position
        holder.bindItems(dataSource[position])

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_ranking, parent, false)
        return ViewHolder(
            v
        )
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(outlet: Outlet) {
            itemView.name.text = outlet.name
            itemView.position.text  = itemView.tag.toString()

        }

    }
}
