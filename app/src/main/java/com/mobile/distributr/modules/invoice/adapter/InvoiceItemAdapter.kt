package com.mobile.distributr.modules.invoice.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.service.TransactionService
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.common.service.DiscountService
import com.mobile.distributr.common.service.NumberService
import kotlinx.android.synthetic.main.item_cart.view.amount
import kotlinx.android.synthetic.main.item_cart.view.discount
import kotlinx.android.synthetic.main.item_cart.view.name
import kotlinx.android.synthetic.main.item_cart.view.tvPrice
import java.math.RoundingMode
import java.text.DecimalFormat

class InvoiceItemAdapter(
    private val context: Context,
    private val dataSource: ArrayList<CartItem>

) :  RecyclerView.Adapter<InvoiceItemAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return dataSource.count()

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position])
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_invoice, parent, false)
        return ViewHolder(
            v
        )
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(cartItem: CartItem) {

            itemView.name.text = cartItem.product?.name
            val productPrice = TransactionService.getActualPrice(cartItem.product!!) ?: 0.0

            val amount = productPrice * cartItem.quantity!!
            val formatter = DecimalFormat("#,###.00")
            val amt = formatter.format(amount)

            if(cartItem.product?.isFreemium == true) {
                itemView.amount.text =  "FREE"
                }
            else {
                itemView.amount.text =   "KES $amt \n"
                val discount = DiscountService.getProductItemDiscount(cartItem)
                if (discount == 0.0) {
                    itemView.discount.text = ""
                } else {
                    itemView.discount.text = "(Minus discount of KES $discount)"
                    itemView.discount.visibility = View.VISIBLE
                }
            }

            itemView.tvPrice.text = "@ ${productPrice?.let { NumberService.formatAmount(it) }} x ${cartItem.quantity}"
        }

        fun roundOffDecimal(number: Double): Double? {
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            return df.format(number).toDouble()
        }
    }

}
