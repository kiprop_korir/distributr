package com.mobile.distributr.modules.close_day

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.model.Product
import com.mobile.distributr.modules.manifest.model.StockReturnItem
import io.realm.Realm


class ReturnActivity : AppCompatActivity() {

    var products: ArrayList<Product> = arrayListOf()
    lateinit var realm: Realm
    lateinit var type: String
    lateinit var outletId: String
    var stockTakeItemStock : StockReturnItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock_take)
    }
//        realm = try {
//            Realm.getDefaultInstance()
//        } catch (e: Exception) {
//            Realm.init(this)
//            Realm.getDefaultInstance()
//        }
//        outletId = PreferenceUtils.getString(CURRENT_OUTLET_ID,"").toString()
//
//        stockTakeItem = getExistingItem()
//
//        tv_title.text = "RETURN ITEMS"
//        complete.text = "COMPLETE ITEM RETURN"
//
//        val outletName = PreferenceUtils.getString(CURRENT_OUTLET_NAME,"")
//        outletId = PreferenceUtils.getString(CURRENT_OUTLET_ID,"").toString()
//
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//
//        realm.beginTransaction()
//        val realmProducts = realm.where(Product::class.java)
//            .findAll()
//        for (product in realmProducts) {
//            products.add(product)
//        }
//        realm.commitTransaction()
//
//        val adapter = ProductAdapter(this@ReturnActivity, products,null,object : ProductAdapter.ItemClickListener {
//
//            override fun itemClick(product: Product) {
//
//
//                val mDialogView = LayoutInflater.from(this@ReturnActivity).inflate(R.layout.dialog_add_to_cart, null)
//                //AlertDialogBuilder
//                val mBuilder = AlertDialog.Builder(this@ReturnActivity)
//                    .setView(mDialogView)
//                    .setTitle(product.name)
//                //show dialog
//                val  mAlertDialog = mBuilder.show()
//
//                val quantity =  1
//
//                mDialogView.quantity.setText(quantity.toString())
//               // mDialogView.quantity.requestFocus()
//                mDialogView.btn_add.text = "DONE"
//                //show keyboard
//                //showSoftKeyboard(mDialogView.ed_quantity)//login button click of custom layout
//                mDialogView.btn_add.setOnClickListener {
//                    //dismiss dialog
//                    mAlertDialog.dismiss()
//
//                    var item:ReturnProductItem = ReturnProductItem()
//                    item.product = product
//                    item.quantity = quantity
//
//                    realm.beginTransaction()
//                    stockTakeItem!!.productItems.add(item)
//                    realm.copyToRealmOrUpdate(stockTakeItem!!)
//                    realm.commitTransaction()
//
//                    addToCart(stockTakeItem!!)
//                    complete.visibility = View.VISIBLE
//                    complete.setOnClickListener {
//                        val intent = Intent(applicationContext, ReturnSummary::class.java)
//                        startActivity(intent)
//                    }
//                }
//            }
//        })
//
//        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
//        rv_products.adapter = adapter
//
//        btn_back.setOnClickListener{
//            finish()
//        }
//    }
//
//    private fun getExistingItem() : ReturnItem? {
//        var item:ReturnItem? = null
//
//        realm.executeTransaction() {
//
//            item = realm.where(ReturnItem::class.java).equalTo("outletId", outletId).findFirst()
//                    ?: ReturnItem()
//                item!!.outletId = outletId
//            }
//        return item
//    }
//
//    fun addToCart(item: ReturnItem){
//
//            realm.executeTransaction() {
//                realm.copyToRealmOrUpdate(item)
//            }
//
//            showSuccessSnackBar()
//    }
//
//
//    fun showSuccessSnackBar(){
//        val snackBarView = Snackbar.make(toolbar, "Return of items done successfully" , Snackbar.LENGTH_LONG)
//        val view = snackBarView.view
//        val params = view.layoutParams as FrameLayout.LayoutParams
//        params.gravity = Gravity.TOP
//        params.topMargin = 170
//        view.layoutParams = params
//        //view.background = drawable.resources.getColor(R.color.green_500)
//        snackBarView.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
//        snackBarView.setTextColor(resources.getColor(R.color.white))
//        snackBarView.setBackgroundTint(resources.getColor(R.color.green_400))
//        snackBarView.show()
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            android.R.id.home -> {
//                onBackPressed()
//                return true
//            }
//        }
//        return false
//    }
//    override fun onBackPressed() {
//       // super.onBackPressed()
//
//        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
//
//        builder.setTitle("Cancel Returns")
//        builder.setMessage("Are you sure you want to cancel this transaction?")
//
//        builder.setPositiveButton("Yes") { _, _ ->
//
//            finish()
//        }
//
//        builder.setNegativeButton("No", null)
//
//        val dialog = builder.create()
//        dialog.show()
//    }
}