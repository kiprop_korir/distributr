package com.mobile.distributr.modules.reports

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import kotlinx.android.synthetic.main.activity_reports.*
import kotlinx.android.synthetic.main.app_bar.*

class Reports : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reports)
        tv_title.text = "Reports"

        sales.setOnClickListener(){
            startActivity(Intent(this, SaleReport::class.java))
        }
        order_reports.setOnClickListener(){
            startActivity(Intent(this, OrderReport::class.java))
        }
        pending_outlets.setOnClickListener(){
            startActivity(Intent(this, PendingOutlets::class.java))
        }
        stock_reports.setOnClickListener(){
            startActivity(Intent(this, StockReports::class.java))
        }
        payments_summary.setOnClickListener(){
            startActivity(Intent(this, PendingPayments::class.java))
        }
        targets.setOnClickListener(){
            startActivity(Intent(this, Targets::class.java))
        }
        summary_reports.setOnClickListener(){
            startActivity(Intent(this, SummaryReport::class.java))
        }

        btn_back.setOnClickListener{
            finish()
        }
    }
}
