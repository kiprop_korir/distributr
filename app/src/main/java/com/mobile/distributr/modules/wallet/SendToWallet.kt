package com.mobile.distributr.modules.wallet

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.modules.security.PinLockActivity

import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.send_wallet.*


class SendToWallet : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.send_wallet)
        tv_title.text = "SEND TO WALLET"



        btn_next.setOnClickListener {

            var amount = ed_amount.text
            var accountNo = ed_account_no.text
            var transactionRef = ed_transaction_ref.text

            val builder = AlertDialog.Builder(this)
            builder.setTitle("Transfer KES $amount to $accountNo?")

            builder.setPositiveButton("Yes") { _, _ ->
                startActivity(Intent(this, PinLockActivity::class.java))
            }

            builder.setNegativeButton("No", null)

            val dialog = builder.create()
            dialog.show()

        }

        btn_back.setOnClickListener{
            finish()
        }

    }
}