package com.mobile.distributr.modules.deliveries.adapter;

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.modules.deliveries.DeliveryDetails
import com.mobile.distributr.modules.deliveries.model.Delivery
import kotlinx.android.synthetic.main.item_delivery.view.*


class DeliveryAdapter(
    private var dataSource: ArrayList<Delivery>

) : RecyclerView.Adapter<DeliveryAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position])
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_delivery, parent, false)
        return ViewHolder(
            v
        )
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(delivery: Delivery) {
            itemView.customer_name.text = delivery.customerName
            itemView.ref_no.text = delivery.refNo

            itemView.setOnClickListener{
                val intent = Intent(itemView.context, DeliveryDetails::class.java)
                intent.putExtra("deliveryID",delivery.id)
                itemView.context.startActivity(intent)
            }
        }
    }
}
