package com.mobile.distributr.modules.sale.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.service.TransactionService
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.common.service.DiscountService
import com.mobile.distributr.common.service.NumberService
import kotlinx.android.synthetic.main.item_summary.view.*
import kotlinx.android.synthetic.main.item_summary.view.amount
import kotlinx.android.synthetic.main.item_summary.view.code
import kotlinx.android.synthetic.main.item_summary.view.discount
import kotlinx.android.synthetic.main.item_summary.view.name
import kotlinx.android.synthetic.main.item_summary.view.remove

class SummaryItemAdapter(
    private val context: Context,
    private val dataSource: ArrayList<CartItem>,
    private val clickListener: ClickListener

) :  RecyclerView.Adapter<SummaryItemAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position], clickListener)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_summary, parent, false)
        return ViewHolder(
            v
        )
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(cartItem: CartItem, clickListener: ClickListener) {
            itemView.name.text = cartItem.product?.name
            itemView.code.text = cartItem.product?.code

            val productPrice = TransactionService.getActualPrice(cartItem.product!!)

            val amount = productPrice!! * cartItem.quantity!!

            itemView.amount.text = NumberService.formatAmount(amount)
            itemView.quantity.text = cartItem.quantity.toString()

            if(cartItem.product?.isFreemium == true) {
                itemView.amount.text =  "FREE"
            }
            else {
                itemView.amount.text = NumberService.formatAmount(amount)
                val discount = DiscountService.getProductItemDiscount(cartItem)
                if (discount == 0.0) {
                    itemView.discount.text = ""
                } else {
                    itemView.discount.text = "(Minus discount of KES $discount)"
                    itemView.discount.visibility = View.VISIBLE
                }
            }

            itemView.quantity.paintFlags = itemView.quantity.paintFlags or Paint.UNDERLINE_TEXT_FLAG

            itemView.quantity.setOnClickListener{
                clickListener.changeQuantity(cartItem,cartItem.quantity!!)
            }

            itemView.remove.setOnClickListener {
                clickListener.deleteFromCart(cartItem, adapterPosition)
            }

            itemView.tvPrice.text = "@ ${productPrice?.let { NumberService.formatAmount(it) }}"
        }
    }

    interface ClickListener {
        fun changeQuantity(item: CartItem, quantity: Int)
        fun deleteFromCart(item: CartItem, position: Int)
    }
}
