package com.mobile.distributr.modules.pos

enum class POSScreenType {
    MOBILE, TABLET
}