//package com.mobile.distributor.ui.reports.adapter
//
//import android.content.Context
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.core.content.ContextCompat
//import androidx.core.content.ContextCompat.startActivity
//import androidx.recyclerview.widget.RecyclerView
//import com.mobile.distributor.R
//import com.mobile.distributor.model.CartItem
//import com.mobile.distributor.model.Order
//import com.mobile.distributor.model.Sale
//import com.mobile.distributor.ui.reports.SaleDetails
//import io.realm.RealmRecyclerViewAdapter
//import kotlinx.android.synthetic.main.item_cart.view.*
//import org.jetbrains.anko.backgroundColor
//import java.math.RoundingMode
//import java.text.DecimalFormat
//
//
//class SaleItemAdapter(
//    private val context: Context,
//    private val dataSource: ArrayList<CartItem>,
//    private val clickListener: ClickListener
//
//) :  RecyclerView.Adapter<SaleItemAdapter.ViewHolder>() {
//
//
//    override fun getItemCount(): Int {
//        return dataSource.count()
//
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.bindItems(dataSource[position], clickListener)
//    }
//
//    override fun onCreateViewHolder(
//        parent: ViewGroup,
//        viewType: Int
//    ): ViewHolder {
//        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_cart, parent, false)
//        return ViewHolder(v)
//    }
//
//    //the class is holding the list view
//    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//
//        fun bindItems(itemObject: Any, isSale:Boolean) {
//            var item = Any()
//            if(isSale) {
//                item = (item as? Sale)!!
//            }
//
//            itemView.name.text = item.product?.productName
//
//            val amount = cartItem.product?.productPrice!! * cartItem.quantity!!
//            val roundedOffAmt = roundOffDecimal(amount)
//            itemView.amount.text = "KES ${roundedOffAmt}"
//            itemView.ed_quantity.setText(cartItem.quantity.toString())
//
//
//
//            itemView.setOnClickListener {
//                startActivity<SaleDetails>()
//            }
//
//
//        }
//
//
//
//        fun roundOffDecimal(number: Double): Double? {
//            val df = DecimalFormat("#.##")
//            df.roundingMode = RoundingMode.CEILING
//            return df.format(number).toDouble()
//        }
//
//
//    }
//
//}
