package com.mobile.distributr.modules.stocktake

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.common.adapter.ProductAdapter
import com.mobile.distributr.helper.ProductContextType
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.model.Product
import com.mobile.distributr.modules.stocktake.model.StockTakeItem
import com.mobile.distributr.modules.stocktake.model.StockTakeProductItem
import com.mobile.distributr.utils.PreferenceUtils
import com.mobile.distributr.utils.PreferenceUtils.CURRENT_OUTLET_ID
import com.mobile.distributr.utils.PreferenceUtils.CURRENT_OUTLET_NAME
import io.realm.Realm
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.content_view_products.*
import kotlinx.android.synthetic.main.dialog_stock_take.view.*

class StockTake : AppCompatActivity() {

    var products: ArrayList<Product> = arrayListOf()
    lateinit var realm: Realm
    private var cart: Cart? = null
    lateinit var type: String
    lateinit var outletId: String
    var stockTakeItem : StockTakeItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock_take)
   realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }
        outletId = PreferenceUtils.getInt(CURRENT_OUTLET_ID,0).toString()

        stockTakeItem = getExistingItem()

        tv_title.text = "STOCK TAKE"

        val outletName = PreferenceUtils.getString(CURRENT_OUTLET_NAME,"")
        outletId = PreferenceUtils.getInt(CURRENT_OUTLET_ID,0).toString()
        complete.text = "COMPLETE STOCK TAKE"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        realm.beginTransaction()
        val realmProducts = realm.where(Product::class.java)
            .findAll()
        for (product in realmProducts) {
            products.add(product)
        }
        realm.commitTransaction()

        val adapter = ProductAdapter(this@StockTake, products,ProductContextType.OTHER,object : ProductAdapter.ItemClickListener {

            override fun itemClick(product: Product) {

                val mDialogView = LayoutInflater.from(this@StockTake).inflate(R.layout.dialog_stock_take, null)
                //AlertDialogBuilder
                val mBuilder = AlertDialog.Builder(this@StockTake)
                    .setView(mDialogView)
                    .setTitle(product.name)
                //show dialog
                val  mAlertDialog = mBuilder.show()

                val quantity =  1

                mDialogView.edQuantity.setText(quantity.toString())
                //place cursor end of edit text
                mDialogView.edQuantity.setSelection(mDialogView.edQuantity.text.length)
               // mDialogView.quantity.requestFocus()
                mDialogView.btn_add.text = "DONE"
                //show keyboard
                //showSoftKeyboard(mDialogView.ed_quantity)//login button click of custom layout
                mDialogView.btn_add.setOnClickListener {
                    //dismiss dialog
                    mAlertDialog.dismiss()

                    var item = StockTakeProductItem()
                    item.product = product
                    item.quantity =  mDialogView.edQuantity.text.toString().toInt()
                    item.productCode = product.code

                    realm.beginTransaction()

                    stockTakeItem!!.productItems.add(item)
                    realm.copyToRealmOrUpdate(stockTakeItem!!)
                    realm.commitTransaction()

                    addToCart(stockTakeItem!!)
                    complete.visibility = View.VISIBLE
                    complete.setOnClickListener {
                        startActivity(Intent(applicationContext, StockTakeSummary::class.java))
                    }

                }
            }
        })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        btn_back.setOnClickListener{
            finish()
        }
    }

    private fun getExistingItem() : StockTakeItem? {
        var item: StockTakeItem? = null

        realm.executeTransaction() {

            item = realm.where(StockTakeItem::class.java).equalTo("outletId", outletId).findFirst()
                    ?: StockTakeItem()
                item!!.outletId = outletId
            }
        return item
    }

    fun addToCart(item: StockTakeItem){

            realm.executeTransaction() {
                realm.copyToRealmOrUpdate(item)
            }

            showSuccessSnackBar("Product added successfully")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }
    override fun onBackPressed() {
        // super.onBackPressed()
        if (stockTakeItem?.productItems?.count()!! > 0) {
            val builder = androidx.appcompat.app.AlertDialog.Builder(this)

            builder.setTitle("Cancel stock take")
            builder.setMessage("Are you sure you want to cancel this stock take transaction?")

            builder.setPositiveButton("Yes") { _, _ ->
                if (stockTakeItem?.isManaged == true) {
                    realm.beginTransaction()
                    stockTakeItem?.deleteFromRealm()
                    realm.commitTransaction()
                }
                finish()
            }

            builder.setNegativeButton("No", null)

            val dialog = builder.create()
            dialog.show()

        } else {
            finish()
        }
    }

    private fun showSuccessSnackBar(description: String){
        Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
    }
}