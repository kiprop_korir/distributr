package com.mobile.distributr.modules.outlets.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey
import java.io.Serializable

open class Outlet : RealmObject(), Serializable {

    @PrimaryKey
    @SerializedName("id")
    var id:  Int? = null

    @SerializedName("town")
    var town:  String? = null

    @SerializedName("code")
    var code:  String? = null

    @SerializedName("isActive")
    var isActive:  Boolean? = null

    @SerializedName("timestamp")
    var timestamp:  String? = null

    @SerializedName("type")
    var type:  String? = null

    @SerializedName("typeId")
    var typeId:  Int? = null

    @SerializedName("name")
    var name:  String? = null

    var routeId:  Int? = null

    @SerializedName("latitude")
    var latitude:  Double? = null

    @SerializedName("longitude")
    var longitude:  Double? = null

    var ownerPhone:  String? = null

    var owner:  String? = null

}
