package com.mobile.distributr.modules.manifest

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.common.enum.ManifestType
import com.mobile.distributr.model.*
import com.mobile.distributr.modules.manifest.model.ManifestItem
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_manifest.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlin.collections.ArrayList

class ManifestActivity : AppCompatActivity() {

    var products: ArrayList<Product> = arrayListOf()
    lateinit var realm: Realm
    lateinit var type: String
    private lateinit var state: State

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manifest)

        state = State()

        tv_title.text = "Manifest"

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        state.realm.beginTransaction()
        val realmProducts = state.realm.where(Product::class.java)
                    .distinct("code").equalTo("isIssued", true).greaterThan("productQty", 0).sort("name").findAll()
        state.realm.commitTransaction()


        btn_back.setOnClickListener{
            onBackPressed()
        }

        btn_sale_manifest.setOnClickListener{
            goToManifest(ManifestType.SALES)
        }

        btn_delivery_manifest.setOnClickListener{
            goToManifest(ManifestType.DELIVERY)
        }

    }

    private fun goToManifest(type: ManifestType){
        val intent = Intent(this, ManifestListingActivity::class.java)
        intent.putExtra("type", type)
        startActivity(intent)
    }


    private class State {
        var items: ArrayList<ManifestItem> = arrayListOf()
        lateinit var realm: Realm
        var distinctItems: Int = 0
    }
}