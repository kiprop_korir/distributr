package com.mobile.distributr.modules.manifest.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.Product
import io.realm.RealmList
import io.realm.annotations.PrimaryKey
import kotlin.properties.Delegates

open class ManifestItem {
    var product: Product? = null
    var quantity: Int = 0
}
