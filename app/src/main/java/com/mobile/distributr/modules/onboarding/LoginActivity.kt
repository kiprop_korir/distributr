package com.mobile.distributr.modules.onboarding

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.multidex.BuildConfig
import com.auth0.android.jwt.JWT
import com.microsoft.identity.client.*
import com.microsoft.identity.client.IPublicClientApplication.IMultipleAccountApplicationCreatedListener
import com.microsoft.identity.client.exception.MsalClientException
import com.microsoft.identity.client.exception.MsalException
import com.microsoft.identity.client.exception.MsalServiceException
import com.mobile.distributr.R
import com.mobile.distributr.app.Distributr
import com.mobile.distributr.common.enum.AppFlavours
import com.mobile.distributr.common.model.Category
import com.mobile.distributr.config.UserConfig
import com.mobile.distributr.model.Product
import com.mobile.distributr.modules.home.Home
import com.mobile.distributr.modules.notifications.NotificationStatus
import com.mobile.distributr.modules.notifications.model.DistributrNotification
import com.mobile.distributr.modules.outlets.model.Outlet
import com.mobile.distributr.modules.outlets.model.Route
import com.mobile.distributr.modules.pos.POSActivity
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.routes.*
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class LoginActivity : AppCompatActivity() {

    private var b2cApp: IMultipleAccountPublicClientApplication? = null
    lateinit var realm: Realm

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        tv_title.text = "Welcome to the new ${resources.getString(R.string.app_name)} app"
        tv_description.text = resources.getString(R.string.app_description)

        if(Distributr.instance.isMockEnvironment){
            tv_mock_data.visibility = View.VISIBLE
         }

        try {
            val pInfo: PackageInfo =
                this.packageManager.getPackageInfo(packageName, 0)
            val v = pInfo.versionName
            version.text = "Version $v"
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        // Creates a PublicClientApplication object with res/raw/auth_config_single_account.json
        PublicClientApplication.createMultipleAccountPublicClientApplication(this@LoginActivity,
            R.raw.auth_config,
            object : IMultipleAccountApplicationCreatedListener {
                override fun onCreated(application: IMultipleAccountPublicClientApplication) {
                    b2cApp = application
                }

                override fun onError(exception: MsalException) {
                    btnLogin.isEnabled = false
                    println("error")
                    println(exception)
                }
            })

        btnLogin.setOnClickListener {

            if(Distributr.instance.isMockEnvironment){
                mockLogin()
            }
            else {
                login()
            }
        }
    }

    private fun login(){
        if (b2cApp == null) {
            return
        }
        val parameters: AcquireTokenParameters = AcquireTokenParameters.Builder()
            .startAuthorizationFromActivity(this)
            .fromAuthority(UserConfig.AUTHORITY_URL)
            .withScopes(UserConfig.SCOPES)
            .withPrompt(Prompt.LOGIN)
            .withCallback(getAuthInteractiveCallback())
            .build()

        b2cApp?.acquireToken(parameters)
    }

        private fun mockLogin(){

            PreferenceUtils.saveAuthToken("mockToken")

            val name = "Mock name"
            val roles: Array<String> = arrayOf("Mock role")
            val id = "Mock id"

            PreferenceUtils.saveUser(name, id, roles)

            addInitDummyData()

            val intent: Intent = if(BuildConfig.FLAVOR == AppFlavours.RETAILR.rawValue) {
                Intent(applicationContext, POSActivity::class.java)
            } else {
                Intent(applicationContext, Home::class.java)
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()

        }
    /**
     * Callback used for interactive request.
     * If succeeds we use the access token to call the Microsoft Graph.
     * Does not check cache.
     */
    private fun getAuthInteractiveCallback(): AuthenticationCallback {
        return object : AuthenticationCallback {
            override fun onSuccess(authenticationResult: IAuthenticationResult) {
                /* Successfully got a token, use it to call a protected resource - MSGraph */

                PreferenceUtils.saveAuthToken(authenticationResult.accessToken)
                val jwt = JWT(authenticationResult.accessToken)
                val name = jwt.getClaim("name").asString()
                val roles = jwt.getClaim("roles").asArray(String::class.java)
                val id = jwt.getClaim("id").asString()

                PreferenceUtils.saveUser(name.toString(), id.toString(), roles)

                val intent: Intent = if(BuildConfig.FLAVOR == AppFlavours.RETAILR.rawValue) {
                    Intent(applicationContext, POSActivity::class.java)
                } else {
                    Intent(applicationContext, Home::class.java)
                }

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
            }

            @SuppressLint("TimberArgCount")
            override fun onError(exception: MsalException) {
                Timber.i("Auth", "onError: $exception" )
                println(exception.toString())
                showFailedDialog()
                if (exception is MsalClientException) {
                    /* Exception inside MSAL, more info inside MsalError.java */
                } else if (exception is MsalServiceException) {
                    /* Exception when communicating with the STS, likely config issue */
                }
            }

            override fun onCancel() {
                /* User canceled the authentication */
                Timber.d("User cancelled login.")
            }
        }
    }

    private fun showFailedDialog(){
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this)
        alertDialog.setTitle("Failed")
        alertDialog.setMessage("There was an error connecting to the server")
        alertDialog.setPositiveButton(
            "Ok"
        , null)

        val alert: AlertDialog = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
    }


    fun addInitDummyData(){

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }


        val products = ArrayList<Product>()

        val product1 = Product()
        product1.name = "Value Pack Beef Sausages 1kg"
        product1.code = "J31015401"
        product1.productQty = 34
        product1.price = 482.76
        product1.category = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        product1.isIssued = true
        product1.id = 1
        product1.vatRate = 16.0

        val product2 = Product()
        product2.name = "Beef Sausages (Economy) 1kg"
        product2.code = "J31015501"
        product2.productQty = 94
        product2.price = 461.21
        product2.category = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        product2.id = 2
        product2.vatRate = 16.0

        val product3 = Product()
        product3.name = "Beef Smokies Labless 1Kg"
        product3.code = "J31031702"
        product3.productQty = 54
        product3.price = 284.48
        product3.category = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        product3.isIssued = true
        product3.id = 3
        product3.vatRate = 16.0

        val product4 = Product()
        product4.name = "Chicken Sausages Value Pack kg"
        product4.code = "J31019106"
        product4.productQty = 39
        product4.price = 431.03
        product4.category = "FDD6CE9C-56AF-49EC-B51A-F672AFD93BC4"
        product4.id = 4
        product4.vatRate = 16.0

        val product5 = Product()
        product5.name = "Beef Smokies, 400gms Ex long"
        product5.code = "J31031706"
        product5.productQty = 134
        product5.price = 116.38
        product5.category = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        product5.id = 5
        product5.vatRate = 16.0


        val product6 = Product()
        product6.name = "Collar Bacon 400gms"
        product6.code = "J31020601"
        product6.productQty = 34
        product6.price = 344.83
        product6.category = "24A4D56E-7D18-4A55-945C-E55683A9CEFC"
        product6.isIssued = true
        product6.id = 6
        product6.vatRate = 16.0

        val product7 = Product()
        product7.name = "Fresh Beef Burgers, 1kg"
        product7.code = "J31100146"
        product7.productQty = 319
        product7.price = 615.00
        product7.category = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        product7.id = 7
        product7.vatRate = 16.0

        val product8 = Product()
        product8.name = "Pork Chipolatas 200gms"
        product8.code = "J31010101"
        product8.productQty = 76
        product8.price = 86.21
        product8.category = "24A4D56E-7D18-4A55-945C-E55683A9CEFC"
        product8.isIssued = true
        product8.id = 8
        product8.vatRate = 16.0

        products.add(product1)
        products.add(product2)
        products.add(product3)
        products.add(product4)
        products.add(product5)
        products.add(product6)
        products.add(product7)
        products.add(product8)


        val categories = ArrayList<Category>()

        val category1 = Category()
        category1.code = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        category1.name = "Cheese"

        val category2 = Category()
        category2.code = "FDD6CE9C-56AF-49EC-B51A-F672AFD93BC4"
        category2.name = "Cheese"

        val category3 = Category()
        category3.code = "24A4D56E-7D18-4A55-945C-E55683A9CEFC"
        category3.name = "Cheese"


        val category4 = Category()
        category4.code = "AC83109A-D129-46AB-97A3-AB4ED8640F29"
        category4.name = "Cheese"

        val category5 = Category()
        category5.code = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        category5.name = "Cheese"

        val category6 = Category()
        category6.code = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        category6.name = "Cheese"

        val category7 = Category()
        category7.code = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        category7.name = "Cheese"

        val category8 = Category()
        category8.code = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        category8.name = "Cheese"

        val category9 = Category()
        category9.code = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        category9.name = "Cheese"

        val category10 = Category()
        category10.code = "D8B96D13-5874-4276-B4F2-9A07F96BEA3A"
        category10.name = "Cheese"


        categories.add(category1)
        categories.add(category2)
        categories.add(category3)
        categories.add(category4)
        categories.add(category5)
        categories.add(category6)
        categories.add(category7)
        categories.add(category8)
        categories.add(category9)
        categories.add(category10)


        val routes = ArrayList<Route>()

        val route1 = Route()
        route1.name = "Westlands Route"
        route1.code = "1234"
        route1.id = 1

        val route2 = Route()
        route2.name = "Kangemi Route"
        route2.code = "1234"
        route2.id = 2

        val route3 = Route()
        route3.name = "Kilimani Route"
        route3.code = "1234"
        route3.id = 3

        val route4 = Route()
        route4.name = "Dagoretti Route"
        route4.code = "1234"
        route4.id = 4

        routes.add(route1)
        routes.add(route2)
        routes.add(route3)
        routes.add(route4)


        val outlets = ArrayList<Outlet>()

        val outlet1 = Outlet()
        outlet1.name = "Bohra Primary School"
        outlet1.latitude = -1.2584047
        outlet1.longitude = 36.7887363
        outlet1.id = 1
        outlet1.owner = "Patrick Njoroge"
        outlet1.ownerPhone = "0729394730"
        outlet1.routeId = 1
        outlet1.typeId = 1
        outlet1.code = "#OUT891"


        val outlet2 = Outlet()
        outlet2.name = "Virtual City"
        outlet2.latitude = -1.258572
        outlet2.longitude = 36.7882686
        outlet2.id = 2
        outlet2.owner = "Patrick Njoroge"
        outlet2.ownerPhone = "0729394730"
        outlet2.routeId = 1
        outlet1.typeId = 1
        outlet2.code = "#OUT892"


        val outlet3 = Outlet()
        outlet3.name = "Safaricom House"
        outlet3.latitude = -1.2591667
        outlet3.longitude = 36.7836393
        outlet3.id = 3
        outlet3.owner = "Patrick Njoroge"
        outlet3.ownerPhone = "0729394730"
        outlet3.routeId = 2
        outlet1.typeId = 2
        outlet3.code = "#OUT847"

        val outlet4 = Outlet()
        outlet4.name = "Yaya Centre"
        outlet4.latitude = -1.2928832
        outlet4.longitude = 36.7858743
        outlet4.id = 4
        outlet4.owner = "Patrick Njoroge"
        outlet4.ownerPhone = "0729394730"
        outlet4.routeId = 2
        outlet1.typeId = 2
        outlet4.code = "#OUT854"

        val outlet5 = Outlet()
        outlet5.name = "Galana Plaza"
        outlet5.latitude = -1.2909174
        outlet5.longitude = 36.7806823
        outlet5.id = 5
        outlet5.owner = "Patrick Njoroge"
        outlet5.ownerPhone = "0729394730"
        outlet5.routeId = 2
        outlet1.typeId = 2
        outlet5.code = "#OUT895"

        val outlet6 = Outlet()
        outlet6.name = "Sifa Towers"
        outlet6.latitude = -1.289793
        outlet6.longitude = 36.784343
        outlet6.id = 6
        outlet6.owner = "Patrick Njoroge"
        outlet6.ownerPhone = "0729394730"
        outlet6.routeId = 3
        outlet1.typeId = 3
        outlet5.code = "#OUT896"

        outlets.add(outlet1)
        outlets.add(outlet2)
        outlets.add(outlet3)
        outlets.add(outlet4)
        outlets.add(outlet5)
        outlets.add(outlet6)


        val notifications = ArrayList<DistributrNotification>()

        val notification1 = DistributrNotification()
        notification1.title = "Remember to Sync"
        notification1.message = "You have not synced in 2 days! Might there be a problem? Call contact center in case you need help"
        notification1.date = "6/5/2020 2:17 PM"
        notification1.status = NotificationStatus.UNREAD.toString()

        val notification2 = DistributrNotification()
        notification2.title = "New Products Available!"
        notification2.message = "Kindly go pick new products at your warehouse before tomorrow evening"
        notification2.date = "7/5/2020 3:54 PM"
        notification2.status = NotificationStatus.UNREAD.toString()

        notifications.add(notification1)
        notifications.add(notification2)

        realm.beginTransaction()
        realm.copyToRealmOrUpdate(outlets)
        realm.copyToRealmOrUpdate(routes)
        realm.copyToRealmOrUpdate(products)
        realm.copyToRealmOrUpdate(categories)
        realm.copyToRealmOrUpdate(notifications)
        realm.commitTransaction()
    }

}

