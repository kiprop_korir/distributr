package com.mobile.distributr.modules.reports.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.common.service.NumberService
import com.mobile.distributr.modules.sale.model.Sale
import com.mobile.distributr.modules.reports.model.StockReportProductItem
import kotlinx.android.synthetic.main.item_sale.view.*
import kotlinx.android.synthetic.main.item_stock_report.view.amount
import java.text.DecimalFormat


class SaleAdapter(
    private val context: Context,
    private val dataSource: ArrayList<Sale>

) :  RecyclerView.Adapter<SaleAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position])
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_sale, parent, false)
        return ViewHolder(v)
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(item: Sale) {
            itemView.sale_description.text = item.outletName
            itemView.date.text = item.date.toString()
            itemView.status.text = "COMPLETE"

            println("dwfwwrweq")
            println(item.amount)
            val amt = item.amount?.let { NumberService.formatAmount(it) }
            itemView.amount.text = "KES $amt"


//            when (item.activityDescription) {
//                "sale" -> itemView.activity_description.text = "sold"
//                "order" -> itemView.activity_description.text = "ordered"
//                else -> itemView.activity_description.text = item.activityDescription
//            }

                        itemView.status.background = itemView.context.resources.getDrawable(R.drawable.status_paid_bg)


        }
    }

    interface ItemClickListener {
        fun changeQuantity(item: StockReportProductItem)
    }
}
