package com.mobile.distributr.modules.security

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.andrognito.pinlockview.PinLockListener
import com.mobile.distributr.R
import kotlinx.android.synthetic.main.activity_pin_lock.*
import kotlinx.android.synthetic.main.app_bar.*

class PinLockActivity:AppCompatActivity (){



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin_lock)

        pin_lock_view.setPinLockListener(mPinLockListener)
        pin_lock_view.attachIndicatorDots(indicator_dots)

        btn_back.setOnClickListener{
            finish()
        }

    }

    private val mPinLockListener = object : PinLockListener {
        override fun onComplete(pin_code: String) {

            println("PIN entered is : $pin_code")



            authenticatePIN(pin_code)
        }

        override fun onEmpty() {
            Log.d("pin", "Pin empty")
        }

        override fun onPinChange(pinLength: Int, intermediatePin: String) {
            Log.d("pin", "Pin changed, new length $pinLength with intermediate pin $intermediatePin")
        }
    }

    fun authenticatePIN(pinCode:String){


    }
}