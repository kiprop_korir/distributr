package com.mobile.distributr.modules.reports


import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.mobile.distributr.R
import com.mobile.distributr.modules.outlets.model.Outlet
import com.mobile.distributr.model.Product
import com.mobile.distributr.modules.reports.adapter.RankingAdapter
import io.realm.Realm
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.content_view_products.*
import java.util.*

open class SummaryReport :AppCompatActivity(), OnSeekBarChangeListener,
    OnChartValueSelectedListener {

    private var chart: PieChart? = null
    var outlets: ArrayList<Outlet> = arrayListOf()
    var products: ArrayList<Product> = arrayListOf()
    var names: ArrayList<String> = arrayListOf()
    lateinit var realm: Realm

    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary_report)
   supportActionBar?.setDisplayHomeAsUpEnabled(true)
        tv_title.text = "TOP GROSSING"

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        realm.beginTransaction()
        val realmOutlets = realm.where(Outlet::class.java)
            .findAll()
        for (product in realmOutlets) {
            outlets.add(product!!)
        }

        val realmProducts = realm.where(Product::class.java)
            .findAll()
        for (product in realmProducts) {
            products.add(product!!)
            names.add(product.name!!)
        }
        realm.commitTransaction()


        chart = findViewById(R.id.chart1)
        chart!!.setUsePercentValues(true)
        chart!!.description.isEnabled = false
        chart!!.setExtraOffsets(5f, 0f, 5f, 5f)

        chart!!.dragDecelerationFrictionCoef = 0.95f


        chart!!.centerText = generateCenterSpannableText();

        chart!!.isDrawHoleEnabled = true
        chart!!.setHoleColor(Color.WHITE)

        chart!!.setTransparentCircleColor(Color.WHITE)
        chart!!.setTransparentCircleAlpha(110)

        chart!!.holeRadius = 58f
        chart!!.transparentCircleRadius = 61f

        chart!!.setDrawCenterText(true)

        chart!!.rotationAngle = 0f
        // enable rotation of the chart by touch
        // enable rotation of the chart by touch
        chart!!.isRotationEnabled = true
        chart!!.isHighlightPerTapEnabled = true

        // chart!!.setUnit(" €");
        // chart!!.setDrawUnitsInChart(true);

        // add a selection listener
        // chart!!.setUnit(" €");
// chart!!.setDrawUnitsInChart(true);
// add a selection listener
        chart!!.setOnChartValueSelectedListener(this)

        chart!!.animateY(1400, Easing.EaseInOutQuad)
        // chart!!.spin(2000, 0, 360);

        // chart!!.spin(2000, 0, 360);
        val l = chart!!.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)
        l.xEntrySpace = 7f
        l.yEntrySpace = 0f
        l.yOffset = 0f

        // entry label styling
        // entry label styling
        chart!!.setEntryLabelColor(Color.BLACK)
        chart!!.setEntryLabelTextSize(12f)

        chart!!.animateY(1400)

        setData(6, 10F)


        val adapter = RankingAdapter(this,outlets)




        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        rv_products.isNestedScrollingEnabled = false

        btn_back.setOnClickListener{
            finish()
        }
    }


    private fun setData(count: Int, range: Float) {
        val entries = ArrayList<PieEntry>()
        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
// the chart.
        for (i in 0 until count) {
            entries.add(
                PieEntry(
                    (Math.random() * range + range / 5).toFloat(),
                    names[i % names.size],
                    resources.getDrawable(R.drawable.ic_check)
                )
            )
        }
        val dataSet = PieDataSet(entries, "")
        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0F, 40F)
        dataSet.selectionShift = 5f
        // add a lot of colors
        val colors = ArrayList<Int>()
        for (c in ColorTemplate.VORDIPLOM_COLORS) colors.add(c)
        for (c in ColorTemplate.JOYFUL_COLORS) colors.add(c)
        for (c in ColorTemplate.COLORFUL_COLORS) colors.add(c)
        for (c in ColorTemplate.LIBERTY_COLORS) colors.add(c)
        for (c in ColorTemplate.PASTEL_COLORS) colors.add(c)
        colors.add(ColorTemplate.getHoloBlue())
        dataSet.colors = colors
        //dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(chart))
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.BLACK)
        // data.setValueTypeface(tfLight);
        chart!!.data = data
        // undo all highlights
        chart!!.highlightValues(null)
        chart!!.invalidate()
    }

    private fun generateCenterSpannableText(): SpannableString? {
        val s = SpannableString("Sale Report 2020\ngrouped by Product Names")
        s.setSpan(RelativeSizeSpan(1.7f), 0, 17, 0)
        s.setSpan(StyleSpan(Typeface.NORMAL), 18, s.length - 18, 0)
        s.setSpan(ForegroundColorSpan(Color.GRAY), 17, s.length - 14, 0)
        s.setSpan(RelativeSizeSpan(.8f), 16, s.length - 14, 0)
        s.setSpan(StyleSpan(Typeface.ITALIC), s.length - 16, s.length, 0)
        s.setSpan(ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length - 14, s.length, 0)
        return s
    }

    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
        setData(4, 10F)
    }


    override fun onStartTrackingTouch(p0: SeekBar?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStopTrackingTouch(p0: SeekBar?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onNothingSelected() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
