package com.mobile.distributr.modules.manifest.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.Product
import com.mobile.distributr.modules.close_day.ReturnProductItem
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class StockReturnItem : RealmObject() {
    @PrimaryKey
    @SerializedName("productId")
    var productId: Int? = null
    @SerializedName("product")
    var product: Product? = null
    @SerializedName("state")
    var state: String? = null
    @SerializedName("quantity")
    var quantity: Int? = null
}
