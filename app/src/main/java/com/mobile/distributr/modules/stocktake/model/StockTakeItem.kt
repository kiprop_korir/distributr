package com.mobile.distributr.modules.stocktake.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class StockTakeItem : RealmObject() {

    @SerializedName("product")
    var productItems: RealmList<StockTakeProductItem> =   RealmList()
    @PrimaryKey
    @SerializedName("date")
    var date: String? = null
    @SerializedName("outletId")
    var outletId: String? = null
}
