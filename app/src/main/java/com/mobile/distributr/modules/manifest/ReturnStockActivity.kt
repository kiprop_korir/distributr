package com.mobile.distributr.modules.manifest

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.helper.ProductContextType
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.model.Product
import com.mobile.distributr.modules.cart.CartActivity
import com.mobile.distributr.common.adapter.ProductAdapter
import com.mobile.distributr.common.enum.ManifestType
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.model.ReturnStockItemState
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_manifest.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.content_view_products.*
import kotlinx.android.synthetic.main.dialog_add_to_cart.view.*
import kotlinx.android.synthetic.main.dialog_add_to_cart.view.available_quantity
import kotlinx.android.synthetic.main.dialog_add_to_cart.view.btn_add
import kotlinx.android.synthetic.main.dialog_add_to_cart.view.edQuantity
import kotlinx.android.synthetic.main.dialog_add_to_cart.view.tvErrorText
import kotlinx.android.synthetic.main.dialog_return_inventory.*
import kotlinx.android.synthetic.main.dialog_return_inventory.view.*
import java.util.*
import kotlin.collections.ArrayList

class ReturnStockActivity : AppCompatActivity() {

    private lateinit var state: State

    @ExperimentalStdlibApi
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_make_sale)

        state = State()

        state.type = intent.getSerializableExtra("type") as TransactionType

        tv_title.text = "RECORD ${state.type}".capitalize(Locale.ROOT)

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        loadViews()
    }

    private fun loadViews(){

        cart_view.visibility = View.VISIBLE

        cart_view.setOnClickListener {
            val intent = Intent(this, CartActivity::class.java)
            intent.putExtra("type", TransactionType.LOSS.rawValue)
            startActivity(intent)
        }

        state.realm.beginTransaction()

        val realmProducts =
                    state.realm.where(Product::class.java).equalTo("isIssued", true).greaterThan("productQty", 0)
                        .findAll()

            if (realmProducts != null) {
                for (product in realmProducts) {
                    state.products.add(product)
                }
            }

        state.realm.commitTransaction()

        val adapter = ProductAdapter(this, state.products, ProductContextType.STOCK,object : ProductAdapter.ItemClickListener {

            @SuppressLint("SetTextI18n")
            override fun itemClick(product: Product) {

                val cartItem = getExistingCartItem(product.id!!)

                val mDialogView = LayoutInflater.from(this@ReturnStockActivity).inflate(R.layout.dialog_return_inventory, null)
                //AlertDialogBuilder
                val mBuilder = AlertDialog.Builder(this@ReturnStockActivity)
                    .setView(mDialogView)
                    .setTitle(product.name)
                //show dialog
                val  mAlertDialog = mBuilder.show()


                val quantity = product.productQty ?: 0

                mDialogView.available_quantity.text = "Total Qty in Van: $quantity"
                mDialogView.available_quantity.visibility = View.VISIBLE

                mDialogView.edQuantity.setText(quantity.toString())

                //place cursor end of edit text
                mDialogView.edQuantity.setSelection(mDialogView.edQuantity.text.length)

                mDialogView.btn_add.setOnClickListener {
                    //dismiss dialog
                    mAlertDialog.dismiss()
                    addToCart(cartItem, product, mDialogView.edQuantity.text.toString().toInt(), mDialogView.spin_states.selectedItem.toString())
                }

                mDialogView.edQuantity.doAfterTextChanged {

                    val qty = it.toString()

                    if(qty == "" || qty == "0") {
                        mDialogView.tvErrorText.text = "Invalid input"
                        mDialogView.tvErrorText.visibility = View.VISIBLE
                        return@doAfterTextChanged
                    }
                    else {
                        mDialogView.tvErrorText.visibility = View.INVISIBLE
                        mDialogView.btn_add.isEnabled = true
                    }

                    if (it.toString().toInt() > quantity){

                        mDialogView.tvErrorText.visibility = View.VISIBLE
                        mDialogView.tvErrorText.text = "Quantity entered is more than the quantity in van!"
                        mDialogView.btn_add.isEnabled = false
                    }
                    else {
                        mDialogView.tvErrorText.visibility = View.INVISIBLE
                        mDialogView.btn_add.isEnabled = true
                    }
                }


                //create drop down

                val states =
                    state.realm.where(ReturnStockItemState::class.java)
                        .findAll()

                val arrayAdapter = ArrayAdapter(mDialogView.context, R.layout.item_return_state, states.map { it.description + "\n"})
                arrayAdapter.setDropDownViewResource(R.layout.item_return_state)

                mDialogView.spin_states.adapter = arrayAdapter

            }
        })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        // Add Text Change Listener to EditText
        edSearch.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                // Call back the Adapter with current character to Filter
                    this@ReturnStockActivity.runOnUiThread(Runnable {
                        adapter.applyFilter(s.toString())
                    })
            }

            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {}
        })


        btn_back.setOnClickListener{
            onBackPressed()
        }

    }

    override fun onResume() {
        super.onResume()
        updateCart()
    }

    fun addToCart(existingCartItem: CartItem?, product: Product, quantity:Int, productState: String){

        if(existingCartItem != null) {
            state.realm.beginTransaction()
                existingCartItem.quantity = quantity
                state.realm.copyToRealmOrUpdate(existingCartItem)
            state.realm.commitTransaction()
            showSuccessSnackBar("Cart item updated")
        }
        else {
            val cartItem = CartItem()
            cartItem.product = product
            cartItem.quantity = quantity
            cartItem.productId = product.id
            cartItem.type = state.type.rawValue
            cartItem.state = productState

            val vat = quantity * product.price!! * product.vatRate!!
            cartItem.VAT = vat
            val amount = quantity * product.price!!
            cartItem.amount = amount
            cartItem.amountTotal = amount + vat
            cartItem.type = state.type.rawValue
            cartItem.outletId = state.outletId

            state.realm.beginTransaction()
                state.cart!!.items.add(cartItem)
                state.realm.copyToRealmOrUpdate(state.cart!!)
            state.realm.commitTransaction()
            showSuccessSnackBar(product.name + " added to cart")
        }
        updateCart()
    }

    fun getExistingCartItem(productId: Int) : CartItem? {
        var item: CartItem? = null
        state.realm.beginTransaction()
            item = state.realm.where(CartItem::class.java).equalTo("productId", productId).equalTo("type",state.type.rawValue).
            equalTo("outletId",state.outletId).findFirst()
        state.realm.commitTransaction()
        return item
    }

    private fun showSuccessSnackBar(description: String){
        val snackBar = Snackbar.make(toolbar,description, Snackbar.LENGTH_SHORT)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(resources.getColor(R.color.colorSuccess))
        snackBar.show()
    }

    private fun updateCart(){

        state.realm.beginTransaction()

        state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type.rawValue)
                .equalTo("outletId", state.outletId).findFirst()

            if (state.cart == null) {
                state.cart = Cart()
                state.cart!!.type = state.type.rawValue
                state.cart!!.outletId = state.outletId
            }

        state.realm.commitTransaction()

            val count = state.cart?.items?.count() ?: 0

            if (count > 0) {
                cart_count.text = count.toString()
                cart_count.visibility = View.VISIBLE
            } else
                cart_count.visibility = View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }
    override fun onBackPressed() {
        // super.onBackPressed()
        if (state.cart?.items?.count() ?: 0 > 0) {

            val builder = androidx.appcompat.app.AlertDialog.Builder(this)

            builder.setTitle("Cancel ${state.type}?")
            builder.setMessage("Are you sure you want to cancel this transaction?")

            builder.setPositiveButton("Yes") { _, _ ->
                //clear cart
                if (state.cart?.isManaged == true) {
                    state.realm.beginTransaction()
                        state.cart?.deleteFromRealm()
                        val cartItems = state.realm.where(CartItem::class.java).equalTo("type",state.type.rawValue).
                        equalTo("outletId",state.outletId).findAll()
                        cartItems?.deleteAllFromRealm()
                    state.realm.commitTransaction()
                }
                finish()
            }

            builder.setNegativeButton("No", null)

            val dialog = builder.create()
            dialog.show()

        } else {
            finish()
        }
    }
    private class State {
        var products: ArrayList<Product> = arrayListOf()
        lateinit var realm: Realm
        var cart: Cart? = null
        var outletId = ""
        lateinit var type: TransactionType
    }
}