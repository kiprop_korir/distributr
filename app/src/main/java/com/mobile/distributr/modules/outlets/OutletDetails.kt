package com.mobile.distributr.modules.outlets

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.SupportMapFragment
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import kotlinx.android.synthetic.main.app_bar.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import com.mobile.distributr.modules.outlets.model.Outlet
import com.google.android.gms.maps.model.CameraPosition
import kotlinx.android.synthetic.main.activity_outlet_details.*
import kotlinx.android.synthetic.main.error_layout.*
import android.content.Intent
import android.net.Uri
import android.view.MenuItem
import com.mobile.distributr.R
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_transaction_history.*
import timber.log.Timber

class OutletDetails : AppCompatActivity(), OnMapReadyCallback {

    lateinit var realm: Realm
    var outlet = Outlet()
    var outletId = 0

    @SuppressLint("TimberArgCount")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_outlet_details)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        error_layout.visibility  = View.VISIBLE
        outletId = intent.getIntExtra("outletId",0)

        outlet = realm.where(Outlet::class.java).equalTo("id", outletId)
            .findFirst()!!

        title = outlet.name
        toolbar_title.text = title

        name.text = outlet.name
        contact_person.text = outlet.owner
        location.text = outlet.town

        call_outlet.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + outlet.ownerPhone ?: ""))
            startActivity(intent)
        }

        btn_back.setOnClickListener{
            finish()
        }

        get_directions.setOnClickListener(){
            val builder = Uri.Builder()
            builder.scheme("https")
                .authority("www.google.com")
                .appendPath("maps")
                .appendPath("dir")
                .appendPath("")
                .appendQueryParameter("api", "1")
                .appendQueryParameter("destination", outlet.latitude.toString() + "," + outlet.longitude.toString())
            val url = builder.build().toString()
            Timber.d("Directions", url)
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }

        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(com.mobile.distributr.R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        btn_back.setOnClickListener{
            finish()
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        val location = LatLng(outlet.latitude ?: 0.0, outlet.longitude ?: 0.0)
        googleMap!!.addMarker(
            MarkerOptions().position(location)
                .title(outlet.name)
        )
        val cameraPosition = CameraPosition.Builder().target(location).zoom(15.0f).build()
        val cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
        googleMap.moveCamera(cameraUpdate)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }
    override fun onBackPressed() {
        // super.onBackPressed()
                finish()
    }
}
