package com.mobile.distributr.modules.pos.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.model.CartItem
import kotlinx.android.synthetic.main.item_cart.view.*
import kotlinx.android.synthetic.main.item_cart.view.amount
import kotlinx.android.synthetic.main.item_cart.view.ivProductImage
import kotlinx.android.synthetic.main.item_cart.view.name
import java.math.RoundingMode
import java.text.DecimalFormat

class POSCartItemAdapter(
    private val context: Context,
    private var dataSource: MutableList<CartItem>,
    private val clickListener: ClickListener

) :  RecyclerView.Adapter<POSCartItemAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return dataSource.count()

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position], clickListener)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_cart_pos, parent, false)
        return ViewHolder(
            v
        )
    }

    fun updateData(updatedData: MutableList<CartItem>) {
        dataSource = updatedData
        notifyDataSetChanged()
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(cartItem: CartItem, clickListener: ClickListener) {

            itemView.name.text = cartItem.product?.name
            itemView.code.text = cartItem.product?.code

            val amount = cartItem.product?.price!! * cartItem.quantity!!
            val roundedOffAmt = roundOffDecimal(amount)
            val formatter = DecimalFormat("#,###.00")
            val amt = formatter.format(amount)

            if(cartItem.product?.isFreemium == true) {
                itemView.amount.text =  "FREE"
                }
            else {
                itemView.amount.text =   "KES $amt"
            }

            itemView.ed_quantity.setText(cartItem.quantity.toString())

            //disable minus if qty is 1

                if(cartItem.quantity == 1) {
//                    itemView.minus.setBackgroundColor(
//                        ContextCompat.getColor(itemView.context, R.color.grey_300))
                    itemView.minus.isEnabled = false
                }

            itemView.add.setOnClickListener{
                addOrSubtract(true, clickListener, cartItem)
            }
            itemView.minus.setOnClickListener{
                addOrSubtract(false, clickListener, cartItem)
            }

            itemView.remove.setOnClickListener {
                clickListener.deleteFromCart(cartItem, adapterPosition)
            }

            itemView.ivProductImage.visibility = View.GONE

        }

        @SuppressLint("SetTextI18n")
        private fun addOrSubtract(add:Boolean, clickListener: ClickListener, cartItem: CartItem){
            var quantity =  itemView.ed_quantity.text.toString().toInt()

//update value on realm

            if (add)
                quantity += 1
            else
                quantity -= 1

            itemView.ed_quantity.setText(quantity.toString())
            val amount = cartItem.product?.price!! * quantity!!
            val roundedOffAmt = roundOffDecimal(amount)

            val formatter = DecimalFormat("#,###.00")
            val amt = formatter.format(roundedOffAmt)

            itemView.amount.text = "KES $amt"

            clickListener.changeQuantity(cartItem, quantity)

            itemView.minus.isEnabled = quantity != 1

        }

        fun roundOffDecimal(number: Double): Double? {
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            return df.format(number).toDouble()
        }


    }

    interface ClickListener {
        fun changeQuantity(item: CartItem, quantity: Int)
        fun deleteFromCart(item: CartItem, position: Int)
    }
}
