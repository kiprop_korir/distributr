package com.mobile.distributr.modules.product_availability

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.model.Product
import com.mobile.distributr.modules.product_availability.model.ProductAvailabilityItem
import com.mobile.distributr.modules.product_availability.model.ProductAvailabilityProductItem
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_image_capture.*
import kotlinx.android.synthetic.main.activity_transaction_history.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.app_bar.toolbar
import java.io.File

class ImageCaptureActivity : AppCompatActivity() {


    private var outletName =  ""
    var product : Product? = null
    lateinit var realm: Realm
    var productAvailabilityItem: ProductAvailabilityItem? = null
    var outletId = ""
    var filePath = ""


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            image.setImageURI(fileUri)

            //You can get File object from intent
            val file:File = ImagePicker.getFile(data)!!

            //You can also get File Path from intent
            filePath = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_capture)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tv_title.text = "PRODUCT AVAILABILITY"

        outletName = PreferenceUtils.getString(PreferenceUtils.CURRENT_OUTLET_NAME,"")!!
        outletId = PreferenceUtils.getInt(PreferenceUtils.CURRENT_OUTLET_ID, 0).toString().toString()

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        product = intent.getSerializableExtra("product") as? Product
        productAvailabilityItem = getExistingItem()

        title = product?.name



        capture.setOnClickListener{
            ImagePicker.with(this)
                .cameraOnly()       //User can only capture image using Camera
                .start()
           }
        choose.setOnClickListener{
            ImagePicker.with(this)
                .galleryOnly()       //User can only select image from Gallery
                .start()
        }

        done.setOnClickListener{
            var item = ProductAvailabilityProductItem()
            item.product = product
            item.imageLocation = filePath
            item.productCode = product?.code
            item.available = yes.isChecked

            realm.beginTransaction()
            productAvailabilityItem!!.productItems.add(item)
            realm.copyToRealmOrUpdate(productAvailabilityItem!!)
            realm.commitTransaction()

            addToCart(productAvailabilityItem!!)
        }

        btn_back.setOnClickListener{
            finish()
        }
    }
    private fun getExistingItem() : ProductAvailabilityItem? {
        var item: ProductAvailabilityItem? = null

        realm.executeTransaction() {

            item = realm.where(ProductAvailabilityItem::class.java).equalTo("outletId", outletId).findFirst()
                ?: ProductAvailabilityItem()
            item!!.outletId = outletId
        }
        return item
    }
    fun addToCart(item: ProductAvailabilityItem){

        realm.executeTransaction() {
            realm.copyToRealmOrUpdate(item)
        }

        showSuccessSnackBar()
        Handler().postDelayed({
            finish()
        }, 500)
    }

    fun showSuccessSnackBar(){
        val snackBarView = Snackbar.make(toolbar, "Product recorded successfully" , Snackbar.LENGTH_LONG)
        val view = snackBarView.view
        val params = view.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.TOP
        params.topMargin = 170
        view.layoutParams = params
        //view.background = drawable.resources.getColor(R.color.green_500)
        snackBarView.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
        snackBarView.setTextColor(resources.getColor(R.color.white))
        snackBarView.setBackgroundTint(resources.getColor(R.color.green_400))
        snackBarView.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }
    override fun onBackPressed() {
        // super.onBackPressed()

        val builder = androidx.appcompat.app.AlertDialog.Builder(this)

        builder.setTitle("Cancel Load Stock")
        builder.setMessage("Are you sure you want to cancel this transaction?")

        builder.setPositiveButton("Yes") { _, _ ->

            finish()
        }

        builder.setNegativeButton("No", null)

        val dialog = builder.create()
        dialog.show()
    }
}

