package com.mobile.distributr.modules.outlets.helper

import android.content.Intent
import android.graphics.Paint
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.modules.outlets.OutletDetails
import com.mobile.distributr.modules.outlets.model.Outlet
import com.mobile.distributr.modules.outlets.model.Route
import com.mobile.distributr.utils.PreferenceUtils
import io.github.luizgrp.sectionedrecyclerviewadapter.Section
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import java.util.*

class OutletSection : Section(
    SectionParameters.builder()
        .itemResourceId(R.layout.item_outlet)
        .headerResourceId(R.layout.item_section_header)
        .build()
) {

    lateinit var route: Route
    lateinit var outlets: ArrayList<Outlet>
    lateinit var itemClickListener: ItemClickListener

    // number of items of this section
//    val contentItemsTotal: Int
//        get() = itemList.size // number of items of this section

    override fun getItemViewHolder(view: View): RecyclerView.ViewHolder {
        // return a custom instance of ViewHolder for the items of this section
        return OutletItemViewHolder(view)
    }

    override fun getContentItemsTotal(): Int {
        return outlets.size
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemView: OutletItemViewHolder = holder as OutletItemViewHolder

        itemView.name.text = outlets[position].name
        itemView.code.text = outlets[position].code

        itemView.select.paintFlags = itemView.select.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        itemView.select.setOnClickListener{

            val builder = androidx.appcompat.app.AlertDialog.Builder(
                holder.itemView.context)

            builder.setMessage("Change current outlet to ${outlets[position].name}?")

            builder.setPositiveButton("Yes") { _, _ ->
                changeOutlet(outlets[position])
                itemClickListener.itemClick()
            }

            builder.setNegativeButton("No", null)

            val dialog = builder.create()
            dialog.show()
        }

        holder.itemView.setOnClickListener{
            val intent = Intent(holder.itemView.context, OutletDetails::class.java)
            intent.putExtra("outletId", outlets[position].id)
            holder.itemView.context.startActivity(intent)
        }
    }

    private fun changeOutlet(outlet: Outlet){
        //change outlet in shared preferences
        PreferenceUtils.putInt(PreferenceUtils.CURRENT_OUTLET_ID, outlet.id!!)
        PreferenceUtils.putString(PreferenceUtils.CURRENT_OUTLET_NAME, outlet.name!!)
        PreferenceUtils.putInt(PreferenceUtils.CURRENT_OUTLET_TYPE_ID, outlet.typeId!!)
    }

    override fun getHeaderViewHolder(view: View?): RecyclerView.ViewHolder? {
        return view?.let { HeaderViewHolder(it) }
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder) {
        val headerHolder: HeaderViewHolder = holder as HeaderViewHolder
        headerHolder.tvTitle.text = route.name + " outlets:"
    }

    interface ItemClickListener {
        fun itemClick()
    }
}