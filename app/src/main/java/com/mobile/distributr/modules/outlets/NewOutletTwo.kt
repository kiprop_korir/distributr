package com.mobile.distributr.modules.outlets

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.modules.outlets.model.Outlet
import com.mobile.distributr.modules.home.Home
import io.realm.Realm
import kotlinx.android.synthetic.main.app_bar.*

import kotlinx.android.synthetic.main.content_new_outlet_two.*

class NewOutletTwo : AppCompatActivity() , OnMapReadyCallback {

    var outlet = Outlet()
    lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_outlet_two)
        tv_title.text = "Add New Outlet"

        val name = intent.getStringExtra("name")
        val route = intent.getStringExtra("route")
        val code = intent.getStringExtra("code")
        val type = intent.getStringExtra("type")
        val category = intent.getStringExtra("category")
        val road = intent.getStringExtra("road")


        val outlet = Outlet()

        outlet.name = name
        outlet.code = code

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        btnNext.setOnClickListener{
            val fName = first_name.text.toString()
            val lName = last_name.text.toString()
            val phone = phone.text.toString()
            val address = address.text.toString()
            val city = city.text.toString()
            val company = company.text.toString()
            val contactClassification = contact_classification.selectedItem.toString()


            //outlet.id = (0 until 1000).random().toString()


            realm.beginTransaction()

            realm.copyToRealmOrUpdate(outlet)
            realm.commitTransaction()

            showSuccessSnackBar("Outlet added successfully")

            Handler().postDelayed({
                val intent = Intent(applicationContext, Home::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            }, 1500)

        }

        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(com.mobile.distributr.R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        btn_back.setOnClickListener{
            finish()
        }

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        val location = LatLng(-1.2921, 36.8219)
        googleMap!!.addMarker(
            MarkerOptions().position(location)
                .title(outlet.name)
        )
        val cameraPosition = CameraPosition.Builder().target(location).zoom(10.0f).build()
        val cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
        googleMap.moveCamera(cameraUpdate)

    }

    private fun showSuccessSnackBar(description: String){
        Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
    }

}
