package com.mobile.distributr.modules.reports.model


import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class StockReportItem : RealmObject() {

    @SerializedName("product")
    var productItems: RealmList<StockReportProductItem> =   RealmList()
    @PrimaryKey
    @SerializedName("date")
    var date: String? = null
    @SerializedName("outletId")
    var outletId: String? = null
}
