package com.mobile.distributr.modules.cart

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.modules.cart.adapter.CartItemAdapter
import com.mobile.distributr.common.service.TransactionService
import com.mobile.distributr.modules.sale.SaleSummary
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.common.service.DiscountService
import com.mobile.distributr.common.service.NumberService
import com.mobile.distributr.modules.manifest.ReturnSummary
import com.mobile.distributr.utils.PreferenceUtils
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_cart.empty_cart
import kotlinx.android.synthetic.main.activity_cart.rv_products
import kotlinx.android.synthetic.main.app_bar.*
import kotlin.collections.ArrayList

class CartActivity : AppCompatActivity() {

    private lateinit var state: State

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        state = State()
        state.type =  intent.getStringExtra("type")!!
        state.outletName = PreferenceUtils.getString(PreferenceUtils.CURRENT_OUTLET_NAME, null).toString()
        state.outletId = PreferenceUtils.getInt(PreferenceUtils.CURRENT_OUTLET_ID, 0).toString()

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        getCartItems()
        loadViews()

        when (state.type){
            TransactionType.RETURN.rawValue, TransactionType.LOSS.rawValue -> {
                calculateTotalItems()
            }
            else -> {
                calculateTotals()
            }
        }

    }

    override fun onResume() {
        super.onResume()
        rv_products.adapter?.notifyDataSetChanged()
    }

    @SuppressLint("SetTextI18n")
    private fun loadViews(){

        tv_title.text = "${state.type.capitalize()} CART"

        val adapter = CartItemAdapter(
            this,
            state.cartItemsList,
            state.type,
            object :
                CartItemAdapter.ClickListener {
                override fun changeQuantity(item: CartItem, quantity: Int) {
                    //get object from state.realm
                    state.realm.beginTransaction()
                    item.quantity = quantity
                    state.realm.copyToRealmOrUpdate(item)
                    state.realm.commitTransaction()

                    calculateTotals()
                }

                override fun deleteFromCart(item: CartItem, position: Int) {
                    //
                    state.realm.beginTransaction()
                    item.deleteFromRealm()
                    state.realm.commitTransaction()
                    //state.realm.close()
                    getCartItems()
                    rv_products.adapter?.notifyDataSetChanged()
                    calculateTotals()
                }
            })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        toolbar.setNavigationOnClickListener{
            finish()
        }

        btn_back.setOnClickListener{
            finish()
        }

        when (state.type){
            TransactionType.RETURN.rawValue, TransactionType.LOSS.rawValue -> {
                tvCartDescription.text = "TOTAL ITEMS"

                rvCheckOut.setOnClickListener{
                    val intent = Intent(this, ReturnSummary::class.java)
                    intent.putExtra("type", state.type)
                    startActivity(intent)
                }
            }
            else -> {
                tvCartDescription.text = "NET TOTAL"

                rvCheckOut.setOnClickListener{
                    val intent = Intent(this, SaleSummary::class.java)
                    intent.putExtra("type", state.type)
                    startActivity(intent)
                }
            }
        }
    }


    fun getCartItems(){

        state.cartItemsList.clear()

        state.realm.beginTransaction()
        if (state.type == TransactionType.POS.rawValue) state.outletId = "POS"


        when (state.type){
            TransactionType.RETURN.rawValue, TransactionType.LOSS.rawValue -> {
                state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type).findFirst()
            }
            else -> {
                state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type).equalTo("outletId",state.outletId).findFirst()
            }
        }

        state.cartItems = state.cart?.items
        state.realm.commitTransaction()

            if (state.cartItems != null) {
                for (item in state.cartItems!!)
                    state.cartItemsList.add(item!!)
            }
            else {
                empty_cart.visibility = View.VISIBLE
                bottom.visibility = View.GONE
            }
    }

    @SuppressLint("SetTextI18n")
    fun calculateTotals(){

        state.grossTotal = 0.0
        state.tax = 0.0
        state.totalIncTax = 0.0
        state.totalLessDiscount = 0.0
        state.cart = null

        if (state.cartItems?.count() ?: 0 > 0) {

            state.grossTotal = TransactionService.getGrossTotal(state.cartItems)
            state.productDiscounts = DiscountService.getTotalProductDiscount(state.cartItems)
            state.valueDiscount = DiscountService.getValueDiscount(state.grossTotal - state.productDiscounts)
            state.subTotal = state.grossTotal - state.valueDiscount - state.productDiscounts
            state.tax = TransactionService.getTaxTotal(state.cartItems)
            state.netAmount = state.tax + state.subTotal

            tvNetTotal.text = NumberService.formatAmount(state.netAmount)
        }
        else {
            tvNetTotal.text = "KES O.OO"
            empty_cart.visibility = View.VISIBLE
            bottom.visibility = View.GONE
        }

    }

    private fun calculateTotalItems() {

        var totalItems = 0
        if (state.cartItems?.count() ?: 0 > 0) {

           for (item in state.cartItems!!){
               totalItems += item?.quantity!!
           }
        }
       tvNetTotal.text = totalItems.toString()
    }

   private class State {
       lateinit var realm: Realm
       lateinit var type: String
       var cart: Cart? = null
       var cartItems : RealmList<CartItem?>? = null
       var cartItemsList = ArrayList<CartItem>()
       var grossTotal: Double = 0.0
       var totalLessDiscount: Double = 0.0
       var tax: Double = 0.0
       var totalIncTax: Double = 0.0
       var productDiscounts: Double = 0.0
       var valueDiscount: Double = 0.0
       var outletId = ""
       var outletName = ""
       var subTotal: Double = 0.0
       var netAmount: Double = 0.0
   }

}
