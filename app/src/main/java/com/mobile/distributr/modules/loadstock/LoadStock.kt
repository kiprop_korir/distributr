package com.mobile.distributr.modules.loadstock

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.common.adapter.ProductAdapter
import com.mobile.distributr.model.*
import com.mobile.distributr.modules.loadstock.model.LoadStockItem
import com.mobile.distributr.helper.ProductContextType
import com.mobile.distributr.modules.loadstock.model.LoadStockProductItem
import io.realm.Realm
import kotlinx.android.synthetic.main.app_bar.*
import kotlin.collections.ArrayList
import kotlinx.android.synthetic.main.activity_view_products.*
import kotlinx.android.synthetic.main.content_view_products.*
import kotlinx.android.synthetic.main.dialog_load_stock.view.*

class LoadStock : AppCompatActivity() {

    var products: ArrayList<Product> = arrayListOf()
    lateinit var realm: Realm
    lateinit var type: String
    var loadStockItem: LoadStockItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_products)

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        tv_title.text = "LOAD STOCK"
        complete.setOnClickListener {
            startActivity(Intent(this@LoadStock, LoadStockSummary::class.java))
        }
        loadStockItem = getExistingItem()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        realm.beginTransaction()
        val realmProducts = realm.where(Product::class.java)
            .findAll()
        for (product in realmProducts) {
            products.add(product)
        }
        realm.commitTransaction()

        val adapter = ProductAdapter(this,products, ProductContextType.STOCK,object : ProductAdapter.ItemClickListener {

            override fun itemClick(product: Product) {

                val mDialogView = LayoutInflater.from(this@LoadStock).inflate(R.layout.dialog_load_stock, null)
                //AlertDialogBuilder
                val mBuilder = AlertDialog.Builder(this@LoadStock)
                    .setView(mDialogView)
                    .setTitle(product.name)
                //show dialog
                val  mAlertDialog = mBuilder.show()

                val quantity =  1

                mDialogView.ed_quantity.setText(quantity.toString())
                mDialogView.available_quantity.visibility = View.VISIBLE
                mDialogView.available_quantity.text = "Available Qty: ${product.productQty}"
                //place cursor end of edit text
                mDialogView.ed_quantity.setSelection(mDialogView.ed_quantity.text.length)
               // mDialogView.quantity.requestFocus()
                mDialogView.btn_add.text = "LOAD ITEM"
                //show keyboard
                //showSoftKeyboard(mDialogView.ed_quantity)//login button click of custom layout
                mDialogView.btn_add.setOnClickListener {
                    //dismiss dialog
                    mAlertDialog.dismiss()

                    val itemLoad = LoadStockProductItem()
                    itemLoad.product = product
                    itemLoad.quantity = mDialogView.ed_quantity.text.toString().toInt()
                    itemLoad.productCode = product.code

                    realm.beginTransaction()
                    loadStockItem!!.productItems.add(itemLoad)
                    realm.copyToRealmOrUpdate(loadStockItem!!)
                    realm.commitTransaction()

                    addToCart(loadStockItem!!)
                    complete.visibility = View.VISIBLE

                }
            }
        })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        btn_back.setOnClickListener{
            finish()
        }
    }

    private fun getExistingItem() : LoadStockItem? {
        var item: LoadStockItem? = null

        realm.executeTransaction() {

            item = realm.where(LoadStockItem::class.java).findFirst()
                ?: LoadStockItem()
        }
        return item
    }

    fun addToCart(item: LoadStockItem){

        realm.executeTransaction() {
            realm.copyToRealmOrUpdate(item)
        }

        showSuccessSnackBar()
    }

    private fun showSuccessSnackBar(){
        Snackbar.make(toolbar, "Item added to list successfully" , Snackbar.LENGTH_LONG)
    }

    override fun onResume() {
        super.onResume()
        loadStockItem = getExistingItem()

        if(loadStockItem?.productItems?.size!! > 0){
            complete.visibility = View.VISIBLE
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }
    override fun onBackPressed() {
        // super.onBackPressed()

        if (loadStockItem?.productItems?.count()!! > 0) {

            val builder = androidx.appcompat.app.AlertDialog.Builder(this)

            builder.setTitle("Cancel Load Stock?")
            builder.setMessage("Are you sure you want to cancel this transaction?")

            builder.setPositiveButton("Yes") { _, _ ->
                val item = getExistingItem()
                realm.beginTransaction()
                item?.deleteFromRealm()
                realm.commitTransaction()
                //}
                finish()
            }

            builder.setNegativeButton("No", null)

            val dialog = builder.create()
            dialog.show()

        } else {
            finish()
        }
    }
}