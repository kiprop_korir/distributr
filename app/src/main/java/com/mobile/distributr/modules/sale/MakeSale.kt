package com.mobile.distributr.modules.sale

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.helper.ProductContextType
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.model.Product
import com.mobile.distributr.modules.cart.CartActivity
import com.mobile.distributr.common.adapter.ProductAdapter
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.common.model.CartItem
import com.mobile.distributr.utils.PreferenceUtils
import com.mobile.distributr.utils.PreferenceUtils.CURRENT_OUTLET_ID
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.content_view_products.*
import kotlinx.android.synthetic.main.dialog_add_to_cart.view.*

class MakeSale : AppCompatActivity() {

    private lateinit var state: State

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_make_sale)

        state = State()

        state.type =  intent.getStringExtra("type")!!
        state.outletId = PreferenceUtils.getInt(CURRENT_OUTLET_ID,0).toString()

        tv_title.text = "MAKE ${state.type}"

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        loadViews()
    }

    private fun loadViews(){

        cart_view.visibility = View.VISIBLE

        cart_view.setOnClickListener {
            val intent = Intent(this, CartActivity::class.java)
            intent.putExtra("type", state.type)
            startActivity(intent)
        }

        state.realm.beginTransaction()

            val realmProducts: RealmResults<Product>? = when (state.type) {
                TransactionType.VAN_SALE.rawValue -> {
                    state.realm.where(Product::class.java).equalTo("isIssued", true).greaterThan("productQty", 0)
                        .findAll()
                }
                else -> {
                    state.realm.where(Product::class.java)
                        .findAll()
                }
            }

            if (realmProducts != null) {
                for (product in realmProducts) {
                    state.products.add(product)
                }
            }

        state.realm.commitTransaction()

        val adapter = ProductAdapter(this, state.products, ProductContextType.SALE,object : ProductAdapter.ItemClickListener {

            @SuppressLint("SetTextI18n")
            override fun itemClick(product: Product) {

                val cartItem = getExistingCartItem(product.id!!)

                val mDialogView = LayoutInflater.from(this@MakeSale).inflate(R.layout.dialog_add_to_cart, null)
                //AlertDialogBuilder
                val mBuilder = AlertDialog.Builder(this@MakeSale)
                    .setView(mDialogView)
                    .setTitle(product.name)
                //show dialog
                val  mAlertDialog = mBuilder.show()

                if(state.type == TransactionType.VAN_SALE.rawValue)
                    mDialogView.available_quantity.visibility = View.VISIBLE

                mDialogView.available_quantity.text = "Available Qty: ${product.productQty}"

                val quantity = cartItem?.quantity ?: 1

                //place cursor end of edit text
                mDialogView.edQuantity.setSelection(mDialogView.edQuantity.text.length)

                mDialogView.btn_add.setOnClickListener {
                    //dismiss dialog
                    mAlertDialog.dismiss()
                    addToCart(cartItem, product, mDialogView.edQuantity.text.toString().toInt())
                }

                mDialogView.edQuantity.doAfterTextChanged {

                    val qty = it.toString()

                    if(qty == "" || qty == "0") {
                        mDialogView.tvErrorText.text = "Invalid input"
                        mDialogView.tvErrorText.visibility = View.VISIBLE
                        return@doAfterTextChanged
                    }
                    else {
                        mDialogView.tvErrorText.visibility = View.INVISIBLE
                        mDialogView.btn_add.isEnabled = true
                    }

                    if (it.toString().toInt() > product.productQty!! && ( state.type == TransactionType.VAN_SALE.rawValue || state.type == TransactionType.POS.rawValue )){

                        mDialogView.tvErrorText.visibility = View.VISIBLE
                        mDialogView.tvErrorText.text = "Quantity entered is more than the available quantity!"
                        mDialogView.btn_add.isEnabled = false
                    }
                    else {
                        mDialogView.tvErrorText.visibility = View.INVISIBLE
                        mDialogView.btn_add.isEnabled = true
                    }
                }

                mDialogView.edQuantity.setText(quantity.toString())

            }
        })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        // Add Text Change Listener to EditText
        edSearch.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                // Call back the Adapter with current character to Filter
                    this@MakeSale.runOnUiThread(Runnable {
                        adapter.applyFilter(s.toString())
                    })
            }

            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {}
        })


        btn_back.setOnClickListener{
            onBackPressed()
        }

    }

    override fun onResume() {
        super.onResume()
        updateCart()
    }

    fun addToCart(existingCartItem: CartItem?, product: Product, quantity:Int){

        if(existingCartItem != null) {
            state.realm.beginTransaction()
                existingCartItem.quantity = quantity
                state.realm.copyToRealmOrUpdate(existingCartItem)
            state.realm.commitTransaction()
            showSuccessSnackBar("Cart item updated")
        }
        else {
            val cartItem = CartItem()
            cartItem.product = product
            cartItem.quantity = quantity
            cartItem.productId = product.id
            cartItem.type = state.type

            val vat = quantity * product.price!! * product.vatRate!!
            cartItem.VAT = vat
            val amount = quantity * product.price!!
            cartItem.amount = amount
            cartItem.amountTotal = amount + vat
            cartItem.type = state.type
            cartItem.outletId = state.outletId

            state.realm.beginTransaction()
                state.cart!!.items.add(cartItem)
                state.realm.copyToRealmOrUpdate(state.cart!!)
            state.realm.commitTransaction()
            showSuccessSnackBar(product.name + " added to cart")
        }
        updateCart()
    }

    fun getExistingCartItem(productId: Int) : CartItem? {
        var item: CartItem? = null
        state.realm.beginTransaction()
            item = state.realm.where(CartItem::class.java).equalTo("productId", productId).equalTo("type",state.type).
            equalTo("outletId",state.outletId).findFirst()
        state.realm.commitTransaction()
        return item
    }

    private fun showSuccessSnackBar(description: String){
        val snackBar = Snackbar.make(toolbar,description, Snackbar.LENGTH_SHORT)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(resources.getColor(R.color.colorSuccess, null))
        snackBar.show()
    }

    private fun updateCart(){

        state.realm.beginTransaction()

            state.cart = state.realm.where(Cart::class.java).equalTo("type", state.type)
                .equalTo("outletId", state.outletId).findFirst()

            if (state.cart == null) {
                state.cart = Cart()
                state.cart!!.type = state.type
                state.cart!!.outletId = state.outletId
            }
        state.realm.commitTransaction()

            val count = state.cart?.items?.count() ?: 0

            if (count > 0) {
                cart_count.text = count.toString()
                cart_count.visibility = View.VISIBLE
            } else
                cart_count.visibility = View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }
    override fun onBackPressed() {
        // super.onBackPressed()
        if (state.cart?.items?.count() ?: 0 > 0) {

            val builder = androidx.appcompat.app.AlertDialog.Builder(this)

            builder.setTitle("Cancel ${state.type}?")
            builder.setMessage("Are you sure you want to cancel this transaction?")

            builder.setPositiveButton("Yes") { _, _ ->
                //clear cart
                if (state.cart?.isManaged == true) {
                    state.realm.beginTransaction()
                        state.cart?.deleteFromRealm()
                        val cartItems = state.realm.where(CartItem::class.java).equalTo("type",state.type).
                        equalTo("outletId",state.outletId).findAll()
                        cartItems?.deleteAllFromRealm()
                    state.realm.commitTransaction()
                }
                finish()
            }

            builder.setNegativeButton("No", null)

            val dialog = builder.create()
            dialog.show()

        } else {
            finish()
        }
    }
    private class State {
        var products: ArrayList<Product> = arrayListOf()
        lateinit var realm: Realm
        var cart: Cart? = null
        var outletId = ""
        lateinit var type: String
    }
}