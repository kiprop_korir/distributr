package com.mobile.distributr.modules.product_availability

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.modules.product_availability.adapter.ProductAvailabilityItemAdapter
import com.mobile.distributr.modules.product_availability.model.ProductAvailabilityItem
import com.mobile.distributr.modules.product_availability.model.ProductAvailabilityProductItem
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.utils.PreferenceUtils
import com.mobile.distributr.utils.PreferenceUtils.CURRENT_OUTLET_ID
import com.mobile.distributr.utils.PreferenceUtils.CURRENT_OUTLET_NAME
import com.mobile.distributr.modules.home.Home
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_summary_stock_take.*
import kotlinx.android.synthetic.main.activity_summary_stock_take.date
import kotlinx.android.synthetic.main.activity_summary_stock_take.outlet
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.dialog_product_availability.view.*
import java.util.*
import kotlin.collections.ArrayList

class ProductAvailabilitySummary : AppCompatActivity() {

    var productItems: ArrayList<ProductAvailabilityProductItem> = arrayListOf()
    lateinit var realm: Realm
    private var cart: Cart? = null
    var outletId = ""
    lateinit var type: String
    var productAvailabilityItem: ProductAvailabilityItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary_product_availability)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        val outletName = PreferenceUtils.getString(CURRENT_OUTLET_NAME,"")
        val outletId = PreferenceUtils.getString(CURRENT_OUTLET_ID,"")
        tv_title.text = "PRODUCT AVAILABILITY SUMMARY"
        date.text = Date().toString()
        outlet.text = outletName

        realm.beginTransaction()
        val realmProducts = realm.where(ProductAvailabilityItem::class.java).equalTo("outletId", outletId)
            .findAll()

        if (realmProducts.size > 0 ) {
            productAvailabilityItem = realmProducts[0]
            for (item in productAvailabilityItem?.productItems!!){
                productItems.add(item)
            }
        }
        realm.commitTransaction()

        val adapter = ProductAvailabilityItemAdapter(this@ProductAvailabilitySummary,productItems,object : ProductAvailabilityItemAdapter.ItemClickListener {

            override fun changeQuantity(item: ProductAvailabilityProductItem) {
                val mDialogView = LayoutInflater.from(this@ProductAvailabilitySummary)
                    .inflate(R.layout.dialog_product_availability, null)
                //AlertDialogBuilder
                val mBuilder = AlertDialog.Builder(this@ProductAvailabilitySummary)
                    .setView(mDialogView)
                    .setTitle(item.product?.name)
                //show dialog
                val mAlertDialog = mBuilder.show()
                //show keyboard
                //showSoftKeyboard(mDialogView.ed_quantity)//login button click of custom layout
                mDialogView.done.setOnClickListener {
                    //dismiss dialog
                    mAlertDialog.dismiss()

                    realm.beginTransaction()

                    item.available  = mDialogView.cbx_yes.isChecked
                    realm.copyToRealmOrUpdate(item)
                    realm.commitTransaction()

                    showSuccessSnackBar("Product updated successfully")
                    rv_products.adapter?.notifyDataSetChanged()
                }
            }
        })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        complete.setOnClickListener{
            showSuccessSnackBar("Product availability completed successfully")
            realm.beginTransaction()
            productAvailabilityItem?.deleteFromRealm()
            realm.commitTransaction()
            Handler().postDelayed({
                val intent = Intent(this, Home::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            }, 1000)
        }

        btn_back.setOnClickListener{
            finish()
        }
    }

    private fun showSuccessSnackBar(description: String){
        Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
    }

}