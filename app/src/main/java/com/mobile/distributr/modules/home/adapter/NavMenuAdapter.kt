package com.mobile.distributr.modules.home.adapter;

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity

class NavMenuAdapter(
    private val context: Context,
    private val dataSource: ArrayList<String>
) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.item_drawer_item, parent, false)
        val textView = rowView.findViewById<TextView>(R.id.title);
        val notificationCount = rowView.findViewById<TextView>(R.id.notification_count);
        textView.text = dataSource[position]

        if(position == 0)
            notificationCount.visibility = View.VISIBLE

        return rowView
    }

}
