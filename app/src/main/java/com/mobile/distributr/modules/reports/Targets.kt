package com.mobile.distributr.modules.reports

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import kotlinx.android.synthetic.main.app_bar.*

class Targets :AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_targets)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        tv_title.text = "MY  TARGETS"

        btn_back.setOnClickListener{
            finish()
        }
    }
}
