package com.mobile.distributr.modules.close_day

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.common.model.Cart
import io.realm.Realm
import kotlin.collections.ArrayList

class ReturnSummary : AppCompatActivity() {

    var productItems: ArrayList<ReturnProductItem> = arrayListOf()
    lateinit var realm: Realm
    private var cart: Cart? = null
    var outletId = ""
    lateinit var type: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary_stock_take)
//         realm = try {
//            Realm.getDefaultInstance()
//        } catch (e: Exception) {
//            Realm.init(this)
//            Realm.getDefaultInstance()
//        }
//
//        val outletName = PreferenceUtils.getString(CURRENT_OUTLET_NAME,"")
//        val outletId = PreferenceUtils.getString(CURRENT_OUTLET_ID,"")
//        tv_title.text = "RETURN ITEMS SUMMARY"
//        date.text = Date().toString()
//        outlet.text = outletName
//        outlet.visibility = View.GONE
//        complete.text = "COMPLETE ITEM RETURN"
//
//
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//
//
//        realm.beginTransaction()
//        val realmProducts = realm.where(ReturnItem::class.java).equalTo("outletId", outletId)
//            .findAll()
//
//        if (realmProducts.size > 0 ) {
//            for (item in realmProducts[0]?.productItems!!){
//                productItems.add(item)
//            }
//        }
//        realm.commitTransaction()
//
//        val adapter = ReturnItemAdapter(this@ReturnSummary,productItems,object : ReturnItemAdapter.ItemClickListener {
//
//            override fun changeQuantity(item: ReturnProductItem) {
//                val mDialogView = LayoutInflater.from(this@ReturnSummary)
//                    .inflate(R.layout.dialog_add_to_cart, null)
//                //AlertDialogBuilder
//                val mBuilder = AlertDialog.Builder(this@ReturnSummary)
//                    .setView(mDialogView)
//                    .setTitle(item.product?.name)
//                //show dialog
//                val mAlertDialog = mBuilder.show()
//
//                val quantity = 1
//
//                mDialogView.quantity.setText(quantity.toString())
//                //mDialogView.quantity.requestFocus()
//                mDialogView.btn_add.text = "DONE"
//                //show keyboard
//                //showSoftKeyboard(mDialogView.ed_quantity)//login button click of custom layout
//                mDialogView.btn_add.setOnClickListener {
//                    //dismiss dialog
//                    mAlertDialog.dismiss()
//
//                    realm.beginTransaction()
//                    val qty = mDialogView.quantity.text.toString().toInt()
//                    item.quantity = qty
//                    realm.copyToRealmOrUpdate(item)
//                    realm.commitTransaction()
//
//                    showSuccessSnackBar()
//                    rv_products.adapter?.notifyDataSetChanged()
//                }
//            }
//        })
//
//        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
//        rv_products.adapter = adapter
//
//        complete.setOnClickListener{
//            showSuccessSnackBar()
//            Handler().postDelayed({
//                val intent = Intent(this, Home::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                startActivity(intent)
//            }, 2000)
//
//        }
//
//        btn_back.setOnClickListener{
//            finish()
//        }
//
//    }
//
//
//    fun showSuccessSnackBar(){
//        val snackBarView = Snackbar.make(toolbar, "Return items completed successfully" , Snackbar.LENGTH_LONG)
//        val view = snackBarView.view
//        val params = view.layoutParams as FrameLayout.LayoutParams
//        params.gravity = Gravity.TOP
//        params.topMargin = 170
//        view.layoutParams = params
//        //view.background = drawable.resources.getColor(R.color.green_500)
//        snackBarView.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
//        snackBarView.setTextColor(resources.getColor(R.color.white))
//        snackBarView.setBackgroundTint(resources.getColor(R.color.green_400))
//        snackBarView.show()
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            android.R.id.home -> {
//                onBackPressed()
//                return true
//            }
//        }
//        return false
//    }
//    override fun onBackPressed() {
//        // super.onBackPressed()
//
//        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
//
//        builder.setTitle("Cancel Stock Take?")
//        builder.setMessage("Are you sure you want to cancel this transaction?")
//
//        builder.setPositiveButton("Yes") { _, _ ->
//            //clear cart
//            val intent = Intent(this, Home::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//            startActivity(intent)
//        }
//
//        builder.setNegativeButton("No", null)
//
//        val dialog = builder.create()
//        dialog.show()
//    }
    }
}