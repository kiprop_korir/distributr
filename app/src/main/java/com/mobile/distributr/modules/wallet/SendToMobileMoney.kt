package com.mobile.distributr.modules.wallet

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.security.PinLockActivity

import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.send_mobile_money.*

class SendToMobileMoney : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.send_mobile_money)
   supportActionBar?.setDisplayHomeAsUpEnabled(true)
        tv_title.text = "SEND TO MOBILE MONEY"

        val adapter = ArrayAdapter.createFromResource(
                applicationContext,
                R.array.mobile_money_providers,
                android.R.layout.simple_spinner_dropdown_item)


        spin_provider.adapter = adapter

        btn_next.setOnClickListener{

            var amount = ed_amount.text
            var phoneNo = ed_phone_No.text

            val builder = AlertDialog.Builder(this)
            builder.setTitle("Transfer KES $amount to $phoneNo?")

            builder.setPositiveButton("Yes") { _, _ ->
                startActivity(Intent(this, PinLockActivity::class.java))
            }

            builder.setNegativeButton("No", null)

            val dialog = builder.create()
            dialog.show()

        }

        btn_back.setOnClickListener{
            finish()
        }

    }

}
