package com.mobile.distributr.modules.settings

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.app_bar.*

class Settings :AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        tv_title.text = "SETTINGS"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        change_password.setOnClickListener{
            val intent = Intent(this, ChangePassword::class.java)
            startActivity(intent)
        }


        btn_back.setOnClickListener{
            finish()
        }
    }
}
