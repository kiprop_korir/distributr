package com.mobile.distributr.modules.manifest.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobile.distributr.R
import com.mobile.distributr.model.Product
import com.mobile.distributr.modules.manifest.model.ManifestItem
import com.mobile.distributr.utils.PreferenceUtils
import kotlinx.android.synthetic.main.item_cart.view.ivProductImage
import kotlinx.android.synthetic.main.item_manifest.view.*
import kotlinx.android.synthetic.main.item_summary.view.code
import kotlinx.android.synthetic.main.item_summary.view.name
import java.math.RoundingMode
import java.text.DecimalFormat

class ManifestItemAdapter(
    private val dataSource: MutableList<ManifestItem>,
    private val clickListener: ClickListener

) :  RecyclerView.Adapter<ManifestItemAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(dataSource[position], clickListener)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_manifest, parent, false)
        return ViewHolder(
            v
        )
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(item: ManifestItem, clickListener: ClickListener) {

            itemView.name.text = item.product?.name
            itemView.code.text = item.product?.code
            itemView.tvQuantity.text = item.product?.productQty.toString()

            val dataMode = PreferenceUtils.getBoolean(PreferenceUtils.DATA_SAVE_MODE, false)

            if(!dataMode) {
                Glide.with(itemView.context).load(item.product?.imageUrl).placeholder(R.mipmap.image_placeholder).into(itemView.ivProductImage)
            }
            else {
                itemView.ivProductImage.visibility = View.GONE
            }
        }
    }

    interface ClickListener {
        fun returnInventory(item: Product)
    }
}