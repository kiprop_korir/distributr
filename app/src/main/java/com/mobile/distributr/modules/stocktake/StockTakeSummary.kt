package com.mobile.distributr.modules.stocktake

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.mobile.distributr.R
import com.mobile.distributr.modules.stocktake.adapter.StockItemAdapter
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.modules.stocktake.model.StockTakeItem
import com.mobile.distributr.modules.stocktake.model.StockTakeProductItem
import com.mobile.distributr.utils.PreferenceUtils
import com.mobile.distributr.utils.PreferenceUtils.CURRENT_OUTLET_ID
import com.mobile.distributr.utils.PreferenceUtils.CURRENT_OUTLET_NAME
import com.mobile.distributr.modules.home.Home
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_summary_stock_take.*
import kotlinx.android.synthetic.main.activity_summary_stock_take.date
import kotlinx.android.synthetic.main.activity_summary_stock_take.outlet
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.dialog_stock_take.view.*
import java.util.*
import kotlin.collections.ArrayList

class StockTakeSummary : AppCompatActivity() {

    var productItems: ArrayList<StockTakeProductItem> = arrayListOf()
    lateinit var realm: Realm
    private var cart: Cart? = null
    var outletId = ""
    lateinit var type: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary_stock_take)
   realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        val outletName = PreferenceUtils.getString(CURRENT_OUTLET_NAME,"")
        val outletId = PreferenceUtils.getString(CURRENT_OUTLET_ID,"")
        tv_title.text = "STOCK TAKE SUMMARY"
        date.text = Date().toString()
        outlet.text = outletName


        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        realm.beginTransaction()
        val realmProducts = realm.where(StockTakeItem::class.java).equalTo("outletId", outletId)
            .findAll()

        if (realmProducts.size > 0 ) {
            for (item in realmProducts[0]?.productItems!!){
                productItems.add(item)
                println(item.productCode)
                println(item.product)
                println(item.quantity)
            }
        }
        realm.commitTransaction()

        val adapter = StockItemAdapter(this@StockTakeSummary,productItems,object : StockItemAdapter.ItemClickListener {

            override fun changeQuantity(item: StockTakeProductItem) {
                val mDialogView = LayoutInflater.from(this@StockTakeSummary)
                    .inflate(R.layout.dialog_stock_take, null)
                //AlertDialogBuilder
                val mBuilder = AlertDialog.Builder(this@StockTakeSummary)
                    .setView(mDialogView)
                    .setTitle(item.product?.name)
                //show dialog
                val mAlertDialog = mBuilder.show()

                val quantity = 1

                mDialogView.edQuantity.setText(quantity.toString())
                //mDialogView.quantity.requestFocus()
                mDialogView.btn_add.text = "DONE"
                //show keyboard
                //showSoftKeyboard(mDialogView.ed_quantity)//login button click of custom layout
                mDialogView.btn_add.setOnClickListener {
                    //dismiss dialog
                    mAlertDialog.dismiss()

                    realm.beginTransaction()
                    val qty = mDialogView.edQuantity.text.toString().toInt()
                    item.quantity = qty
                    realm.copyToRealmOrUpdate(item)
                    realm.commitTransaction()

                    showSuccessSnackBar()
                    rv_products.adapter?.notifyDataSetChanged()
                }
            }
        })

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        complete.setOnClickListener{
            showSuccessSnackBar()
            Handler().postDelayed({
                val intent = Intent(this, Home::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            }, 1000)

        }

        btn_back.setOnClickListener{
            finish()
        }

    }


    fun showSuccessSnackBar(){
        val snackBarView = Snackbar.make(toolbar, "Stock take completed successfully" , Snackbar.LENGTH_LONG)
        val view = snackBarView.view
        val params = view.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.TOP
        params.topMargin = 170
        view.layoutParams = params
        //view.background = drawable.resources.getColor(R.color.green_500)
        snackBarView.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
        snackBarView.setTextColor(resources.getColor(R.color.white))
        snackBarView.setBackgroundTint(resources.getColor(R.color.green_400))
        snackBarView.show()
    }

}