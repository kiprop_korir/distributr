package com.mobile.distributr.modules.wallet

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import kotlinx.android.synthetic.main.activity_my_account.*
import kotlinx.android.synthetic.main.app_bar.*

class MyAccount :AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account)
        tv_title.text = "MY ACCOUNT"

        transaction_history.setOnClickListener{
            val intent = Intent(this, TransactionHistory::class.java)
            startActivity(intent)
        }

        btn_back.setOnClickListener{
            finish()
        }

    }

}
