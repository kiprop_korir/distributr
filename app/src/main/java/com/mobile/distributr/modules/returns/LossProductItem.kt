package com.mobile.distributr.modules.returns

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.Product
import io.realm.annotations.PrimaryKey

open class LossProductItem : RealmObject() {

    @SerializedName("product")
    var product: Product? = null
    @PrimaryKey
    @SerializedName("productCode")
    var productCode: String? = null
    @SerializedName("quantity")
    var quantity: Int? = null
}
