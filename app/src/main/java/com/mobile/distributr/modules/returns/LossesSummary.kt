package com.mobile.distributr.modules.returns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.common.model.Cart
import io.realm.Realm
import kotlin.collections.ArrayList

class LossesSummary : AppCompatActivity() {

    var productItems: ArrayList<LossProductItem> = arrayListOf()
    lateinit var realm: Realm
    private var cart: Cart? = null
    var outletId = ""
    lateinit var type: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary_stock_take)
        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }
    }
//
//        val outletName = PreferenceUtils.getString(CURRENT_OUTLET_NAME,"")
//        val outletId = PreferenceUtils.getString(CURRENT_OUTLET_ID,"")
//        tv_title.text = "RETURNS SUMMARY"
//        date.text = Date().toString()
//        outlet.text = outletName
//        outlet.visibility = View.GONE
//        complete.text = "COMPLETE RETURN ACTIVITY"
//
//
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//
//
//        realm.beginTransaction()
//        val realmProducts = realm.where(LossItem::class.java).equalTo("outletId", outletId)
//            .findAll()
//
//        if (realmProducts.size > 0 ) {
//            for (item in realmProducts[0]?.productItems!!){
//                productItems.add(item)
//            }
//        }
//        realm.commitTransaction()
//
//        val adapter = LossItemAdapter(this@LossesSummary,productItems,object : LossItemAdapter.ItemClickListener {
//
//            override fun changeQuantity(item: LossProductItem) {
//                val mDialogView = LayoutInflater.from(this@LossesSummary)
//                    .inflate(R.layout.dialog_add_to_cart, null)
//                //AlertDialogBuilder
//                val mBuilder = AlertDialog.Builder(this@LossesSummary)
//                    .setView(mDialogView)
//                    .setTitle(item.product?.name)
//                //show dialog
//                val mAlertDialog = mBuilder.show()
//
//                val quantity = 1
//
//                mDialogView.quantity.setText(quantity.toString())
//                //place cursor end of edit text
//                mDialogView.quantity.setSelection(mDialogView.quantity.text.length)
//                //mDialogView.quantity.requestFocus()
//                mDialogView.btn_add.text = "DONE"
//                //show keyboard
//                //showSoftKeyboard(mDialogView.ed_quantity)//login button click of custom layout
//                mDialogView.btn_add.setOnClickListener {
//                    //dismiss dialog
//                    mAlertDialog.dismiss()
//
//                    realm.beginTransaction()
//                    val qty = mDialogView.quantity.text.toString().toInt()
//                    item.quantity = qty
//                    realm.copyToRealmOrUpdate(item)
//                    realm.commitTransaction()
//
//                    showSuccessSnackBar("Item added successfully")
//                    rv_products.adapter?.notifyDataSetChanged()
//                }
//            }
//        })
//
//        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
//        rv_products.adapter = adapter
//
//        complete.setOnClickListener{
//            showSuccessSnackBar("Returns recorded successfully")
//            Handler().postDelayed({
//                val intent = Intent(this, Home::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                startActivity(intent)
//            }, 1000)
//
//        }
//
//        btn_back.setOnClickListener{
//            finish()
//        }
//
//    }
//
//    private fun showSuccessSnackBar(description: String){
//        Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
//    }
//
//
}