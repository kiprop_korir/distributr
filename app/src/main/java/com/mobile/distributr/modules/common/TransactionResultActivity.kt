package com.mobile.distributr.modules.common

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.modules.home.Home
import kotlinx.android.synthetic.main.activity_transaction_result.*
import java.text.DecimalFormat

class TransactionResultActivity : AppCompatActivity() {

    private lateinit var state: State

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_result)

        state = State()

        state.type =  intent.getStringExtra("type")!!
        state.saleValue =  intent.getDoubleExtra("invoice_value", 0.0)
        state.beneficiary =  intent.getStringExtra("beneficiary")!!
        state.totalItems = intent.getIntExtra("total_items", 0)

        val formatter = DecimalFormat("#,###.00")

        when (state.type){
            TransactionType.POS.rawValue -> {
                description.text = "You have successfully made a POS sale for a total value amount of KES ${formatter.format(state.saleValue)}"
            }
            TransactionType.RETURN.rawValue, TransactionType.LOSS.rawValue-> {
                description.text = "You have successfully recorded a ${state.type} of ${state.totalItems} items"
            }
            else -> {
                description.text = "You have successfully made a ${state.type} to ${state.beneficiary} for a total value of KES ${formatter.format(state.saleValue)}"
            }
        }

        btn_complete.setOnClickListener{

            val intent = Intent(this, Home::class.java)
            intent.putExtra("id", 5)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }


        btn_print_invoice.setOnClickListener{
            Toast.makeText(this, "Coming soon :)", Toast.LENGTH_LONG).show()
        }
    }

    private class State {
        var saleValue = 0.0
        var beneficiary = ""
        var balance = 0.0
        var type = ""
        var totalItems = 0
    }

}
