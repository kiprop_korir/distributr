package com.mobile.distributr.modules.notifications.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.notifications.model.DistributrNotification
import kotlinx.android.synthetic.main.item_notification.view.*


class NotificationsAdapter(
    private val context: Context,
    private var dataSource: ArrayList<DistributrNotification>,
    private val itemClickListener: ItemClickListener

) : RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataSource.count()

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sorted = dataSource.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.date!! })
        val newDataSource: ArrayList<DistributrNotification> = arrayListOf()
        newDataSource.addAll(sorted)
        dataSource = newDataSource
        
            holder.bindItems(context,dataSource[position],itemClickListener)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return ViewHolder(
            v
        )
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(context: Context, notification: DistributrNotification, itemClickListener: ItemClickListener) {
            itemView.title.text = notification.title
            itemView.message.text = notification.message
            itemView.date.text = notification.date

            itemView.setOnClickListener {

                itemClickListener.itemClick(notification  )
            }
        }
    }

    interface ItemClickListener {
        fun itemClick(notification: DistributrNotification)
    }

}
