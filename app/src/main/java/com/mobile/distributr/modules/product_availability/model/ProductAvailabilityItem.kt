package com.mobile.distributr.modules.product_availability.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class ProductAvailabilityItem : RealmObject() {

    @SerializedName("product")
    var productItems: RealmList<ProductAvailabilityProductItem> =   RealmList()
    @PrimaryKey
    @SerializedName("date")
    var date: String? = null
    @SerializedName("outletId")
    var outletId: String? = null
}
