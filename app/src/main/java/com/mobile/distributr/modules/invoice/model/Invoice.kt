package com.mobile.distributr.modules.invoice.model

import io.realm.RealmObject
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.modules.payment.model.Payment
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class Invoice : RealmObject() {

    @PrimaryKey
    @SerializedName("refNo")
    var refNo: String? = null
    @SerializedName("id")
    var id: String? = null
    @SerializedName("date")
    var date: String? = null
    @SerializedName("payments")
    var payments: RealmList<Payment?> = RealmList()

}
