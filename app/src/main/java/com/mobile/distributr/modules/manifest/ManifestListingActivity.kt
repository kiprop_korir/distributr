package com.mobile.distributr.modules.manifest

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.common.enum.ManifestType
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.model.*
import com.mobile.distributr.modules.manifest.adapter.ManifestItemAdapter
import com.mobile.distributr.modules.manifest.model.ManifestItem
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_listing_manifest.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlin.collections.ArrayList

class ManifestListingActivity : AppCompatActivity() {

    var products: ArrayList<Product> = arrayListOf()
    lateinit var realm: Realm
    lateinit var type: String
    private lateinit var state: State

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listing_manifest)

        state = State()

        state.type = intent.getSerializableExtra("type") as ManifestType

        tv_title.text = "${state.type.rawValue} Manifest"

        when (state.type) {
            ManifestType.SALES -> {
                btnMakeDelivery.visibility = View.GONE
            }

            ManifestType.DELIVERY -> {
                btnReturnInventory.visibility = View.GONE
            }
        }

        state.realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        state.realm.beginTransaction()
        val realmProducts = state.realm.where(Product::class.java)
                    .distinct("code").equalTo("isIssued", true).greaterThan("productQty", 0).sort("name").findAll()
        state.realm.commitTransaction()

        if (realmProducts != null) {
            state.distinctItems = realmProducts.count()
            tvDistinctItems.text = state.distinctItems.toString()

            for (product in realmProducts) {

                state.realm.beginTransaction()
                val quantity = state.realm.where(Product::class.java).equalTo("code", product.code).equalTo("isIssued", true).greaterThan("productQty", 0).
                    findAll()
                state.realm.commitTransaction()

                val manifestItem =  ManifestItem()
                manifestItem.product = product
                manifestItem.quantity = quantity.size
                state.items.add(manifestItem)
            }
        }

        loadItems()

        btn_back.setOnClickListener{
            onBackPressed()
        }

        btnRecordLoss.setOnClickListener{
           goToReturns(TransactionType.LOSS)
        }

        btnReturnInventory.setOnClickListener{
            goToReturns(TransactionType.RETURN)
        }

    }

    private fun goToReturns(type: TransactionType){
        val intent = Intent(this, ReturnStockActivity::class.java)
        intent.putExtra("type", type)
        startActivity(intent)
    }


    private fun loadItems() {
        val adapter = ManifestItemAdapter(state.items, object : ManifestItemAdapter.ClickListener {

            override fun returnInventory(item: Product) {

            }
        })

        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapter
    }

    private class State {
        var items: ArrayList<ManifestItem> = arrayListOf()
        lateinit var realm: Realm
        var distinctItems: Int = 0
        lateinit var type: ManifestType
    }
}