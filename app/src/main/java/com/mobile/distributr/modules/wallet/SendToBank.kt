package com.mobile.distributr.modules.wallet

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.common.BaseActivity
import com.mobile.distributr.modules.security.PinLockActivity
import com.mobile.distributr.modules.settings.Settings

import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.send_bank.*

class SendToBank : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.send_bank)

        tv_title.text = "SEND TO BANK"

        val adapter = ArrayAdapter.createFromResource(
            applicationContext,
            R.array.banks,
            android.R.layout.simple_spinner_dropdown_item)


        spin_provider.adapter = adapter

        btn_next.setOnClickListener{

            var amount = ed_amount.text
            var accountNo = ed_phone_No.text

            val builder = AlertDialog.Builder(this)
            builder.setTitle("Transfer KES $amount to $accountNo?")

            builder.setPositiveButton("Yes") { _, _ ->
                startActivity(Intent(this, PinLockActivity::class.java))
            }

            builder.setNegativeButton("No", null)

            val dialog = builder.create()
            dialog.show()

        }

        btn_back.setOnClickListener{
            finish()
        }

    }

}
