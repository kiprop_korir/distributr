package com.mobile.distributr.modules.reports

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobile.distributr.R
import com.mobile.distributr.helper.TransactionType
import com.mobile.distributr.modules.sale.model.Sale
import com.mobile.distributr.modules.reports.adapter.SaleAdapter
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_cart.rv_products
import kotlinx.android.synthetic.main.activity_sale_report.*
import kotlinx.android.synthetic.main.app_bar.*

class SaleReport : AppCompatActivity() {

    var products: java.util.ArrayList<Sale> = arrayListOf()
    lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sale_report)

        tv_title.text = "SALE REPORT"

        realm = try {
            Realm.getDefaultInstance()
        } catch (e: Exception) {
            Realm.init(this)
            Realm.getDefaultInstance()
        }

        realm.beginTransaction()
        val realmProducts = realm.where(Sale::class.java).equalTo("type", TransactionType.VAN_SALE.rawValue)
            .findAll()
        for (product in realmProducts) {
            products.add(product)
        }

        realm.commitTransaction()

        val adapter = SaleAdapter(this,products)

        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_products.adapter = adapter

        if(products.size == 0)
            empty.visibility = View.VISIBLE

        btn_back.setOnClickListener{
            finish()
        }
    }
}
