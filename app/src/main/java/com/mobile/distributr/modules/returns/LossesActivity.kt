package com.mobile.distributr.modules.returns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobile.distributr.R
import com.mobile.distributr.common.model.Cart
import com.mobile.distributr.model.Product
import io.realm.Realm


class LossesActivity : AppCompatActivity() {

    var products: ArrayList<Product> = arrayListOf()
    lateinit var realm: Realm
    private var cart: Cart? = null
    lateinit var type: String
    lateinit var outletId: String
    var stockTakeItem : LossItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_products)
//        realm = try {
//            Realm.getDefaultInstance()
//        } catch (e: Exception) {
//            Realm.init(this)
//            Realm.getDefaultInstance()
//        }
//        outletId = PreferenceUtils.getString(CURRENT_OUTLET_ID,"").toString()
//
//        stockTakeItem = getExistingItem()
//
//        tv_title.text = "RETURNS"
//        complete.setOnClickListener {
//            startActivity(Intent(applicationContext,LossesActivity::class.java))
//        }
//
//
//        val outletName = PreferenceUtils.getString(CURRENT_OUTLET_NAME,"")
//        outletId = PreferenceUtils.getString(CURRENT_OUTLET_ID,"").toString()
//
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//
//        realm.beginTransaction()
//        val realmProducts = realm.where(Product::class.java)
//            .findAll()
//        for (product in realmProducts) {
//            products.add(product)
//        }
//        realm.commitTransaction()
//
//        val adapter = ProductAdapter(this@LossesActivity,products,ProductContextType.STOCK,object : ProductAdapter.ItemClickListener {
//
//            override fun itemClick(product: Product) {
//
//
//                val mDialogView = LayoutInflater.from(this@LossesActivity).inflate(R.layout.dialog_add_to_cart, null)
//                //AlertDialogBuilder
//                val mBuilder = AlertDialog.Builder(this@LossesActivity)
//                    .setView(mDialogView)
//                    .setTitle(product.name)
//                //show dialog
//                val  mAlertDialog = mBuilder.show()
//
//                val quantity =  1
//
//                mDialogView.quantity.setText(quantity.toString())
//                //place cursor end of edit text
//                mDialogView.quantity.setSelection(mDialogView.quantity.text.length)
//              //  mDialogView.quantity.requestFocus()
//                mDialogView.btn_add.text = "DONE"
//                //show keyboard
//                //showSoftKeyboard(mDialogView.ed_quantity)//login button click of custom layout
//                mDialogView.btn_add.setOnClickListener {
//                    //dismiss dialog
//                    mAlertDialog.dismiss()
//
//                    var item:LossProductItem = LossProductItem()
//                    item.product = product
//                    item.quantity = quantity
//
//                    realm.beginTransaction()
//                    stockTakeItem!!.productItems.add(item)
//                    realm.copyToRealmOrUpdate(stockTakeItem!!)
//                    realm.commitTransaction()
//
//                    addToCart(stockTakeItem!!)
//                    complete.visibility = View.VISIBLE
//
//                }
//            }
//        })
//
//        rv_products.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
//        rv_products.adapter = adapter
//
//        btn_back.setOnClickListener{
//            finish()
//        }
//    }
//
//    private fun getExistingItem() : LossItem? {
//        var item:LossItem? = null
//
//        realm.executeTransaction() {
//
//            item = realm.where(LossItem::class.java).equalTo("outletId", outletId).findFirst()
//                    ?: LossItem()
//                item!!.outletId = outletId
//            }
//        return item
//    }
//
//    fun addToCart(item: LossItem){
//
//            realm.executeTransaction() {
//                realm.copyToRealmOrUpdate(item)
//            }
//
//            showSuccessSnackBar("Item added successfully")
//    }
//
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            android.R.id.home -> {
//                onBackPressed()
//                return true
//            }
//        }
//        return false
//    }
//    override fun onBackPressed() {
//       // super.onBackPressed()
//
//        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
//
//        builder.setTitle("Cancel Returns")
//        builder.setMessage("Are you sure you want to cancel this transaction?")
//
//        builder.setPositiveButton("Yes") { _, _ ->
//          finish()
//        }
//
//        builder.setNegativeButton("No", null)
//
//        val dialog = builder.create()
//        dialog.show()
//    }
//
//    private fun showSuccessSnackBar(description: String){
//        Snackbar.make(toolbar, description , Snackbar.LENGTH_LONG)
//    }
    }
}