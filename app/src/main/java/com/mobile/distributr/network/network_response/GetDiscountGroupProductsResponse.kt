package com.mobile.distributr.network.network_response

import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.DiscountGroup
import com.mobile.distributr.model.DiscountGroupProduct
import com.mobile.distributr.model.PromotionalOffer
import com.mobile.distributr.modules.deliveries.model.Delivery

class GetDiscountGroupProductsResponse {
    @SerializedName("items")
    var items: List<DiscountGroupProduct>? = null
}