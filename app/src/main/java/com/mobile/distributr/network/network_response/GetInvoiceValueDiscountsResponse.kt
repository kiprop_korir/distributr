package com.mobile.distributr.network.network_response

import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.InvoiceValueDiscount
import com.mobile.distributr.model.PromotionalOffer
import com.mobile.distributr.modules.deliveries.model.Delivery

class GetInvoiceValueDiscountsResponse {
    @SerializedName("items")
    var items: List<InvoiceValueDiscount>? = null
}