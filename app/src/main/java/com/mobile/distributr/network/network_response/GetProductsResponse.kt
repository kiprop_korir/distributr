package com.mobile.distributr.network.network_response

import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.Product

class GetProductsResponse {
    @SerializedName("items")
    var items: List<Product>? = null
}