package com.mobile.distributr.network

import com.google.gson.JsonObject
import com.mobile.distributr.model.*
import com.mobile.distributr.modules.outlets.model.Route
import com.mobile.distributr.network.network_body.PostSaleBody
import com.mobile.distributr.network.network_body.ReturnNoteBody
import com.mobile.distributr.network.network_response.*
import io.reactivex.Observable
import retrofit2.http.*

interface RestInterface {

    @GET("fieldreps/{id}/issues")
    fun getIssues(        @Path("id", encoded = true) id: String,
                                  @Query("brandId") brandId: Int? = null,
                     @Query("subBrandId") subBrandId: Int? = null,
                     @Query("catalogueId") catalogueId: Int? = null,
                     @Query("productTypeId") productTypeId: Int? = null,
                     @Query("DateFilter") DateFilter: String? = null,
                     @Query("page") page: Int? = null,
                     @Query("perpage") perpage: Int? = null,
                     @Query("q") q: String? = null,
                     @Query("order") order: String? = null,
                     @Query("orderBy") orderBy: String? = null,
                     @Query("isInActive") isInActive: Boolean? = null): Observable<Array<IssueNote>?>

    @GET("issuenotes/{id}/products")
    fun getIssuedProducts(        @Path("id", encoded = true) id: Int,
                          @Query("brandId") brandId: Int? = null,
                          @Query("subBrandId") subBrandId: Int? = null,
                          @Query("catalogueId") catalogueId: Int? = null,
                          @Query("productTypeId") productTypeId: Int? = null,
                          @Query("DateFilter") DateFilter: String? = null,
                          @Query("page") page: Int? = null,
                          @Query("perpage") perpage: Int? = null,
                          @Query("q") q: String? = null,
                          @Query("order") order: String? = null,
                          @Query("orderBy") orderBy: String? = null,
                                  @Query("tierPrices") tierPrices: Boolean = true,
                          @Query("isInActive") isInActive: Boolean? = null): Observable<Array<Product>?>

    @GET("catalogues/products")
    fun getAllProducts(@Query("brandId") brandId: Int? = null,
                    @Query("subBrandId") subBrandId: Int? = null,
                    @Query("catalogueId") catalogueId: Int? = null,
                    @Query("productTypeId") productTypeId: Int? = null,
                    @Query("DateFilter") DateFilter: String? = null,
                    @Query("page") page: Int? = null,
                    @Query("perpage") perpage: Int? = null,
                    @Query("q") q: String? = null,
                    @Query("order") order: String? = null,
                    @Query("orderBy") orderBy: String? = null,
                       @Query("tierPrices") tierPrices: Boolean = true,
                    @Query("isInActive") isInActive: Boolean? = null): Observable<GetProductsResponse?>

    @GET("fieldreps/{id}/routes")
    fun getRoutes(
        @Path("id", encoded = true) id: String,
        @Query("DateFilter") DateFilter: String? = null,
        @Query("page") page: Int? = null,
        @Query("perpage") perpage: Int? = null,
        @Query("q") q: String? = null,
        @Query("order") order: String? = null,
        @Query("orderBy") orderBy: String? = null,
        @Query("isInActive") isInActive: Boolean? = null
        ): Observable<Array<Route>?>


    @GET("routes/{id}/outlets")
    fun getOutlets(
        @Path("id", encoded = true) id: String,
                    @Query("DateFilter") DateFilter: String? = null,
                    @Query("page") page: Int? = null,
                    @Query("perpage") perpage: Int? = null,
                    @Query("q") q: String? = null,
                    @Query("order") order: String? = null,
                    @Query("orderBy") orderBy: String? = null,
                    @Query("isInActive") isInActive: Boolean? = null
                    ): Observable<GetOutletsResponse?>

    @GET("dispatchnotes")
    fun getDeliveries(
        @Query("DateFilter") DateFilter: String? = null,
        @Query("page") page: Int? = null,
        @Query("perpage") perpage: Int? = null,
        @Query("q") q: String? = null,
        @Query("order") order: String? = null,
        @Query("orderBy") orderBy: String? = null,
        @Query("isInActive") isInActive: Boolean? = null): Observable<GetDeliveriesResponse?>

    @POST("mobilesales")
    fun postSale(@Body request: PostSaleBody) : Observable<JsonObject>

    @POST("mobileorders")
    fun postOrder(@Body request: PostSaleBody) : Observable<JsonObject>

    @GET("discountgroups/{id}/products")
    fun getDiscountGroupProducts(
        @Path("id", encoded = true) id: String,
        @Query("DateFilter") DateFilter: String? = null,
        @Query("page") page: Int? = null,
        @Query("perpage") perpage: Int? = null,
        @Query("q") q: String? = null,
        @Query("order") order: String? = null,
        @Query("orderBy") orderBy: String? = null,
        @Query("isInActive") isInActive: Boolean? = null): Observable<GetDiscountGroupProductsResponse?>


    @GET("discountgroups")
    fun getDiscountGroups(
        @Query("DateFilter") DateFilter: String? = null,
        @Query("page") page: Int? = null,
        @Query("perpage") perpage: Int? = null,
        @Query("q") q: String? = null,
        @Query("order") order: String? = null,
        @Query("orderBy") orderBy: String? = null,
        @Query("isInActive") isInActive: Boolean? = null): Observable<GetDiscountGroupsResponse?>

    @GET("incentiveoffers")
    fun getIncentiveOffers(
        @Query("DateFilter") DateFilter: String? = null,
        @Query("page") page: Int? = null,
        @Query("perpage") perpage: Int? = null,
        @Query("q") q: String? = null,
        @Query("order") order: String? = null,
        @Query("orderBy") orderBy: String? = null,
        @Query("isInActive") isInActive: Boolean? = null): Observable<GetIncentiveOffersResponse?>

    @GET("invoicevaluediscounts")
    fun getInvoiceValueDiscounts(
        @Query("DateFilter") DateFilter: String? = null,
        @Query("page") page: Int? = null,
        @Query("perpage") perpage: Int? = null,
        @Query("q") q: String? = null,
        @Query("order") order: String? = null,
        @Query("orderBy") orderBy: String? = null,
        @Query("isInActive") isInActive: Boolean? = null): Observable<GetInvoiceValueDiscountsResponse?>


    @GET("promotionoffers")
    fun getPromotionOffers(
        @Query("DateFilter") DateFilter: String? = null,
        @Query("page") page: Int? = null,
        @Query("perpage") perpage: Int? = null,
        @Query("q") q: String? = null,
        @Query("order") order: String? = null,
        @Query("orderBy") orderBy: String? = null,
        @Query("isInActive") isInActive: Boolean? = null): Observable<GetPromotionalOffersResponse?>

    @GET("pricingtiers")
    fun getPricingTiers(
        @Query("DateFilter") DateFilter: String? = null,
        @Query("page") page: Int? = null,
        @Query("perpage") perpage: Int? = null,
        @Query("q") q: String? = null,
        @Query("order") order: String? = null,
        @Query("orderBy") orderBy: String? = null,
        @Query("isInActive") isInActive: Boolean? = null): Observable<GetPricingTiersResponse?>


    @GET("outlettypes")
    fun getOutletTypes(
        @Query("DateFilter") DateFilter: String? = null,
        @Query("page") page: Int? = null,
        @Query("perpage") perpage: Int? = null,
        @Query("q") q: String? = null,
        @Query("order") order: String? = null,
        @Query("orderBy") orderBy: String? = null,
        @Query("isInActive") isInActive: Boolean? = null): Observable<GetOutletTypesResponse?>

    @GET("tierprices")
    fun getTierPrices(
        @Query("DateFilter") DateFilter: String? = null,
        @Query("page") page: Int? = null,
        @Query("perpage") perpage: Int? = null,
        @Query("q") q: String? = null,
        @Query("order") order: String? = null,
        @Query("orderBy") orderBy: String? = null,
        @Query("isInActive") isInActive: Boolean? = null): Observable<GetTierPricesResponse?>

    @POST("/api/returnnotes")
    fun postReturns(@Body request:  ReturnNoteBody) : Observable<JsonObject>

    //enums

    @GET("enums/PaymentOption")
    fun getPaymentOptions(): Observable<Array<PaymentOption>>

    @GET("enums/PaymentOption")
    fun getOrderStates(): Observable<Array<OrderState>>

    @GET("enums/ReturnStockItemState")
    fun getReturnStates(): Observable<Array<ReturnStockItemState>>

    @GET("fieldreps/me")
    fun getUserDetails(): Observable<GetUserIdResponse?>

}