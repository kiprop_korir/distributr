package com.mobile.distributr.network.network_response

import com.google.gson.annotations.SerializedName
import com.mobile.distributr.modules.outlets.model.Outlet

class GetOutletsResponse {
    @SerializedName("items")
    var items: List<Outlet>? = null
}