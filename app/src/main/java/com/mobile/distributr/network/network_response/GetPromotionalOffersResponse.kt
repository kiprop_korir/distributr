package com.mobile.distributr.network.network_response

import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.PromotionalOffer
import com.mobile.distributr.modules.deliveries.model.Delivery

class GetPromotionalOffersResponse {
    @SerializedName("items")
    var items: List<PromotionalOffer>? = null
}