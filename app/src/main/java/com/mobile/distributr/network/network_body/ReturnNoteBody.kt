package com.mobile.distributr.network.network_body

import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.ProductLineItem

class ReturnNoteBody {

    @SerializedName("timestamp")
    var timeStamp: String? = null

    @SerializedName("items")
    var lineItems: MutableList<ProductLineItem>? = null

    @SerializedName("fieldRepId")
    var fieldRepId: String? = null

}

