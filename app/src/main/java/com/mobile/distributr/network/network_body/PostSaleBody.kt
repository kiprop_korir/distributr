package com.mobile.distributr.network.network_body

import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.ProductLineItem

class PostSaleBody {

    @SerializedName("refNo")
    var refNo: String? = null

    @SerializedName("outletId")
    var outletId: Int? = null

    @SerializedName("consumerId")
    var consumerId: Int? = null

    @SerializedName("state")
    var state: String? = null

    @SerializedName("timestamp")
    var timeStamp: String? = null

    @SerializedName("lineItems")
    var lineItems: MutableList<ProductLineItem>? = null

    @SerializedName("invoice")
    var invoice: InvoiceBody? = null

}

