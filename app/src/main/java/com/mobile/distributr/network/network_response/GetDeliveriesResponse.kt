package com.mobile.distributr.network.network_response

import com.google.gson.annotations.SerializedName
import com.mobile.distributr.modules.deliveries.model.Delivery

class GetDeliveriesResponse {
    @SerializedName("items")
    var items: List<Delivery>? = null
}