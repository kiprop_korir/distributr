package com.mobile.distributr.network.network_response

import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.IncentiveOffer
import com.mobile.distributr.model.PromotionalOffer
import com.mobile.distributr.modules.deliveries.model.Delivery

class GetIncentiveOffersResponse {
    @SerializedName("items")
    var items: List<IncentiveOffer>? = null
}