package com.mobile.distributr.network.network_response

import com.google.gson.annotations.SerializedName

class GetUserIdResponse {
    @SerializedName("id")
    var id: String? = null
}