package com.mobile.distributr.network.network_body
import com.google.gson.annotations.SerializedName
import com.mobile.distributr.model.PaymentItem
import io.realm.RealmList

class InvoiceBody{

    @SerializedName("refNo")
    var refNo: String? = null

    @SerializedName("payments")
    var payments: List<PaymentItem?>? = null
}