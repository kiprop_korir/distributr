package com.mobile.distributr.network.network_response

import com.google.gson.annotations.SerializedName

open class BaseResponse {
    @SerializedName("code")
    var code: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("status")
    var status: Boolean? = null

    @SerializedName("message")
    var message: String? = null
}
