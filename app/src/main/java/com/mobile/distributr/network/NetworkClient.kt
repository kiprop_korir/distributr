package com.mobile.distributr.network

import com.mobile.distributr.BuildConfig
import com.mobile.distributr.app.Distributr
import com.mobile.distributr.event.LoggedOutEvent
import com.mobile.distributr.utils.PreferenceUtils
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object NetworkClient {

    private val dispatcher = Dispatcher()
    private val loggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    val client: OkHttpClient
        get() {
            dispatcher.maxRequests = 2

            // we using this OkHttp, you can add authenticator, interceptors, dispatchers,
            // logging stuff etc. easily for all your requests just editing this OkHttp
           val builder =  OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .addInterceptor { chain ->
                        val original = chain.request()
                        val accessToken = PreferenceUtils.getAuthToken()

                        val request = original.newBuilder()
                            .addHeader("Authorization", "Bearer $accessToken")
                            .addHeader("Content-Type", "application/json")
                            .build()
                        chain.proceed(request)
                    }
               .addInterceptor(loggingInterceptor)
               .addInterceptor { chain ->
                   val request = chain.request()
                   val response = chain.proceed(request)

                   if(response.code == 403 || response.code == 401 ) {
                       Distributr.instance
                           .bus()
                           ?.send(LoggedOutEvent())
                   }

                   chain.proceed(request)
               }

               .dispatcher(dispatcher)
               .cache(null)

            if(BuildConfig.DEBUG)
                builder.addInterceptor(loggingInterceptor)

            return builder.build()


        }


}