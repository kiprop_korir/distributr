package com.mobile.distributr.network.network_response

import com.google.gson.annotations.SerializedName
import com.mobile.distributr.common.model.ProductItem
import com.mobile.distributr.model.PricingTier
import com.mobile.distributr.model.PromotionalOffer
import com.mobile.distributr.model.TierPrice
import com.mobile.distributr.modules.deliveries.model.Delivery

class GetTierPricesResponse {
    @SerializedName("items")
    var items: List<TierPrice>? = null
}